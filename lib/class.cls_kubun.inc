<?php

/***************************************************************************
 * Name 		:class.cls_kubun.inc
 * Description 		:基本情報クラス
 * Include		:
 * Trigger		:
 * Create		:2010/02/01 Brico Suzuki
 * LastModify	:
 *
 *
 *
 **************************************************************************/

class cls_kubun {
	var $m_db;			// cls_db
	var $m_data;		// データ配列
	var $m_where;		// where配列
	var $m_uid;		// KBN_ID
	var $m_admin;	// 管理画面：1 コンテンツ表示:0

	//////////////////////////////////////////////////////////////////////////////
	// コンストラクタ
	// 引数:
	function cls_kubun () {
		$this->m_db = new cls_db();
	}

	//////////////////////////////////////////////////////////////////////////////
	// 新規ユニークID取得
	// static
	// 引数:
	// 戻値:ユニークID
	function getUniqId () {
		return uniqid('', true);
	}

	//////////////////////////////////////////////////////////////////////////////
	// UIDセット
	// 引数:
	// 戻値:true Or false
	function setUid ($p_uid) {
		if (strlen($p_uid) == 0) {
			return false;
		}
		$this->m_uid = $p_uid;
		return true;
	}


	//////////////////////////////////////////////////////////////////////////////
	// データ配列セット
	// 引数:データ配列
	// 戻値:true
	function setData ($p_data, $p_admin) {
		$this->m_data = $p_data;
		$this->m_admin = $p_admin;
		return true;
	}

	//////////////////////////////////////////////////////////////////////////////
	// UID取得
	// 引数:
	// 戻値:UID
	function getUid () {
		return $this->m_uid;
	}
	//////////////////////////////////////////////////////////////////////////////
	// KBN情報を取得
	// 引数:
	// 戻値:ENT情報配列 Or false
	function getInfo () {
		$sql = 	"SELECT " . 
					"KBN.KBN_UID," .
					"KBN.KBN_NO," .
					"KBN.KBN_PRIORITY," .
					"KBN.KBN_NAME," .
					"KBN.KBN_DELETE," .
					"KBN.KBN_DATE_DELETE," .
					"KBN.KBN_DATE_CREATE," .
					"KBN.KBN_DATE_UPDATE " .
				"FROM " . 
					"MST_KUBUN_DATA KBN " . 
					$this->m_where . 
				" ORDER BY " .
					"KBN.KBN_NO DESC " .
				" LIMIT 1";
		$this->m_db->execute($sql);
		unset($this->m_where);
		if ($row = $this->m_db->fetchrow()) {
			return $row;
		} else {
			return false;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	// Where句組み立て
	function setWhere() {
		unset($this->m_where);
		$arrWhere = array();
		//削除のもの以外
		$arrWhere[] = "KBN.KBN_DELETE <> 1 ";
		if ($this->m_admin == 1){
			//管理画面用where
			// UID
			if (strlen($this->m_data['KBN_UID']) > 0) {
				$arrWhere[] = "KBN.KBN_UID = " . cls_db::quote($this->m_data['KBN_UID']);
			}
			// UID
			if (strlen($this->m_data['uid']) > 0) {
				$arrWhere[] = "KBN.KBN_UID = " . cls_db::quote($this->m_data['uid']);
			}
		}

		if (count($arrWhere) > 0) {
			$this->m_where = " WHERE " . " (" . implode(") AND (", $arrWhere) . ") ";
		}
		
		return true;
	}


	//////////////////////////////////////////////////////////////////////////////
	// KBN情報を取得
	// 引数:
	// 戻値:ENT情報配列 Or false
	function getList () {
		$sql = 	"SELECT " . 
					"KBN.KBN_UID," .
					"KBN.KBN_NO," .
					"KBN.KBN_PRIORITY," .
					"KBN.KBN_NAME," .
					"KBN.KBN_DELETE," .
					"KBN.KBN_DATE_DELETE," .
					"KBN.KBN_DATE_CREATE," .
					"KBN.KBN_DATE_UPDATE " .
				"FROM " . 
					"MST_KUBUN_DATA KBN " . 
					$this->m_where . 
				" ORDER BY " .
					"KBN.KBN_PRIORITY ";
		$this->m_db->execute($sql);
		unset($this->m_where);
		if ($row = $this->m_db->fetchAll()) {
			return $row;
		} else {
			return false;
		}
	}
	//////////////////////////////////////////////////////////////////////////////
	// KBN情報を取得
	// 引数:
	// 戻値:ENT情報配列 Or false
	function getAllList () {
		$sql = 	"SELECT " . 
					"KBN.KBN_NO," .
					"KBN.KBN_NAME " .
				"FROM " . 
					"MST_KUBUN_DATA KBN " . 
				"WHERE KBN_DELETE = 0 " .
				" ORDER BY " .
					"KBN.KBN_PRIORITY ";
		$this->m_db->execute($sql);
		if ($row = $this->m_db->fetchAll()) {
			foreach ($row as $rows){
				$rtn[$rows['KBN_NO']] = $rows['KBN_NAME'];
			}
			return $rtn;
		} else {
			return false;
		}
	}
	//////////////////////////////////////////////////////////////////////////////
	// KBN情報更新
	// 引数:
	// 戻値:true Or false
	function doUpdate () {

		// KBNデータ
		$arrSql 			= array();
		$arrSql['KBN_DATE_UPDATE'] 			= "now()";
		$arrSql['KBN_NAME'] 		= cls_db::quoteS($this->m_data['KBN_NAME']);

		$arr = array();
		foreach ($arrSql as $key => $val) {
			array_push($arr, $key . "=" . $val);
		}

		$sql = 	"UPDATE MST_KUBUN_DATA SET " . 
						implode(',', $arr) . " " . 
					"WHERE " . 
						"KBN_UID = " . cls_db::quoteS($this->m_data['KBN_UID']);
		$this->m_db->execute($sql);
		
		$this->m_db->commit();
		
		
		return true;
	}


	//////////////////////////////////////////////////////////////////////////////
	// KBN情報登録(コンテンツ登録時に自動作成)
	// 引数:
	// 戻値:true Or false
	function doInsert () {
		//新規ID取得
		$this->setUid($this->getUniqId());
		//新規コンテンツID
		$contid = $this->m_db->getNextVal('KBN_NO','MST_KUBUN_DATA KBN', "");
		//新規プライオリティ
		$prio = $this->m_db->getNextVal('KBN_PRIORITY','MST_KUBUN_DATA KBN', " WHERE KBN.KBN_DELETE = 0 ");
		// 基本データ
		$arrSql 			= array();
		$arrSql['KBN_UID'] 				= cls_db::quoteS($this->getUid());
		$arrSql['KBN_DATE_CREATE'] 			= "now()";
		$arrSql['KBN_DATE_UPDATE'] 			= "NULL";
		$arrSql['KBN_NO'] 			= cls_db::quoteN($contid);
		$arrSql['KBN_PRIORITY'] 			= cls_db::quoteN($prio);
		$arrSql['KBN_NAME'] 		= cls_db::quoteS($this->m_data['KBN_NAME']);

		$sql = 	"INSERT INTO MST_KUBUN_DATA (" . 
						implode(',', array_keys($arrSql)) . 
					") VALUES ( " . 
						implode(',', array_values($arrSql)) . 
					" ) ";
					
		if ($this->m_db->execute($sql)===false) {
			$this->m_db->rollback();
		} else {
			$this->m_db->commit();

		}

		return $contid;

	}
	


	//////////////////////////////////////////////////////////////////
	// CAT情報表示順更新
	// 引数:
	// 戻値:Boolean true or false
	function modPriority () {
		if (
			(strlen($this->m_data['uid']) == 0 ) ||
			(strlen($this->m_data['pri']) == 0 ) ||
			(strlen($this->m_data['move'])== 0 )
		) {
			return true;
		}
		//一番上位なのにさらにupが押されたらなにも処理しない
		if (
			($this->m_data['pri'] == '0')&&($this->m_data['move'] == 'up')
		){
			return true;
		}
		//一番下位なのにさらにdownが押されたらなにも処理しない
		$sql = 	"SELECT MAX(KBN.KBN_PRIORITY) MAXID " .
				"FROM " . 
					"MST_KUBUN_DATA KBN " . 
				"WHERE KBN_DELETE = 0 ";
		$this->m_db->execute($sql);
		if ($row = $this->m_db->fetchrow()) {
			$maxid =  $row['MAXID'];
		} else {
			$maxid = 0;
		}
		if (
			($this->m_data['pri'] == $maxid)&&($this->m_data['move'] == 'down')
		){
			return true;
		}
		
		//表示順更新
		if ($this->m_data['move'] == 'up'){
			$pri = $this->m_data['pri'] - 1;
		}else if ($this->m_data['move'] == 'down'){
			$pri = $this->m_data['pri'] + 1;
		}
		//移動先のカテゴリーのプライオリティを先に更新
		$arrSql 		= array();
		$arrSql['KBN_PRIORITY'] 		= cls_db::quoteN($this->m_data['pri']);
		$arrSql['KBN_DATE_UPDATE'] 	= "now()";

		$where 		= "KBN_PRIORITY = ". cls_db::quoteN($pri) .
				 " AND KBN_DELETE = 0 ";

		$arr = array();
		foreach ($arrSql as $key => $val) {
			array_push($arr, $key . "=" . $val);
		}

		$sql = 	"UPDATE MST_KUBUN_DATA SET " . 
						implode(',', $arr) . " " . 
					"WHERE " . $where;
		$this->m_db->execute($sql);
		//移動対象のカテゴリーのプライオリティを次に更新
		$arrSql 		= array();
		$arrSql['KBN_PRIORITY'] 		= cls_db::quoteN($pri);
		$arrSql['KBN_DATE_UPDATE'] 	= "now()";

		$where 		= "KBN_UID = ". cls_db::quoteS($this->m_data['uid']) .
				 " AND KBN_DELETE = 0 ";

		$arr = array();
		foreach ($arrSql as $key => $val) {
			array_push($arr, $key . "=" . $val);
		}

		$sql = 	"UPDATE MST_KUBUN_DATA SET " . 
						implode(',', $arr) . " " . 
					"WHERE " . $where;
		$this->m_db->execute($sql);
		
		//そして優先順位振りなおし
		$this->renumPriority();

		$this->m_db->commit();
		return true;

	}
	//////////////////////////////////////////////////////////////////////////////
	// プライオリティ振りなおし
	// 引数:
	// 戻値:戻値:true Or false
	function renumPriority () {
		//優先順位振りなおし
		$sql = "SELECT KBN_UID FROM MST_KUBUN_DATA WHERE KBN_DELETE = 0 " .
			" ORDER BY KBN_PRIORITY ";
		$this->m_db->execute($sql);
		$row = $this->m_db->fetchAll();
		$cnt = 0;
		foreach ($row as $rows){
			$sql = 	"UPDATE MST_KUBUN_DATA " . 
						"SET  KBN_PRIORITY = " . cls_db::quoteN($cnt) .
						",KBN_DATE_UPDATE = now()  " . 
						"WHERE KBN_DELETE = 0 AND " . 
							"KBN_UID = " . cls_db::quoteS($rows['KBN_UID']);
			$this->m_db->execute($sql);
			$cnt++;
		}
		
		return true;

	}
	//////////////////////////////////////////////////////////////////////////////
	// CCT情報を削除
	// 引数:
	// 戻値:戻値:true Or false
	function doDelete ($p_KBN_ID) {
		$sql = 	"UPDATE MST_KUBUN_DATA " . 
					"SET  KBN_DELETE = 1 ," . 
					"KBN_DATE_DELETE = now()  " . 
					"WHERE " . 
						"KBN_UID = " . cls_db::quoteS($p_KBN_ID);
		$this->m_db->execute($sql);
		//そして優先順位振りなおし
		$this->renumPriority();
		$sql = 	"SELECT KBN_NO FROM MST_KUBUN_DATA " . 
					"WHERE " . 
						"KBN_UID = " . cls_db::quoteS($p_KBN_ID);
		$this->m_db->execute($sql);
		$row = $this->m_db->fetchRow();
		
		
		
		//関連内訳削除
		$sql = 	"UPDATE MST_UCHIWAKE_DATA " . 
					"SET  UCW_DELETE = 1 ," . 
					"UCW_DATE_DELETE = now()  " . 
					"WHERE " . 
						"UCW_KBN_NO = " . cls_db::quoteS($row['KBN_NO']);
		$this->m_db->execute($sql);
	
		$this->m_db->commit();

		return true;

	}
	////////////////////////////////////////////////////////////////////////////
	// TOP情報エラーチェック
	// 引数:データ
	// 戻値:エラー配列
	function isValidData () {
		$ERR = array();
		if (strlen($this->m_data['KBN_NAME'])==0) {
			$ERR['KBN_NAME'] = '区分名:必須入力です';
		}else{
			if (!isValidRangeS($this->m_data['KBN_NAME'],1,20,true)){
				$ERR['KBN_NAME'] =  '区分名:'.sprintf(OVER_ERROR,20,10);
			}
		}
		
		return $ERR;
	}
	//////////////////////////////////////////////////////////////////////////////
	// デストラクタ
	// 引数:
	// 戻値:true
	function close () {
		$this->m_db->close();
		return true;
	}
}
?>