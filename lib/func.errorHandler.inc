<?php
	/***************************************************************************
	 * Name 		:func.errHandler.inc
	 * Description 		:エラーハンドラー
	 * Include		:
	 * Trigger		:
	 * Create		:2009/07/02 Brico Suzuki
	 * LastModify	:
	 *
	 *
	 *
	 **************************************************************************/

	$errHandler = set_error_handler('errHandler');

	////////////////////////////////////////////////////////////////////////////
	// エラーハンドラー関数
	// 引数:エラーレベル
	// 		エラー文字列
	// 		エラー発生ファイル
	// 		エラー発生行
	// 		エラーコンテキスト
	// 戻値:
	function errHandler ($p_level, $p_str, $p_file, $p_line, $p_context) {
		$err 					= array();
		$err['Level'] 			= $p_level;
		$err['File'] 			= $p_file;
		$err['Line'] 			= $p_line;
		$err['Description'] 	= $p_str;
		$err['Note'] 			= 'from errHandler';
		$err['Context'] 		= $p_context;

		switch (true) {
		case in_array($p_level, array(ERR_NOTICE, ERR_USER_NOTICE, E_STRICT)):
			// 警告
			return;
//			$out = array(ERR_OUT_DB);
			break;
		default:
			// その他エラー
//			$out = array(ERR_OUT_DB, ERR_OUT_FILE, ERR_OUT_MAIL);
			$out = array(ERR_OUT_FILE);
			break;
		}
		if (ERR_DEBUG_MODE === 1) {
			// デバッグ
			array_push($out, ERR_OUT_STDOUT);
			$out = array_unique($out);
			doOutErr($err, $out);
		} else {
			// 本番
			if (count($out) > 0) {
				doOutErr($err, $out);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	// エラー出力
	// 引数:エラー配列
	// 		出力先(配列可)
	// 			ERR_OUT_STDOUT
	// 			ERR_OUT_DB
	// 			ERR_OUT_FILE
	// 			ERR_OUT_MAIL
	// 戻値:true
	function doOutErr ($p_err, $p_out = ERR_OUT_STDOUT) {
		if (!is_array($p_out)) {
			$arrOut = array($p_out);
		} else {
			$arrOut = $p_out;
		}

		if (strlen($_SERVER['HTTP_HOST']) == 0) {
			$bBatch = true;
		} else {
			$bBatch = false;
		}

		// エラー文字列作成
		$uniq 		= uniqid('', true);
		$phpfile 	= @file($p_err['File']);
		$line 		= "(" . $p_err['Line'] . ") " . str_replace(array("\t", "\n", "\r"), "", $phpfile[$p_err['Line'] - 1]);

		$string 	= array();
		$string[] 	= "[ ErrorTime ]        : " . date('Y/m/d H:i:s');
		$string[] 	= "[ ErrorCode ]        : " . $uniq;
		$string[] 	= "[ ErrorLevel ]       : " . $GLOBALS['_ERROR_LEVEL'][$p_err['Level']];
		$string[] 	= "[ ErrorFile ]        : " . $p_err['File'];
		$string[] 	= "[ ErrorLine ]        : " . $line;
		$string[] 	= "[ ErrorNumber ]      : " . $p_err['Number'];
		$string[] 	= "[ ErrorDescription ] : " . $p_err['Description'];
		$string[] 	= "[ ErrorNote ]        : " . $p_err['Note'];

		$context 	= "[ ErrorContext ]     : " . print_r($p_err['Context'], true);

		// それぞれの主力先に出力
		foreach ($arrOut as $out) {
			switch ($out) {
			case ERR_OUT_STDOUT:
				if ($bBatch) {
					$head = "System Error !\n\n";
					$foot = "";
				} else {
					$head = "<pre>" . 
							"<b>System Error !</b>\n\n";
					$foot = "</pre>";
				}
				print $head;
				print implode("\n", $string) . "\n";
				print $foot;
				break;
			case ERR_OUT_DB:
				$arrSql 					= array();
				$arrSql['ERR_ID'] 			= cls_db::quote($uniq);
				$arrSql['ERR_LEVEL'] 		= NZ($p_err['Level']);
				$arrSql['ERR_FILE'] 		= cls_db::quote($p_err['File']);
				$arrSql['ERR_LINE'] 		= cls_db::quote($line);
				$arrSql['ERR_NUMBER'] 		= cls_db::quote($p_err['Number']);
				$arrSql['ERR_DESCRIPTION'] 	= cls_db::quote($p_err['Description']);
				$arrSql['ERR_NOTE'] 		= cls_db::quote($p_err['Note']);
				$arrSql['ERR_DATE_CREATE'] 	= "CURRENT_TIMESTAMP";

				$sql = 	"INSERT INTO TBL_ERROR (" . 
							implode(',', array_keys($arrSql)) . 
						") VALUES (" . 
							implode(',', array_values($arrSql)) . 
						")";

				@$clsDb = new cls_db();
				@$clsDb->execute($sql);
				@$clsDb->commit();
				@$clsDb->close();
				break;
			case ERR_OUT_FILE:
				ob_start();
				print implode("\n", $string) . "\n";
				print $context . "\n";
				$logstr = ob_get_contents();
				ob_end_clean();
				$fp 	= @fopen(sprintf(ERR_LOG_FILE, $uniq), 'w+');
				if ($fp) {
					@fwrite($fp, $logstr);
					@fclose($fp);
				}
				break;
			case ERR_OUT_MAIL:
				$body = "システムエラーが発生しました。\n\n" . 
						implode("\n", $string);
						
				$arrHeaders = array();
				array_push($arrHeaders, 'From: "' . mb_encode_mimeheader(ERR_SYSTEM_NAME . ' システム') . '" <' . MAIL_ADMIN_1 . '>');

				@mb_language('Japanese');
				@mb_send_mail(
					ERR_SYSTEM_MAIL,
					ERR_SYSTEM_NAME . ' システムエラー',
					$body,
					implode("\n", $arrHeaders)
				);
				break;
			}
		}
		return true;
	}

?>