<?php

	/***************************************************************************
	 * Name 		:func.fieldcheck.inc
	 * Description 	:入力チェック関数
	 * Include		:
	 * Trigger		:
	 * Create		:2009/07/02 Brico Suzuki
	 * LastModify	:
	 *
	 *
	 *
	 **************************************************************************/

	////////////////////////////////////////////////////////////////////////////////////////////////
	// メールアドレスかどうか
	// 引数：メールアドレス
	// 戻値：true Or false
	function isValidMail ($p_mail) {
		// 文字チェック
		if (strlen($p_mail) == 0) {
			return false;
		}
		if (preg_match('/[^0-9a-zA-Z@\._\/\-]/', $p_mail)) {
			return false;
		}

		// DNSチェック
		$arrTemp = explode('@', $p_mail);
		if ((strlen($arrTemp[1]) == 0) || (!checkdnsrr($arrTemp[1], 'ANY'))) {
			return false;
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 携帯アドレスかどうか
	// 引数：メールアドレス
	// 戻値：true Or false
	function isMobileMail ($p_mail) {
		if (strlen($p_mail) == 0) {
			return false;
		}
		$arrTemp = explode('@', $p_mail);
		if (count($arrTemp) != 2) {
			// そもそも不正なメールアドレス
			return false;
		}
		$arrTemp[1] = strtolower($arrTemp[1]);
		if (preg_match('/(docomo|vodafone|softbank|ezweb|tu-ka)\.ne\.jp/', $arrTemp[1])) {
			// 携帯
			return true;
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 文字範囲チェック
	// 引数：文字列
	// 		 最小文字数
	// 		 最大文字数
	// 		 文字数でチェックするかどうか
	// 戻値：true Or false
	function isValidRangeS ($p_value, $p_min = 0, $p_max = null, $p_bMultibyte = false) {
		if ($p_bMultibyte) {
			//$len = mb_strlen($p_value);

			//文字列の文字数
			$mojiNum = mb_strlen($p_value);
			$a = 0;
			$mojiBite =0;
			$rtn = 0;
			$return = array();
			//文字数分ループ
			while ($a < $mojiNum){
				//$a文字目は全角か半角か？半角なら1全角なら2加算
				if (strlen(mb_substr($p_value,$a,1,'UTF-8')) > 1){
					$mojiBite = $mojiBite + 2;
				}else{
					$mojiBite = $mojiBite + 1;
				}
				$a++;
			}
			$len = $mojiBite;

		} else {
			$len = strlen($p_value);
		}

		if ($len < $p_min) {
			return false;
		}
		if (!is_null($p_max))
			if ($len > $p_max) {
			return false;
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 数値範囲チェック
	// 引数：数値
	// 		 最小
	// 		 最大
	// 戻値：true Or false
	function isValidRangeN ($p_value, $p_min, $p_max) {
		if (!isValidNum($p_value)) {
			return false;
		}

		$num = intval($p_value);
		if ($num < $p_min) {
			return false;
		}
		if (!is_null($p_max))
			if ($num > $p_max) {
			return false;
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 日付形式かどうかチェック
	// 引数：日付配列 Or YYYY/MM/DD HH24:MI:SS
	// 戻値：true Or false
	// LastModify: YYYY,MM,DD,HH24,MI対応 
	function isValidDate ($p_date) {
		if (!is_array($p_date)) {
			$tmp1 = explode(' ', $p_date);
			$tmp2 = explode('/', $tmp1[0]);
			$tmp3 = explode(':', $tmp1[1]);
			$date = array_merge($tmp2, $tmp3);
		} else {
			$date = $p_date;
		}
		if (count($date) != 3 && count($date) != 5 && count($date) != 6) {
			return false;
		}
		foreach ($date as $val) {
			if (strlen($val) == 0) {
				return false;
			}
		}
		if (!checkdate($date[1], $date[2], $date[0])) {
			return false;
		}

		if (count($date) == 5) {
			if (
				($date[3] < 0 || $date[3] > 23) || 
				($date[4] < 0 || $date[4] > 59)
			) {
				return false;
			}
		} elseif (count($date) == 6) { 
			if (
				($date[3] < 0 || $date[3] > 23) || 
				($date[4] < 0 || $date[4] > 59) || 
				($date[5] < 0 || $date[5] > 59)
			) {
				return false;
			}
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 正しいパスワードかどうか
	// 引数：パスワード
	// 戻値：true Or false
	function isValidPass ($p_pass) {
		// 文字チェック
		if (strlen($p_pass) == 0 || strlen($p_pass) > 8) {
			return false;
		}
		if (preg_match('/[^0-9a-zA-Z]/', $p_pass)) {
			return false;
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// URLかどうか
	// 引数：URL
	// 戻値：true Or false
	function isValidUrl ($p_url) {
		// 文字チェック
		if (strlen($p_url) == 0) {
			return false;
		}
		if (preg_match('/[\x8E\xA1-\xFE\n \'\\\,\"<>]/', $p_url)) {
			return false;
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 文字範囲チェック
	// 引数：文字列
	// 		 文字数
	// 		 タイプ（0半角数字、1半角英数、2半角英）
	// 		 
	// 戻値：true Or false
	function isValidRangeX ($p_value, $p_mojisu = 0, $p_type = 0) {
		$len = strlen($p_value);
		
		if ($p_type == 0){
			//半角数字
			if (preg_match('/[^0-9]/', $p_value)) {
				return false;
			}
		}else if ($p_type == 1){
			//半角数字
			if (preg_match('/[^0-9a-zA-Z]/', $p_value)) {
				return false;
			}
		}else if ($p_type == 2){
			//半角英
			if (preg_match('/[^a-zA-Z]/', $p_value)) {
				return false;
			}
		}
		
		if (($len <> $p_mojisu)&&($p_mojisu > 0)) {
			return false;
		}
		return true;
	}

?>