<?php

/***************************************************************************
 * Name 		:class.cls_db.inc
 * Description 	:データベース操作クラス
 * Include		:
 * Trigger		:
 * Create		:2008/02/07 Brico Suzuki
 * LastModify	:
 *
 *
 *
 **************************************************************************/

class cls_db {
	var $m_debug;						// デバッグモード

	var $m_conn;						// DB接続リソース
	var $m_result;						// 結果リソース
	var $m_error 		= array();		// エラー配列
	var $m_row;							// 現在の位置の結果配列
	var $m_hash;						// 現在の位置の結果連想配列

	var $m_autocommit;					// オートコミット(true Or false)

	//////////////////////////////////////////////////////////////////////////////
	// コンストラクタ
	// 引数:オートコミット
	// 		新規接続を開くかどうか
	function cls_db($p_autocommit = true, $p_new = true) {
		if (DB_DEBUG === 1) {
			$this->m_debug = true;
		} else {
			$this->m_debug = false;
		}
		$this->m_autocommit = $p_autocommit;
		$cnString = array (
						'host=' . DB_HOST,
						',user=' . DB_USER,
						',password=' . DB_PASS
					);

		// 接続処理
		if ($p_new) {
			$this->m_conn = @mysql_connect(DB_HOST,DB_USER,DB_PASS, TRUE)
			    or die('Could not connect: ' . mysql_error());
		} else {
			$this->m_conn = @mysql_connect(DB_HOST,DB_USER,DB_PASS)
			    or die('Could not connect: ' . mysql_error());
		}
		
		// PHP内部文字コードで通信させる
		//if (strlen(DB_TO_INTERNAL_CHAR)>0){
			$this->execute("SET NAMES utf8");
		//}

		//DATABASE選択
		$db_selected = mysql_select_db(DB_NAME ,$this->m_conn);
		if (!$db_selected) {
		    die ('Can\'t use '. DB_USER .' : ' . mysql_error());
		}
	//	$result = mysql_query("SHOW VARIABLES");
	//	while ($row = mysql_fetch_assoc($result)) {
 	//	print "answer is ..."; print_r($row);
	//	}
		
		if (!$this->m_autocommit) {
			$this->begin();
		}
	}

	//////////////////////////////////////////////////////////////////////////////
	// エラーをセット
	// private
	// 引数		:エラーメッセージ
	// 			 実行したSQL
	// 戻値 	:true
	function _setError($p_err, $p_sql = null) {
		array_push($this->m_error, $p_err);
		if ($this->m_debug) {
			// DEBUG
			$msg = $this->m_error;
			if (!is_null($p_sql)) {
				array_push($msg, 'SQL:'.$p_sql);
			}
			print nl2br(implode("\n", $msg));
		} else {
			trigger_error(implode("\n", $this->m_error), ERR_USER_WARNING);
		}
		return true;
	}

	//////////////////////////////////////////////////////////////////////////////
	// エラーメッセージを返す
	// 引数		:
	// 戻値		:エラーメッセージ
	function getError () {
		if (count($this->m_error) > 0) {
			return implode("\n", $this->m_error);
		}
	}

	//////////////////////////////////////////////////////////////////////////////
	// SQL文を実行
	// 引数		：SQLステートメント
	// 戻り値	：true:正常,false:失敗
	function execute($p_stmt) {
		$this->_freeResult();
		if (!$this->_isConnect()) {
			// DB接続無効
			$this->_setError('can\'t connect database');
			return false;
		}
		$this->m_result = @mysql_query($p_stmt, $this->m_conn);
		//if (!$this->_isValidResult()) {
		if (!$this->m_result) {
			$this->_setError(@mysql_error($this->m_conn), $p_stmt);
			return false;
		}
		return true;
	}

	//////////////////////////////////////////////////////////////////////////////
	// UPDATE文実行
	// 引数		：対象テーブル
	// 			  データ配列(カラム名 = 値)
	// 			  WHERE句
	// 戻り値	：true:正常,false:失敗
	function doUpdate($p_table, $p_values, $p_where) {
		$arrTemp 	= array();
		$where 		= "";

		if (strlen($p_table) == 0 || count($_p_values) == 0) {
			return false;
		}

		foreach($p_values as $key => $val){
			$arrTemp[] = $key . "=" .$val;
		}
		if (strlen($p_where) > 0) {
			$where = "WHERE " . $p_where;
		}

		$sql = 	"UPDATE " . $this->quote($p_table) . " SET " . 
					implode(',', $arrTemp) . " " . 
				$where;

		return $this->execute($sql);
	}

	//////////////////////////////////////////////////////////////////////////////
	// INSERT文実行
	// 引数		：対象テーブル
	// 			  データ配列(カラム名 => 値)
	// 戻り値	：true:正常,false:失敗
	function doInsert($p_table, $p_values) {
		if (strlen($p_table) == 0 || count($_p_values) == 0) {
			return false;
		}
		$sql = 	"INSERT INTO " . $this->quote($p_table) . " (" . 
					implode(',', array_keys($p_values)) . 
				") VALUES (" . 
					implode(',', array_values($p_values)) . 
				")";

		return $this->execute($sql);
	}


	//////////////////////////////////////////////////////////////////////////////
	// MAX+1値取得
	// 引数		：項目名
	// 戻り値	：MAX + 1値 Or False
	function getNextVal ($p_seqname,$p_seqtbl,$_option=null) {
		if (strlen($p_seqname) == 0) {
			return false;
		}

		$sql = "SELECT IF(MAX(" . $p_seqname . ") is null,1,MAX(" . $p_seqname . ") + 1) AS SEQ FROM " . $p_seqtbl .$_option ;
		$this->execute($sql);
		if ($row = $this->fetchrow()) {
			return $row['SEQ'];
		} else {
			return false;
		}
	}

	//////////////////////////////////////////////////////////////////////////////
	// 行数を返す
	// 引数		:
	// 戻値		:行数
	function get_numrows() {
		if ($this->_isValidResult()) {
			return mysql_num_rows($this->m_result);
		}
	}

	//////////////////////////////////////////////////////////////////////////////
	// 列数を返す
	// 引数		:
	// 戻値		:カラム数
	function get_numcols() {
		if ($this->_isValidResult()) {
			return mysql_num_fields($this->m_result);
		}
	}

	//////////////////////////////////////////////////////////////////////////////
	// 影響のあった行数を返す
	function get_affected(){
		if ($this->_isValidResult()) {
			return mysql_affected_rows($this->m_result);
		}
	}

	//////////////////////////////////////////////////////////////////////////////
	// 結果をfetchして、連想配列を返す
	function fetchrow() {
		if (!$this->_isValidResult()) {
			return;
		}

		$this->m_row 	= array();
		$this->m_hash 	= array();

		$tmprow = @mysql_fetch_assoc($this->m_result);

		if ($tmprow === false || !is_array($tmprow)) {
			return;
		}

		// valueメソッド用にハッシュを作成
		$this->m_hash 	= array_change_key_case($tmprow, CASE_UPPER);
		// valueメソッド用に配列を作成
		$this->m_row 	= array_values($this->m_hash);

		$row = array();
		foreach ($tmprow as $key => $val) {
			$row[strtoupper($key)] = $val;
		}
		return $row;
	}

	//////////////////////////////////////////////////////////////////////////////
	// 全ての結果を連想配列で取得
	function fetchAll() {

		if (!$this->_isValidResult()) {
			return;
		}

		$rows 		= array();

		while($row = @mysql_fetch_assoc($this->m_result)) {
			array_push($rows, $row);
		}
		return $rows;
	}

	//////////////////////////////////////////////////////////////////////////////
	// DBに接続しているかどうか
	// 引数		:
	// 戻値		:ture:接続中,false:接続失敗している
	function _isConnect() {
		if (
			is_resource($this->m_conn) && 
			get_resource_type($this->m_conn) 	=== 'mysql link' && 
			mysql_ping($this->m_conn)
		) {
			return true;
		} else {
			return false;
		}
	}

	//////////////////////////////////////////////////////////////////////////////
	// 結果リソースが正しいかどうか
	// 引数		:
	// 戻値		:ture:正しい結果リソース,false:不正な結果リソース
	function _isValidResult() {
		if (
			is_resource($this->m_result) && 
			get_resource_type($this->m_result) === 'mysql result'
		) {
			return true;
		} else {
			return false;
		}
	}

	//////////////////////////////////////////////////////////////////////////////
	// 結果リソースを開放
	// private
	// 引数		:
	// 戻値		:ture:OK,false:NG
	function _freeResult () {
		unset($this->m_row);
		unset($this->m_hash);

		if ($this->_isValidResult()) {
			if (!@mysql_free_result($this->m_result)) {
				$this->_setError('can\'t release mysql link resource');
				return false;
			}
		}
		return true;
	}

	//////////////////////////////////////////////////////////////////////////////
	// トランザクション開始
	function begin() {
		if ($this->m_autocommit) {
			return false;
		}
		$this->execute('BEGIN');
		return true;
	}

	//////////////////////////////////////////////////////////////////////////////
	// SQL用文字列エスケープ
	// static
	// 引数:文字列
	// 戻値:エスケープ文字列
	function quote ($p_string) {
		if (strlen($p_string) == 0) {
			return "NULL";
		} else {
			if (DB_ENCODING == 'SJIS') {
				$string = mb_ereg_replace('\\\\', '\\\\', $p_string); 
				$string = mb_ereg_replace('\'', '\\\'', $string); 
			} else {
				$string = mysql_real_escape_string($p_string);
			}
		}
		return "'" . $string . "'";
	}

	//////////////////////////////////////////////////////////////////////////////
	// SQL用に文字列クオート
	// public static
	// 引数: String SQL値用文字列
	// 戻値: String or null
	function quoteS ($p_string) {
		if (strlen($p_string) == 0) {
			return "NULL";
		} else {
			//$string = mysql_real_escape_string($p_string);
			$string = mysql_real_escape_string($p_string);
		}
		return "'" . $string . "'";
	}

	//////////////////////////////////////////////////////////////////////////////
	// SQL用に数値クオート
	// public static
	// 引数: Integer SQL用数値
	// 		 Mix 数値でなかった場合の代替値
	// 戻値: Integer or String ('NULL')
	function quoteN ($p_number, $p_replacement = "NULL") {
		if (strlen($p_number) == 0 || preg_match('/[^0-9\-]/', $p_number)) {
			return $p_replacement;
		} else {
			return intval($p_number);
		}
	}
	//////////////////////////////////////////////////////////////////////////////
	// コミット
	function commit() {
		if ($this->m_autocommit) {
			return false;
		}
		$this->execute('COMMIT');
		$this->begin();
		return true;
	}

	//////////////////////////////////////////////////////////////////////////////
	// ロールバック
	function rollback() {
		if ($this->m_autocommit) {
			return false;
		}
		$this->execute('ROLLBACK');
		$this->begin();
		return true;
	}

	//////////////////////////////////////////////////////////////////////////////
	// デストラクタ
	function close() {
		$this->_freeResult();
		@mysql_close($this->m_conn);
	}
}
?>
