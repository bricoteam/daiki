<?php

/***************************************************************************
 * Name 		:class.cls_shain.inc
 * Description 		:基本情報クラス
 * Include		:
 * Trigger		:
 * Create		:2010/02/01 Brico Suzuki
 * LastModify	:
 *
 *
 *
 **************************************************************************/

class cls_shain {
	var $m_db;			// cls_db
	var $m_data;		// データ配列
	var $m_where;		// where配列
	var $m_uid;		// SHN_ID
	var $m_admin;	// 管理画面：1 コンテンツ表示:0

	//////////////////////////////////////////////////////////////////////////////
	// コンストラクタ
	// 引数:
	function cls_shain () {
		$this->m_db = new cls_db();
	}

	//////////////////////////////////////////////////////////////////////////////
	// 新規ユニークID取得
	// static
	// 引数:
	// 戻値:ユニークID
	function getUniqId () {
		return uniqid('', true);
	}

	//////////////////////////////////////////////////////////////////////////////
	// UIDセット
	// 引数:
	// 戻値:true Or false
	function setUid ($p_uid) {
		if (strlen($p_uid) == 0) {
			return false;
		}
		$this->m_uid = $p_uid;
		return true;
	}


	//////////////////////////////////////////////////////////////////////////////
	// データ配列セット
	// 引数:データ配列
	// 戻値:true
	function setData ($p_data, $p_admin) {
		$this->m_data = $p_data;
		$this->m_admin = $p_admin;
		return true;
	}

	//////////////////////////////////////////////////////////////////////////////
	// UID取得
	// 引数:
	// 戻値:UID
	function getUid () {
		return $this->m_uid;
	}
	//////////////////////////////////////////////////////////////////////////////
	// SHN情報を取得
	// 引数:
	// 戻値:ENT情報配列 Or false
	function getInfo () {
		$sql = 	"SELECT " . 
					"SHN.SHN_UID," .
					"SHN.SHN_NO," .
					"SHN.SHN_PRIORITY," .
					"SHN.SHN_NAME," .
					"SHN.SHN_NICKNAME," .
					"SHN.SHN_HEADOFFICE," .
					"SHN.SHN_DELETE," .
					"SHN.SHN_DATE_DELETE," .
					"SHN.SHN_DATE_CREATE," .
					"SHN.SHN_DATE_UPDATE " .
				"FROM " . 
					"MST_SHAIN_DATA SHN " . 
					$this->m_where . 
				" ORDER BY " .
					"SHN.SHN_NO DESC " .
				" LIMIT 1";
		$this->m_db->execute($sql);
		unset($this->m_where);
		if ($row = $this->m_db->fetchrow()) {
			return $row;
		} else {
			return false;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	// Where句組み立て
	function setWhere() {
		unset($this->m_where);
		$arrWhere = array();
		//削除のもの以外
		$arrWhere[] = "SHN.SHN_DELETE <> 1 ";
		if ($this->m_admin == 1){
			//管理画面用where
			// UID
			if (strlen($this->m_data['SHN_UID']) > 0) {
				$arrWhere[] = "SHN.SHN_UID = " . cls_db::quote($this->m_data['SHN_UID']);
			}
			// UID
			if (strlen($this->m_data['uid']) > 0) {
				$arrWhere[] = "SHN.SHN_UID = " . cls_db::quote($this->m_data['uid']);
			}
		}

		if (count($arrWhere) > 0) {
			$this->m_where = " WHERE " . " (" . implode(") AND (", $arrWhere) . ") ";
		}
		
		return true;
	}


	//////////////////////////////////////////////////////////////////////////////
	// SHN情報を取得
	// 引数:
	// 戻値:ENT情報配列 Or false
	function getList () {
		$sql = 	"SELECT " . 
					"SHN.SHN_UID," .
					"SHN.SHN_NO," .
					"SHN.SHN_PRIORITY," .
					"SHN.SHN_NAME," .
					"SHN.SHN_NICKNAME," .
					"SHN.SHN_HEADOFFICE," .
					"SHN.SHN_DELETE," .
					"SHN.SHN_DATE_DELETE," .
					"SHN.SHN_DATE_CREATE," .
					"SHN.SHN_DATE_UPDATE " .
				"FROM " . 
					"MST_SHAIN_DATA SHN " . 
					$this->m_where . 
				" ORDER BY " .
					"SHN.SHN_PRIORITY ";
		$this->m_db->execute($sql);
		unset($this->m_where);
		if ($row = $this->m_db->fetchAll()) {
			return $row;
		} else {
			return false;
		}
	}
	//////////////////////////////////////////////////////////////////////////////
	// SHN情報を取得
	// 引数:
	// 戻値:ENT情報配列 Or false
	function getAllList () {
		$sql = 	"SELECT " . 
					"SHN.SHN_NO," .
					"SHN.SHN_NAME " .
				"FROM " . 
					"MST_SHAIN_DATA SHN " . 
				"WHERE SHN_DELETE = 0 " .
				" ORDER BY " .
					"SHN.SHN_PRIORITY ";
		$this->m_db->execute($sql);
		if ($row = $this->m_db->fetchAll()) {
			foreach ($row as $rows){
				$rtn[$rows['SHN_NO']] = $rows['SHN_NAME'];
			}
			return $rtn;
		} else {
			return false;
		}
	}
	//////////////////////////////////////////////////////////////////////////////
	// SHN情報更新
	// 引数:
	// 戻値:true Or false
	function doUpdate () {

		// SHNデータ
		$arrSql 			= array();
		$arrSql['SHN_DATE_UPDATE'] 			= "now()";
		$arrSql['SHN_NAME'] 		= cls_db::quoteS($this->m_data['SHN_NAME']);
		$arrSql['SHN_NICKNAME'] 		= cls_db::quoteS($this->m_data['SHN_NICKNAME']);
		$arrSql['SHN_HEADOFFICE'] 		= cls_db::quoteN($this->m_data['SHN_HEADOFFICE']);

		$arr = array();
		foreach ($arrSql as $key => $val) {
			array_push($arr, $key . "=" . $val);
		}

		$sql = 	"UPDATE MST_SHAIN_DATA SET " . 
						implode(',', $arr) . " " . 
					"WHERE " . 
						"SHN_UID = " . cls_db::quoteS($this->m_data['SHN_UID']);
		$this->m_db->execute($sql);
		
		$this->m_db->commit();
		
		
		return true;
	}


	//////////////////////////////////////////////////////////////////////////////
	// SHN情報登録(コンテンツ登録時に自動作成)
	// 引数:
	// 戻値:true Or false
	function doInsert () {
		//新規ID取得
		$this->setUid($this->getUniqId());
		//新規コンテンツID
		$contid = $this->m_db->getNextVal('SHN_NO','MST_SHAIN_DATA SHN', "");
		//新規プライオリティ
		$prio = $this->m_db->getNextVal('SHN_PRIORITY','MST_SHAIN_DATA SHN', " WHERE SHN.SHN_DELETE = 0 ");
		// 基本データ
		$arrSql 			= array();
		$arrSql['SHN_UID'] 				= cls_db::quoteS($this->getUid());
		$arrSql['SHN_DATE_CREATE'] 			= "now()";
		$arrSql['SHN_DATE_UPDATE'] 			= "NULL";
		$arrSql['SHN_NO'] 			= cls_db::quoteN($contid);
		$arrSql['SHN_PRIORITY'] 			= cls_db::quoteN($prio);
		$arrSql['SHN_NAME'] 		= cls_db::quoteS($this->m_data['SHN_NAME']);
		$arrSql['SHN_NICKNAME'] 		= cls_db::quoteS($this->m_data['SHN_NICKNAME']);
		$arrSql['SHN_HEADOFFICE'] 		= cls_db::quoteN($this->m_data['SHN_HEADOFFICE']);

		$sql = 	"INSERT INTO MST_SHAIN_DATA (" . 
						implode(',', array_keys($arrSql)) . 
					") VALUES ( " . 
						implode(',', array_values($arrSql)) . 
					" ) ";
					
		if ($this->m_db->execute($sql)===false) {
			$this->m_db->rollback();
		} else {
			$this->m_db->commit();

		}

		return $contid;

	}
	


	//////////////////////////////////////////////////////////////////
	// CAT情報表示順更新
	// 引数:
	// 戻値:Boolean true or false
	function modPriority () {
		if (
			(strlen($this->m_data['uid']) == 0 ) ||
			(strlen($this->m_data['pri']) == 0 ) ||
			(strlen($this->m_data['move'])== 0 )
		) {
			return true;
		}
		//一番上位なのにさらにupが押されたらなにも処理しない
		if (
			($this->m_data['pri'] == '0')&&($this->m_data['move'] == 'up')
		){
			return true;
		}
		//一番下位なのにさらにdownが押されたらなにも処理しない
		$sql = 	"SELECT MAX(SHN.SHN_PRIORITY) MAXID " .
				"FROM " . 
					"MST_SHAIN_DATA SHN " . 
				"WHERE SHN_DELETE = 0 ";
		$this->m_db->execute($sql);
		if ($row = $this->m_db->fetchrow()) {
			$maxid =  $row['MAXID'];
		} else {
			$maxid = 0;
		}
		if (
			($this->m_data['pri'] == $maxid)&&($this->m_data['move'] == 'down')
		){
			return true;
		}
		//表示順更新
		if ($this->m_data['move'] == 'up'){
			$pri = $this->m_data['pri'] - 1;
		}else if ($this->m_data['move'] == 'down'){
			$pri = $this->m_data['pri'] + 1;
		}
		//移動先のカテゴリーのプライオリティを先に更新
		$arrSql 		= array();
		$arrSql['SHN_PRIORITY'] 		= cls_db::quoteN($this->m_data['pri']);
		$arrSql['SHN_DATE_UPDATE'] 	= "now()";

		$where 		= "SHN_PRIORITY = ". cls_db::quoteN($pri) .
				 " AND SHN_DELETE = 0 ";

		$arr = array();
		foreach ($arrSql as $key => $val) {
			array_push($arr, $key . "=" . $val);
		}

		$sql = 	"UPDATE MST_SHAIN_DATA SET " . 
						implode(',', $arr) . " " . 
					"WHERE " . $where;
		$this->m_db->execute($sql);
		//移動対象のカテゴリーのプライオリティを次に更新
		$arrSql 		= array();
		$arrSql['SHN_PRIORITY'] 		= cls_db::quoteN($pri);
		$arrSql['SHN_DATE_UPDATE'] 	= "now()";

		$where 		= "SHN_UID = ". cls_db::quoteS($this->m_data['uid']) .
				 " AND SHN_DELETE = 0 ";

		$arr = array();
		foreach ($arrSql as $key => $val) {
			array_push($arr, $key . "=" . $val);
		}

		$sql = 	"UPDATE MST_SHAIN_DATA SET " . 
						implode(',', $arr) . " " . 
					"WHERE " . $where;
		$this->m_db->execute($sql);
		
		$this->m_db->commit();
		return true;

	}
	//////////////////////////////////////////////////////////////////////////////
	// プライオリティ振りなおし
	// 引数:
	// 戻値:戻値:true Or false
	function renumPriority () {
		//優先順位振りなおし
		$sql = "SELECT SHN_UID FROM MST_SHAIN_DATA WHERE SHN_DELETE = 0 " .
			" ORDER BY SHN_PRIORITY ";
		$this->m_db->execute($sql);
		$row = $this->m_db->fetchAll();
		$cnt = 0;
		foreach ($row as $rows){
			$sql = 	"UPDATE MST_SHAIN_DATA " . 
						"SET  SHN_PRIORITY = " . cls_db::quoteN($cnt) .
						",SHN_DATE_UPDATE = now()  " . 
						"WHERE " . 
							"SHN_UID = " . cls_db::quoteS($rows['SHN_UID']);
			$this->m_db->execute($sql);
			$cnt++;
		}
		
		return true;

	}
	//////////////////////////////////////////////////////////////////////////////
	// CCT情報を削除
	// 引数:
	// 戻値:戻値:true Or false
	function doDelete ($p_SHN_ID) {
		$sql = 	"UPDATE MST_SHAIN_DATA " . 
					"SET  SHN_DELETE = 1 ," . 
					"SHN_DATE_DELETE = now()  " . 
					"WHERE " . 
						"SHN_UID = " . cls_db::quoteS($p_SHN_ID);
		$this->m_db->execute($sql);
		//そして優先順位振りなおし
		$this->renumPriority();
		$sql = 	"SELECT SHN_NO FROM MST_SHAIN_DATA " . 
					"WHERE " . 
						"SHN_UID = " . cls_db::quoteS($p_SHN_ID);
		$this->m_db->execute($sql);
		$row = $this->m_db->fetchRow();
		
		
		
		//関連内訳削除
//		$sql = 	"UPDATE MST_UCHIWAKE_DATA " . 
//					"SET  UCW_DELETE = 1 ," . 
//					"UCW_DATE_DELETE = now()  " . 
//					"WHERE " . 
//						"UCW_SHN_NO = " . cls_db::quoteS($row['SHN_NO']);
//		$this->m_db->execute($sql);
	
		$this->m_db->commit();

		return true;

	}
	////////////////////////////////////////////////////////////////////////////
	// TOP情報エラーチェック
	// 引数:データ
	// 戻値:エラー配列
	function isValidData () {
		$ERR = array();
		if (strlen($this->m_data['SHN_NAME'])==0) {
			$ERR['SHN_NAME'] = '社員名:必須入力です';
		}else{
			if (!isValidRangeS($this->m_data['SHN_NAME'],1,20,true)){
				$ERR['SHN_NAME'] =  '社員名:'.sprintf(OVER_ERROR,20,10);
			}
		}
		if (strlen($this->m_data['SHN_NICKNAME'])==0) {
			$ERR['SHN_NICKNAME'] = '略称:必須入力です';
		}else{
			if (!isValidRangeS($this->m_data['SHN_NICKNAME'],1,20,true)){
				$ERR['SHN_NICKNAME'] =  '略称:'.sprintf(OVER_ERROR,20,10);
			}
		}
		
		return $ERR;
	}
	//////////////////////////////////////////////////////////////////////////////
	// デストラクタ
	// 引数:
	// 戻値:true
	function close () {
		$this->m_db->close();
		return true;
	}
}
?>