<?php
// 管理画面ユーザパスワード
$_ADMIN = array(
	'kii' => array('1570','喜井'),
	'hamana' => array('ha38ma55','濱名')
);
//見積書番号頭文字と相手先名
$_CUSTOMER = array(
	'A' => '三菱電機プラントエンジニアリング株式会社',
	'B' => '三菱電機システムサービス株式会社',
	'C' => '菱神電子エンジニアリング株式会社',
	'D' => '京急電機株式会社発変電部',
	'E' => '株式会社関電工',
	'F' => ''
);


define('NEED_ERROR','必須入力です。');
define('DATE_ERROR','日付の指定が不正です。');
define('OVER_ERROR','文字数オーバーです。(半角%d文字、全角%d文字以内で入力してください)');
define('OVER_ERROR2','文字数オーバーです。(半角%d文字以内で入力してください)');
define('OVER_ERROR3','入力内容が不正です。(%s%d文字で入力してください)');
define('NUM_ERROR','数値範囲外です。(%sから%sの範囲で入力してください)');

//一覧件数
define('CNT_EST_LIST',20);

$_REPORT = array(
	0 => '無し',
	1 => '有り'
);
$_CLIP = array(
	1 => '1.添付',
	2 => '2.別途提出'
);

$_FLG_VALUE = array(
	0 => '',
	1 => '<span class="icon-ok"></span>'
);

$_OUTPUT_SPAN = array(
	0 => '5日分',
	1 => '7日分',
	2 => '1ページ分',
	3 => '2ページ分',
	4 => '3ページ分',
	5 => '6ページ分'
);

?>