<?php

/***************************************************************************
 * Name 		:class.cls_vehicle.inc
 * Description 		:基本情報クラス
 * Include		:
 * Trigger		:
 * Create		:2010/02/01 Brico Suzuki
 * LastModify	:
 *
 *
 *
 **************************************************************************/

class cls_vehicle {
	var $m_db;			// cls_db
	var $m_data;		// データ配列
	var $m_where;		// where配列
	var $m_uid;		// VHC_ID
	var $m_admin;	// 管理画面：1 コンテンツ表示:0

	//////////////////////////////////////////////////////////////////////////////
	// コンストラクタ
	// 引数:
	function cls_vehicle () {
		$this->m_db = new cls_db();
	}

	//////////////////////////////////////////////////////////////////////////////
	// 新規ユニークID取得
	// static
	// 引数:
	// 戻値:ユニークID
	function getUniqId () {
		return uniqid('', true);
	}

	//////////////////////////////////////////////////////////////////////////////
	// UIDセット
	// 引数:
	// 戻値:true Or false
	function setUid ($p_uid) {
		if (strlen($p_uid) == 0) {
			return false;
		}
		$this->m_uid = $p_uid;
		return true;
	}


	//////////////////////////////////////////////////////////////////////////////
	// データ配列セット
	// 引数:データ配列
	// 戻値:true
	function setData ($p_data, $p_admin) {
		$this->m_data = $p_data;
		$this->m_admin = $p_admin;
		return true;
	}

	//////////////////////////////////////////////////////////////////////////////
	// UID取得
	// 引数:
	// 戻値:UID
	function getUid () {
		return $this->m_uid;
	}
	//////////////////////////////////////////////////////////////////////////////
	// VHC情報を取得
	// 引数:
	// 戻値:ENT情報配列 Or false
	function getInfo () {
		$sql = 	"SELECT " . 
					"VHC.VHC_UID," .
					"VHC.VHC_NO," .
					"VHC.VHC_PRIORITY," .
					"VHC.VHC_NAME," .
					"VHC.VHC_NICKNAME," .
					"VHC.VHC_DELETE," .
					"VHC.VHC_DATE_DELETE," .
					"VHC.VHC_DATE_CREATE," .
					"VHC.VHC_DATE_UPDATE " .
				"FROM " . 
					"MST_VEHICLE_DATA VHC " . 
					$this->m_where . 
				" ORDER BY " .
					"VHC.VHC_NO DESC " .
				" LIMIT 1";
		$this->m_db->execute($sql);
		unset($this->m_where);
		if ($row = $this->m_db->fetchrow()) {
			return $row;
		} else {
			return false;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	// Where句組み立て
	function setWhere() {
		unset($this->m_where);
		$arrWhere = array();
		//削除のもの以外
		$arrWhere[] = "VHC.VHC_DELETE <> 1 ";
		if ($this->m_admin == 1){
			//管理画面用where
			// UID
			if (strlen($this->m_data['VHC_UID']) > 0) {
				$arrWhere[] = "VHC.VHC_UID = " . cls_db::quote($this->m_data['VHC_UID']);
			}
			// UID
			if (strlen($this->m_data['uid']) > 0) {
				$arrWhere[] = "VHC.VHC_UID = " . cls_db::quote($this->m_data['uid']);
			}
		}

		if (count($arrWhere) > 0) {
			$this->m_where = " WHERE " . " (" . implode(") AND (", $arrWhere) . ") ";
		}
		
		return true;
	}


	//////////////////////////////////////////////////////////////////////////////
	// VHC情報を取得
	// 引数:
	// 戻値:ENT情報配列 Or false
	function getList () {
		$sql = 	"SELECT " . 
					"VHC.VHC_UID," .
					"VHC.VHC_NO," .
					"VHC.VHC_PRIORITY," .
					"VHC.VHC_NAME," .
					"VHC.VHC_NICKNAME," .
					"VHC.VHC_DELETE," .
					"VHC.VHC_DATE_DELETE," .
					"VHC.VHC_DATE_CREATE," .
					"VHC.VHC_DATE_UPDATE " .
				"FROM " . 
					"MST_VEHICLE_DATA VHC " . 
					$this->m_where . 
				" ORDER BY " .
					"VHC.VHC_PRIORITY ";
		$this->m_db->execute($sql);
		unset($this->m_where);
		if ($row = $this->m_db->fetchAll()) {
			return $row;
		} else {
			return false;
		}
	}
	//////////////////////////////////////////////////////////////////////////////
	// VHC情報を取得
	// 引数:
	// 戻値:ENT情報配列 Or false
	function getAllList () {
		$sql = 	"SELECT " . 
					"VHC.VHC_NO," .
					"VHC.VHC_NAME " .
				"FROM " . 
					"MST_VEHICLE_DATA VHC " . 
				"WHERE VHC_DELETE = 0 " .
				" ORDER BY " .
					"VHC.VHC_PRIORITY ";
		$this->m_db->execute($sql);
		if ($row = $this->m_db->fetchAll()) {
			foreach ($row as $rows){
				$rtn[$rows['VHC_NO']] = $rows['VHC_NAME'];
			}
			return $rtn;
		} else {
			return false;
		}
	}
	//////////////////////////////////////////////////////////////////////////////
	// VHC情報更新
	// 引数:
	// 戻値:true Or false
	function doUpdate () {

		// VHCデータ
		$arrSql 			= array();
		$arrSql['VHC_DATE_UPDATE'] 			= "now()";
		$arrSql['VHC_NAME'] 		= cls_db::quoteS($this->m_data['VHC_NAME']);
		$arrSql['VHC_NICKNAME'] 		= cls_db::quoteS($this->m_data['VHC_NICKNAME']);

		$arr = array();
		foreach ($arrSql as $key => $val) {
			array_push($arr, $key . "=" . $val);
		}

		$sql = 	"UPDATE MST_VEHICLE_DATA SET " . 
						implode(',', $arr) . " " . 
					"WHERE " . 
						"VHC_UID = " . cls_db::quoteS($this->m_data['VHC_UID']);
		$this->m_db->execute($sql);
		
		$this->m_db->commit();
		
		
		return true;
	}


	//////////////////////////////////////////////////////////////////////////////
	// VHC情報登録(コンテンツ登録時に自動作成)
	// 引数:
	// 戻値:true Or false
	function doInsert () {
		//新規ID取得
		$this->setUid($this->getUniqId());
		//新規コンテンツID
		$contid = $this->m_db->getNextVal('VHC_NO','MST_VEHICLE_DATA VHC', "");
		//新規プライオリティ
		$prio = $this->m_db->getNextVal('VHC_PRIORITY','MST_VEHICLE_DATA VHC', " WHERE VHC.VHC_DELETE = 0 ");
		// 基本データ
		$arrSql 			= array();
		$arrSql['VHC_UID'] 				= cls_db::quoteS($this->getUid());
		$arrSql['VHC_DATE_CREATE'] 			= "now()";
		$arrSql['VHC_DATE_UPDATE'] 			= "NULL";
		$arrSql['VHC_NO'] 			= cls_db::quoteN($contid);
		$arrSql['VHC_PRIORITY'] 			= cls_db::quoteN($prio);
		$arrSql['VHC_NAME'] 		= cls_db::quoteS($this->m_data['VHC_NAME']);
		$arrSql['VHC_NICKNAME'] 		= cls_db::quoteS($this->m_data['VHC_NICKNAME']);

		$sql = 	"INSERT INTO MST_VEHICLE_DATA (" . 
						implode(',', array_keys($arrSql)) . 
					") VALUES ( " . 
						implode(',', array_values($arrSql)) . 
					" ) ";
					
		if ($this->m_db->execute($sql)===false) {
			$this->m_db->rollback();
		} else {
			$this->m_db->commit();

		}

		return $contid;

	}
	


	//////////////////////////////////////////////////////////////////
	// CAT情報表示順更新
	// 引数:
	// 戻値:Boolean true or false
	function modPriority () {
		if (
			(strlen($this->m_data['uid']) == 0 ) ||
			(strlen($this->m_data['pri']) == 0 ) ||
			(strlen($this->m_data['move'])== 0 )
		) {
			return true;
		}
		//一番上位なのにさらにupが押されたらなにも処理しない
		if (
			($this->m_data['pri'] == '0')&&($this->m_data['move'] == 'up')
		){
			return true;
		}
		//一番下位なのにさらにdownが押されたらなにも処理しない
		$sql = 	"SELECT MAX(VHC.VHC_PRIORITY) MAXID " .
				"FROM " . 
					"MST_VEHICLE_DATA VHC " . 
				"WHERE VHC_DELETE = 0 ";
		$this->m_db->execute($sql);
		if ($row = $this->m_db->fetchrow()) {
			$maxid =  $row['MAXID'];
		} else {
			$maxid = 0;
		}
		if (
			($this->m_data['pri'] == $maxid)&&($this->m_data['move'] == 'down')
		){
			return true;
		}
		//表示順更新
		if ($this->m_data['move'] == 'up'){
			$pri = $this->m_data['pri'] - 1;
		}else if ($this->m_data['move'] == 'down'){
			$pri = $this->m_data['pri'] + 1;
		}
		//移動先のカテゴリーのプライオリティを先に更新
		$arrSql 		= array();
		$arrSql['VHC_PRIORITY'] 		= cls_db::quoteN($this->m_data['pri']);
		$arrSql['VHC_DATE_UPDATE'] 	= "now()";

		$where 		= "VHC_PRIORITY = ". cls_db::quoteN($pri) .
				 " AND VHC_DELETE = 0 ";

		$arr = array();
		foreach ($arrSql as $key => $val) {
			array_push($arr, $key . "=" . $val);
		}

		$sql = 	"UPDATE MST_VEHICLE_DATA SET " . 
						implode(',', $arr) . " " . 
					"WHERE " . $where;
		$this->m_db->execute($sql);
		//移動対象のカテゴリーのプライオリティを次に更新
		$arrSql 		= array();
		$arrSql['VHC_PRIORITY'] 		= cls_db::quoteN($pri);
		$arrSql['VHC_DATE_UPDATE'] 	= "now()";

		$where 		= "VHC_UID = ". cls_db::quoteS($this->m_data['uid']) .
				 " AND VHC_DELETE = 0 ";

		$arr = array();
		foreach ($arrSql as $key => $val) {
			array_push($arr, $key . "=" . $val);
		}

		$sql = 	"UPDATE MST_VEHICLE_DATA SET " . 
						implode(',', $arr) . " " . 
					"WHERE " . $where;
		$this->m_db->execute($sql);
		
		$this->m_db->commit();
		return true;

	}
	//////////////////////////////////////////////////////////////////////////////
	// プライオリティ振りなおし
	// 引数:
	// 戻値:戻値:true Or false
	function renumPriority () {
		//優先順位振りなおし
		$sql = "SELECT VHC_UID FROM MST_VEHICLE_DATA WHERE VHC_DELETE = 0 " .
			" ORDER BY VHC_PRIORITY ";
		$this->m_db->execute($sql);
		$row = $this->m_db->fetchAll();
		$cnt = 0;
		foreach ($row as $rows){
			$sql = 	"UPDATE MST_VEHICLE_DATA " . 
						"SET  VHC_PRIORITY = " . cls_db::quoteN($cnt) .
						",VHC_DATE_UPDATE = now()  " . 
						"WHERE " . 
							"VHC_UID = " . cls_db::quoteS($rows['VHC_UID']);
			$this->m_db->execute($sql);
			$cnt++;
		}
		
		return true;

	}
	//////////////////////////////////////////////////////////////////////////////
	// CCT情報を削除
	// 引数:
	// 戻値:戻値:true Or false
	function doDelete ($p_VHC_ID) {
		$sql = 	"UPDATE MST_VEHICLE_DATA " . 
					"SET  VHC_DELETE = 1 ," . 
					"VHC_DATE_DELETE = now()  " . 
					"WHERE " . 
						"VHC_UID = " . cls_db::quoteS($p_VHC_ID);
		$this->m_db->execute($sql);
		//そして優先順位振りなおし
		$this->renumPriority();
		$sql = 	"SELECT VHC_NO FROM MST_VEHICLE_DATA " . 
					"WHERE " . 
						"VHC_UID = " . cls_db::quoteS($p_VHC_ID);
		$this->m_db->execute($sql);
		$row = $this->m_db->fetchRow();
			
		$this->m_db->commit();

		return true;

	}
	////////////////////////////////////////////////////////////////////////////
	// TOP情報エラーチェック
	// 引数:データ
	// 戻値:エラー配列
	function isValidData () {
		$ERR = array();
		if (strlen($this->m_data['VHC_NAME'])==0) {
			$ERR['VHC_NAME'] = '車両名:必須入力です';
		}else{
			if (mb_strlen($this->m_data['VHC_NAME']) > 30){
				$ERR['VHC_NAME'] =  '車両名:'.sprintf(OVER_ERROR,30,30);
			}
		}
		if (strlen($this->m_data['VHC_NICKNAME'])==0) {
			$ERR['VHC_NICKNAME'] = '略称:必須入力です';
		}else{
			if (!isValidRangeS($this->m_data['VHC_NICKNAME'],1,8,true)){
				$ERR['VHC_NICKNAME'] =  '略称:'.sprintf(OVER_ERROR,8,4);
			}
		}
		
		return $ERR;
	}
	//////////////////////////////////////////////////////////////////////////////
	// デストラクタ
	// 引数:
	// 戻値:true
	function close () {
		$this->m_db->close();
		return true;
	}
}
?>