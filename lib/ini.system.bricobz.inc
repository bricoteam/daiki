<?php

/***************************************************************************
 * Name 		:ini.system.inc
 * Description 		:システム定数
 * Include		:
 * Trigger		:
 * Create		:2012/04/24 Brico Suzuki
 * LastModify	:
 *
 *
 *
 **************************************************************************/


//////////////////////////////////////////////////////////////////////
// ディレクトリ関連

// ベースディレクトリ★
//define('DIR_BASE', '/home/daiki');
define('DIR_BASE', '/var/www/sub/daiki');
//HTMLファイルディレクトリ★
define('DIR_HTML', DIR_BASE . '/html/');


//////////////////////////////////////////////////////////////////////
// URL関連
//////////////////////////////////////////////////////////////////////
// SSL 0:使用しない 1:使用する
define('SSL_USE', 1);
define('URL_SSL_BASE', 'http://192.168.1.10/');
// DEMO 0:使用しない 1:使用する
define('DEMO_USE', '1');


// ベースURL★
define('URL_BASE', 'http://daiki.brico.bz/');
// 管理画面URL★
define('URL_MANAGE', URL_BASE.'manage/');

//commonフォルダ★
define('URL_COMMON', URL_MANAGE.'common/');

//ログインURL
define('URL_LOGIN', URL_MANAGE.'login.php');
define('URL_LOGOUT', URL_MANAGE.'logout.php');


//人件費の区分NO
//define('LABOR_KBN_NO',3);
//honban
define('LABOR_KBN_NO',1);

//////////////////////////////////////////////////////////////////////
// データベース関連

// 1 = デバックモード
//define('DB_DEBUG', 0);
define('DB_DEBUG', 0);
// クライアントエンコーディング
define('DB_ENCODING', 'UTF-8');
//★
//define('DB_HOST', 'localhost');
define('DB_HOST', 'localhost');
//★
//define('DB_NAME', 'dbname');
define('DB_NAME', 'daiki_db');
//★
//define('DB_USER', 'dbuser');
define('DB_USER', 'daiki_db_user');
//★
//define('DB_PASS', 'password');
define('DB_PASS', '2012denki');

//////////////////////////////////////////////////////////////////////
// ユーザー定数
//メールドメイン

//サイトタイトル
define('SITE_NAME', 'DAIKI見積書作成システム');

//////////////////////////////////////////////////////////////////////
// セッションID
define('CONST_SES_ID','_SesDai2012');

//////////////////////////////////////////////////////////////////////
// エラー処理関連 

// 1 = デバッグ
//define('ERR_DEBUG_MODE', 0);
define('ERR_DEBUG_MODE', 1);

// エラーログファイル★
//define('ERR_LOG_FILE', DIR_HTML . '/log/%s.log');
define('ERR_LOG_FILE', DIR_BASE . '/log/%s.log');

// 開発サーバーIP
$_TEST_SERVER_IP = array(
	'192.168.1.202',
	'118.22.56.28',
	'brifla.bricoleur.in'
);

// エラー時システム名
define('ERR_SYSTEM_NAME', "DAIKI見積書作成システム");

// システムエラー送信先
define("ERR_SYSTEM_MAIL", 'rei.suzu@gmail.com');

// エラー関連定数
define('ERR_ERROR', E_ERROR);
define('ERR_WARNING', E_WARNING);
define('ERR_PARSE', E_PARSE);
define('ERR_NOTICE', E_NOTICE);
define('ERR_CORE_ERROR', E_CORE_ERROR);
define('ERR_CORE_WARNING', E_CORE_WARNING);
define('ERR_COMPILE_ERROR', E_COMPILE_ERROR);
define('ERR_COMPILE_WARNING', E_COMPILE_WARNING);
define('ERR_USER_ERROR', E_USER_ERROR);
define('ERR_USER_WARNING', E_USER_WARNING);
define('ERR_USER_NOTICE', E_USER_NOTICE);

$_ERROR_LEVEL = array(
	ERR_ERROR 			=> 'E_ERROR',
	ERR_WARNING 		=> 'E_WARNING',
	ERR_PARSE 			=> 'E_PARSE',
	ERR_NOTICE 			=> 'E_NOTICE',
	ERR_CORE_ERROR 		=> 'E_CORE_ERROR',
	ERR_CORE_WARNING 	=> 'E_CORE_WARNING',
	ERR_COMPILE_ERROR 	=> 'E_COMPILE_ERROR',
	ERR_COMPILE_WARNING => 'E_COMPILE_WARNING',
	ERR_USER_ERROR 		=> 'E_USER_ERROR',
	ERR_USER_WARNING 	=> 'E_USER_WARNING',
	ERR_USER_NOTICE 	=> 'E_USER_NOTICE',
	ERR_STRICT 	=> 'E_STRICT'

);

// エラー出力先定数
define('ERR_OUT_STDOUT', 1);
define('ERR_OUT_DB', 2);
define('ERR_OUT_FILE', 3);
define('ERR_OUT_MAIL', 4);

?>
