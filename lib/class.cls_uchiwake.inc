<?php

/***************************************************************************
 * Name 		:class.cls_uchiwake.inc
 * Description 		:基本情報クラス
 * Include		:
 * Trigger		:
 * Create		:2010/02/01 Brico Suzuki
 * LastModify	:
 *
 *
 *
 **************************************************************************/

class cls_uchiwake {
	var $m_db;			// cls_db
	var $m_data;		// データ配列
	var $m_where;		// where配列
	var $m_uid;		// UCW_ID
	var $m_admin;	// 管理画面：1 コンテンツ表示:0

	//////////////////////////////////////////////////////////////////////////////
	// コンストラクタ
	// 引数:
	function cls_uchiwake () {
		$this->m_db = new cls_db();
	}

	//////////////////////////////////////////////////////////////////////////////
	// 新規ユニークID取得
	// static
	// 引数:
	// 戻値:ユニークID
	function getUniqId () {
		return uniqid('', true);
	}

	//////////////////////////////////////////////////////////////////////////////
	// UIDセット
	// 引数:
	// 戻値:true Or false
	function setUid ($p_uid) {
		if (strlen($p_uid) == 0) {
			return false;
		}
		$this->m_uid = $p_uid;
		return true;
	}


	//////////////////////////////////////////////////////////////////////////////
	// データ配列セット
	// 引数:データ配列
	// 戻値:true
	function setData ($p_data, $p_admin) {
		$this->m_data = $p_data;
		$this->m_admin = $p_admin;
		return true;
	}

	//////////////////////////////////////////////////////////////////////////////
	// UID取得
	// 引数:
	// 戻値:UID
	function getUid () {
		return $this->m_uid;
	}
	//////////////////////////////////////////////////////////////////////////////
	// UCW情報を取得
	// 引数:
	// 戻値:ENT情報配列 Or false
	function getInfo () {
		$sql = 	"SELECT " . 
					"UCW.UCW_UID," .
					"UCW.UCW_NO," .
					"UCW.UCW_KBN_NO," .
					"UCW.UCW_PRIORITY," .
					"UCW.UCW_NAME," .
					"UCW.UCW_FORMAT," .
					"UCW.UCW_NUM," .
					"UCW.UCW_UNIT," .
					"UCW.UCW_PRICE," .
					"UCW.UCW_TOTAL," .
					"UCW.UCW_DELETE," .
					"UCW.UCW_DATE_DELETE," .
					"UCW.UCW_DATE_CREATE," .
					"UCW.UCW_DATE_UPDATE " .
				"FROM " . 
					"MST_UCHIWAKE_DATA UCW " . 
					$this->m_where . 
				" ORDER BY " .
					"UCW.UCW_NO DESC " .
				" LIMIT 1";
		$this->m_db->execute($sql);
		unset($this->m_where);
		if ($row = $this->m_db->fetchrow()) {
			return $row;
		} else {
			return false;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	// Where句組み立て
	function setWhere() {
		unset($this->m_where);
		$arrWhere = array();
		//削除のもの以外
		$arrWhere[] = "UCW.UCW_DELETE <> 1 ";
		if (strlen($this->m_data['SEL_UCW_KBN_NO']) > 0) {
			$arrWhere[] = "UCW.UCW_KBN_NO = " . cls_db::quote($this->m_data['SEL_UCW_KBN_NO']);
		}
		if ($this->m_admin == 1){
			//管理画面用where
			// UID
			if (strlen($this->m_data['uid']) > 0) {
				$arrWhere[] = "UCW.UCW_UID = " . cls_db::quote($this->m_data['uid']);
			}
			if (strlen($this->m_data['UCW_UID']) > 0) {
				$arrWhere[] = "UCW.UCW_UID = " . cls_db::quote($this->m_data['UCW_UID']);
			}
		}

		if (count($arrWhere) > 0) {
			$this->m_where = " WHERE " . " (" . implode(") AND (", $arrWhere) . ") ";
		}
		
		return true;
	}


	//////////////////////////////////////////////////////////////////////////////
	// UCW情報を取得
	// 引数:
	// 戻値:ENT情報配列 Or false
	function getList () {
		$sql = 	"SELECT " . 
					"UCW.UCW_UID," .
					"UCW.UCW_NO," .
					"UCW.UCW_KBN_NO," .
					"UCW.UCW_PRIORITY," .
					"UCW.UCW_NAME," .
					"UCW.UCW_FORMAT," .
					"UCW.UCW_NUM," .
					"UCW.UCW_UNIT," .
					"UCW.UCW_PRICE," .
					"UCW.UCW_TOTAL," .
					"UCW.UCW_DELETE," .
					"UCW.UCW_DATE_DELETE," .
					"UCW.UCW_DATE_CREATE," .
					"UCW.UCW_DATE_UPDATE " .
				"FROM " . 
					"MST_UCHIWAKE_DATA UCW " . 
					$this->m_where . 
				" ORDER BY " .
					"UCW.UCW_PRIORITY ";
		$this->m_db->execute($sql);
		unset($this->m_where);
		if ($row = $this->m_db->fetchAll()) {
			return $row;
		} else {
			return false;
		}
	}
	//////////////////////////////////////////////////////////////////////////////
	// UCW情報更新
	// 引数:
	// 戻値:true Or false
	function doUpdate () {

		//カテゴリ変更確認
		$sql = "SELECT UCW_KBN_NO FROM MST_UCHIWAKE_DATA WHERE UCW_DELETE = 0 " .
			" AND UCW_UID = " . cls_db::quoteS($this->m_data['UCW_UID']);
		$this->m_db->execute($sql);
		$rtn = $this->m_db->fetchrow();
		// UCWデータ
		$arrSql 			= array();
		$arrSql['UCW_DATE_UPDATE'] 			= "now()";
		$arrSql['UCW_KBN_NO'] 		= cls_db::quoteN($this->m_data['UCW_KBN_NO']);
		$arrSql['UCW_NAME'] 		= cls_db::quoteS($this->m_data['UCW_NAME']);
		$arrSql['UCW_FORMAT'] 		= cls_db::quoteS($this->m_data['UCW_FORMAT']);
		//$arrSql['UCW_NUM'] 		= cls_db::quoteN($this->m_data['UCW_NUM']);
		$arrSql['UCW_NUM'] 		= nVal($this->m_data['UCW_NUM'],'NULL');
		$arrSql['UCW_UNIT'] 		= cls_db::quoteS($this->m_data['UCW_UNIT']);
		$arrSql['UCW_PRICE'] 		= cls_db::quoteN($this->m_data['UCW_PRICE']);
		$arrSql['UCW_TOTAL'] 		= cls_db::quoteN($this->m_data['UCW_TOTAL']);
		//カテゴリが変更されていたら
		if ($rtn['UCW_KBN_NO'] <> $this->m_data['UCW_KBN_NO']){
			$prio = $this->m_db->getNextVal('UCW_PRIORITY','MST_UCHIWAKE_DATA UCW', " WHERE  UCW.UCW_DELETE = 0 AND " .
				 " UCW.UCW_KBN_NO = " . cls_db::quoteN($this->m_data['UCW_KBN_NO']));
			$arrSql['UCW_PRIORITY']		= cls_db::quoteN($prio);
		}

		$arr = array();
		foreach ($arrSql as $key => $val) {
			array_push($arr, $key . "=" . $val);
		}

		$sql = 	"UPDATE MST_UCHIWAKE_DATA SET " . 
						implode(',', $arr) . " " . 
					"WHERE " . 
						"UCW_UID = " . cls_db::quoteS($this->m_data['UCW_UID']);
		$this->m_db->execute($sql);
		
		$this->m_db->commit();
		
		//カテゴリが変更されていたら
		if ($rtn['UCW_KBN_NO'] <> $this->m_data['UCW_KBN_NO']){
			//そして優先順位振りなおし
			$this->m_data['kno'] = $this->m_data['UCW_KBN_NO'];
			$this->renumPriority();
			$this->m_data['kno'] = $rtn['UCW_KBN_NO'];
			$this->renumPriority();
		}
		return true;
	}


	//////////////////////////////////////////////////////////////////////////////
	// UCW情報登録(コンテンツ登録時に自動作成)
	// 引数:
	// 戻値:true Or false
	function doInsert () {
		//新規ID取得
		$this->setUid($this->getUniqId());
		//新規コンテンツID
		$contid = $this->m_db->getNextVal('UCW_NO','MST_UCHIWAKE_DATA UCW', "");
		//新規プライオリティ
		$prio = $this->m_db->getNextVal('UCW_PRIORITY','MST_UCHIWAKE_DATA UCW', " WHERE UCW.UCW_DELETE = 0 AND UCW.UCW_KBN_NO = ".cls_db::quoteN($this->m_data['UCW_KBN_NO']));
		// 基本データ
		$arrSql 			= array();
		$arrSql['UCW_UID'] 				= cls_db::quoteS($this->getUid());
		$arrSql['UCW_DATE_CREATE'] 			= "now()";
		$arrSql['UCW_DATE_UPDATE'] 			= "NULL";
		$arrSql['UCW_NO'] 			= cls_db::quoteN($contid);
		$arrSql['UCW_KBN_NO'] 		= cls_db::quoteN($this->m_data['UCW_KBN_NO']);
		$arrSql['UCW_PRIORITY'] 			= cls_db::quoteN($prio);
		$arrSql['UCW_NAME'] 		= cls_db::quoteS($this->m_data['UCW_NAME']);
		$arrSql['UCW_FORMAT'] 		= cls_db::quoteS($this->m_data['UCW_FORMAT']);
		//$arrSql['UCW_NUM'] 		= cls_db::quoteN($this->m_data['UCW_NUM']);
		$arrSql['UCW_NUM'] 		= nVal($this->m_data['UCW_NUM'],'NULL');
		$arrSql['UCW_UNIT'] 		= cls_db::quoteS($this->m_data['UCW_UNIT']);
		$arrSql['UCW_PRICE'] 		= cls_db::quoteN($this->m_data['UCW_PRICE']);
		$arrSql['UCW_TOTAL'] 		= cls_db::quoteN($this->m_data['UCW_TOTAL']);

		$sql = 	"INSERT INTO MST_UCHIWAKE_DATA (" . 
						implode(',', array_keys($arrSql)) . 
					") VALUES ( " . 
						implode(',', array_values($arrSql)) . 
					" ) ";

		if ($this->m_db->execute($sql)===false) {
			$this->m_db->rollback();
		} else {
			$this->m_db->commit();

		}

		return $contid;

	}
	


	//////////////////////////////////////////////////////////////////
	// CAT情報表示順更新
	// 引数:
	// 戻値:Boolean true or false
	function modPriority () {
		if (
			(strlen($this->m_data['kno']) == 0 ) ||
			(strlen($this->m_data['uid']) == 0 ) ||
			(strlen($this->m_data['pri']) == 0 ) ||
			(strlen($this->m_data['move'])== 0 )
		) {
			return true;
		}
		//一番上位なのにさらにupが押されたらなにも処理しない
		if (
			($this->m_data['pri'] == '0')&&($this->m_data['move'] == 'up')
		){
			return true;
		}
		//一番下位なのにさらにdownが押されたらなにも処理しない
		$sql = 	"SELECT MAX(UCW.UCW_PRIORITY) MAXID " .
				"FROM " . 
					"MST_UCHIWAKE_DATA UCW " . 
				"WHERE UCW_DELETE = 0 ".
				" AND  UCW_KBN_NO = ". cls_db::quoteN($this->m_data['kno']);

		$this->m_db->execute($sql);
		if ($row = $this->m_db->fetchrow()) {
			$maxid =  $row['MAXID'];
		} else {
			$maxid = 0;
		}
		if (
			($this->m_data['pri'] == $maxid)&&($this->m_data['move'] == 'down')
		){
			return true;
		}
		
		//表示順更新
		if ($this->m_data['move'] == 'up'){
			$pri = $this->m_data['pri'] - 1;
		}else if ($this->m_data['move'] == 'down'){
			$pri = $this->m_data['pri'] + 1;
		}
		//移動先のカテゴリーのプライオリティを先に更新
		$arrSql 		= array();
		$arrSql['UCW_PRIORITY'] 		= cls_db::quoteN($this->m_data['pri']);
		$arrSql['UCW_DATE_UPDATE'] 	= "now()";

		$where 		= "UCW_PRIORITY = ". cls_db::quoteN($pri) .
				 " AND UCW_DELETE = 0 ".
				" AND  UCW_KBN_NO = ". cls_db::quoteN($this->m_data['kno']);

		$arr = array();
		foreach ($arrSql as $key => $val) {
			array_push($arr, $key . "=" . $val);
		}

		$sql = 	"UPDATE MST_UCHIWAKE_DATA SET " . 
						implode(',', $arr) . " " . 
					"WHERE " . $where;
		$this->m_db->execute($sql);
		//移動対象のカテゴリーのプライオリティを次に更新
		$arrSql 		= array();
		$arrSql['UCW_PRIORITY'] 		= cls_db::quoteN($pri);
		$arrSql['UCW_DATE_UPDATE'] 	= "now()";

		$where 		= "UCW_UID = ". cls_db::quoteS($this->m_data['uid']) .
				 " AND UCW_DELETE = 0 ".
				" AND  UCW_KBN_NO = ". cls_db::quoteN($this->m_data['kno']);

		$arr = array();
		foreach ($arrSql as $key => $val) {
			array_push($arr, $key . "=" . $val);
		}

		$sql = 	"UPDATE MST_UCHIWAKE_DATA SET " . 
						implode(',', $arr) . " " . 
					"WHERE " . $where;
		$this->m_db->execute($sql);
		
		//そして優先順位振りなおし
		$this->renumPriority();

		
		$this->m_db->commit();
		return true;

	}
	//////////////////////////////////////////////////////////////////////////////
	// プライオリティ振りなおし
	// 引数:
	// 戻値:戻値:true Or false
	function renumPriority () {
		//優先順位振りなおし
		$sql = "SELECT UCW_UID FROM MST_UCHIWAKE_DATA WHERE UCW_DELETE = 0 " .
			" AND  UCW_KBN_NO = ". cls_db::quoteN($this->m_data['kno']) .
			" ORDER BY UCW_PRIORITY ";

		$this->m_db->execute($sql);
		$row = $this->m_db->fetchAll();
		$cnt = 0;
		foreach ($row as $rows){
			$sql = 	"UPDATE MST_UCHIWAKE_DATA " . 
						"SET  UCW_PRIORITY = " . cls_db::quoteN($cnt) .
						",UCW_DATE_UPDATE = now()  " . 
						"WHERE UCW_DELETE = 0 AND " . 
							"UCW_UID = " . cls_db::quoteS($rows['UCW_UID']);
			$this->m_db->execute($sql);
			$cnt++;
		}
		
		return true;

	}
	//////////////////////////////////////////////////////////////////////////////
	// CCT情報を削除
	// 引数:
	// 戻値:戻値:true Or false
	function doDelete ($p_UCW_ID) {
		$sql = 	"UPDATE MST_UCHIWAKE_DATA " . 
					"SET  UCW_DELETE = 1 ," . 
					"UCW_DATE_DELETE = now()  " . 
					"WHERE " . 
						"UCW_UID = " . cls_db::quoteS($p_UCW_ID);
		$this->m_db->execute($sql);
		//そして優先順位振りなおし
		$this->renumPriority();
	
		$this->m_db->commit();

		return true;

	}
	////////////////////////////////////////////////////////////////////////////
	// TOP情報エラーチェック
	// 引数:データ
	// 戻値:エラー配列
	function isValidData () {
		$ERR = array();
		if (strlen($this->m_data['UCW_KBN_NO'])==0) {
			$ERR['UCW_KBN_NO'] = '区分:必須入力です';
		}
		if (strlen($this->m_data['UCW_NAME'])==0) {
			$ERR['UCW_NAME'] = '内訳項目名:必須入力です';
		}else{
			if (!isValidRangeS($this->m_data['UCW_NAME'],1,50,true)){
				$ERR['UCW_NAME'] =  '内訳項目名:'.sprintf(OVER_ERROR,50,25);
			}
		}
		if (!isValidRangeS($this->m_data['UCW_FORMAT'],0,40,true)){
			$ERR['UCW_FORMAT'] =  '形式・形状:'.sprintf(OVER_ERROR,40,20);
		}
		//if ((strlen($this->m_data['UCW_NUM'])>0)&&(!isValidRangeN($this->m_data['UCW_NUM'],0,999))){
		//if ((strlen($this->m_data['UCW_NUM'])>0)&&((!is_numeric($this->m_data['UCW_NUM']))||($this->m_data['UCW_NUM'] < -1000)||($this->m_data['UCW_NUM'] > 1000))){
		if ((strlen($this->m_data['UCW_NUM'])>0)&&((!is_numeric($this->m_data['UCW_NUM']))||($this->m_data['UCW_NUM'] < -10000)||($this->m_data['UCW_NUM'] > 10000))){
			//$ERR['UCW_NUM'] =  '数量:'.sprintf(NUM_ERROR,'0','999');
			$ERR['UCW_NUM'] =  '数量:'.sprintf(NUM_ERROR,'0','9999');
		}
		if (!isValidRangeS($this->m_data['UCW_UNIT'],0,10,true)){
			$ERR['UCW_UNIT'] =  '単位:'.sprintf(OVER_ERROR,10,5);
		}
		if ((strlen($this->m_data['UCW_PRICE'])>0)&&(!isValidRangeN($this->m_data['UCW_PRICE'],-999999999,999999999))){
			$ERR['UCW_PRICE'] =  '単価:'.sprintf(NUM_ERROR,'0','999,999,999');
		}
		if ((strlen($this->m_data['UCW_TOTAL'])>0)&&(!isValidRangeN($this->m_data['UCW_TOTAL'],-9999999999,9999999999))){
			$ERR['UCW_TOTAL'] =  '金額:'.sprintf(NUM_ERROR,'0','9,999,999,999');
		}
		if ((strlen($this->m_data['UCW_TOTAL'])==0)&&(strlen($this->m_data['UCW_PRICE'])==0)){
			$ERR['UCW_TOTAL'] =  '単価･金額:単価もしくは金額のどちらか一方は必須入力です';
		}
		
		return $ERR;
	}
	////////////////////////////////////////////////////////////////////////////
	// TOP情報エラーチェック
	// 引数:データ
	// 戻値:エラー配列
	function isValidSearchData () {
		$ERR = array();
		if (strlen($this->m_data['SEL_UCW_KBN_NO'])==0) {
			$ERR['SEL_UCW_KBN_NO'] = '区分:必須入力です';
		}
		
		return $ERR;
	}
	//////////////////////////////////////////////////////////////////////////////
	// デストラクタ
	// 引数:
	// 戻値:true
	function close () {
		$this->m_db->close();
		return true;
	}
}
?>