<?php
	/***************************************************************************
	 * Name 		:func.common.inc
	 * Description 		:共通関数郡
	 * Include		:ini.system.inc
	 * 			 ini.definedarr.inc
	 * 			 class.cls_db.inc
	 * 			 func.errorHandler.inc
	 * Trigger		:
	 * Create		:2009/07/02 Brico Suzuki
	 * LastModify	:
	 *
	 *
	 *
	 **************************************************************************/

	require_once 'ini.system.inc';
	require_once 'ini.definedarr.inc';
	require_once 'class.cls_db.inc';
	require_once 'func.errorHandler.inc';


	////////////////////////////////////////////////////////////////////////////////////////////////
	// 数値かどうかチェック
	// 引数：文字列
	// 戻値：true Or false
	function isValidNum ($p_value) {
		//if (strlen($p_value) == 0 || preg_match('/[^0-9\-]/', $p_value)) {
		if (strlen($p_value) == 0 || !preg_match('/^[-]?[0-9]+(\.[0-9]+)?$/', $p_value)) {
			return false;
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// NULL to ZERO
	// 引数: 対象文字列
	// 戻値: 対象文字列 Or 0
	function NZ ($p_num) {
		if (strlen($p_num) == 0 || preg_match('/[^0-9]/', $p_num)) {
			return 0;
		} else {
			return intval($p_num);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// NULL to NULL
	// 引数: 対象文字列
	// 戻値: 対象文字列 Or NULL
	function NN ($p_num) {
		if (strlen($p_num) == 0 || preg_match('/[^0-9]/', $p_num)) {
			return "";
		} else {
			return intval($p_num);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 対象文字列が空なら置換文字列で返す
	// 引数:対象文字列
	//		置換文字列
	// 戻値:文字列
	function replaceBlank ($p_val, $p_replace) {
		if (strlen($p_val) == 0) {
			return $p_replace;
		} else {
			return $p_val;
		}
	}
	////////////////////////////////////////////////////////////////////////////////////////////////
	// エラーメッセージを出力
	function putError ($p_msg, $p_next = null){
		if (is_null($p_next)) {
			$url = '<a href="javascript:history.back()">戻る</a>';
		} else {
			$url = $p_next;
		}

		print '<br><br><center>';
		print htmlspecialchars($p_msg)."<BR>";
		print $url;
		print '<center><br><br>';
		exit;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 入力エラーHTMLを返す
	// 引数:エラー文字列(配列可)
	// 戻値:HTML
	function outError($p_val){
		if (is_array($p_val)) {
			$err = implode('<br>', $p_val);
		} else {
			$err = $p_val;
		}

		if (strlen($err) == 0) {
			return;
		}

		$html = '<p style="color: rgb(255, 0, 0);">' . $err . '</p>';
		return $html;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////
	// 入力エラーHTMLを返す
	// 引数: Mix エラー文字列(配列可)
	// 		 Boolean <br>をつけるかどうか
	// 戻値: String HTML
	function showError4Manage($p_val, $p_br = false){
		if (is_array($p_val)) {
			$err = implode('<br>', $p_val);
		} else {
			$err = $p_val;
		}

		if (strlen($err) == 0) {
			return;
		}

		$html = '<p id="message">' . $err . '</p>';
		if ($p_br) {
			$html .= '<br />';
		}
		return $html;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////
	// 入力エラーHTMLを返す (センタリング版)
	// 引数: Mix エラー文字列(配列可)
	// 戻値: String HTML
	function showError24Manage($p_val, $p_br = false){
		return '<div align="center">' . showError4Manage($p_val,$p_br) . "</div><br>";
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Location
	// 引数:URL
	// 戻値:失敗時 false
	function doLocation ($p_uri) {
//		if (strlen($p_uri) == 0 || headers_sent()) {
		if (strlen($p_uri) == 0) {
			return false;
		}
		header('Location: ' . $p_uri);
		exit();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 開発サーバーかどうか
	// 引数:
	// 戻値:true Or false
	function isDevServer () {
		if (is_array($GLOBALS['_TEST_SERVER_IP']) && in_array($_SERVER['HTTP_HOST'], $_TEST_SERVER_IP)) {
			return true;
		} else {
			return false;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	// httpからのURLを取得
	// 引数:URL
	// 		SSLかどうか
	// 戻値:HTTPSのURL
	function getFullUrl ($p_url, $p_bSSL = true) {
		if (SSL_USE === 1 && $p_bSSL) {
			$url = 'https://' . $_SERVER['HTTP_HOST'] . $p_url;
		} else {
			$url = 'http://' . $_SERVER['HTTP_HOST'] . $p_url;
		}
		return $url;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// HTTPならばHTTPSに遷移
	// 引数:URL
	// 戻値:
	function redirectHttps ($p_uri = null) {
		if (SSL_USE === 1 && $_SERVER['SERVER_PORT'] != 443) {
			if (is_null($p_uri)) {
				$uri = $_SERVER['REQUEST_URI'];
			} else {
				$uri = $p_uri;
			}
			$url = 'https://' . $_SERVER['HTTP_HOST'] . $uri;
		} else {
			return;
		}
		doLocation($url);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 引数でしていされた文字数のランダムなIDをかえす
	// 引数:文字数
	// 戻値:ランダムID
	function getRandomId ($p_num) {
		$numbers = array_merge(range(0,9), range(A,Z), range(a,z));
		srand ((float)microtime()*1000000);
		shuffle($numbers);
		return substr(implode('', $numbers), 0, $p_num);
	}

	////////////////////////////////////////////////////////////////////////////////
	// 文字列が一定文字以上だったら省略して返す
	// 引数:対象文字列
	// 		何文字まで表示するか
	// 		省略した後につける文字列
	// 戻値:html
	function getOmitStr ($p_str, $p_count, $p_omitstr = '…') {
		if (mb_strlen($p_str) <= $p_count) {
			return $p_str;
		}
		$retStr = mb_substr($p_str, 0, ($p_count - 1)) . $p_omitstr;
		return $retStr;
	}
	
	//////////////////////////////////////////////////////////////////////////////
	// 改行コードを除去して<BR>に変換する(nl2brは改行を残すので別)
	// 引数:文字列
	// 戻値:変換後文字列
	function nl4br ($p_string) {
		if (strlen($p_string) == 0) {
			return "";
		} else {
			// 改行文字配列
			$linefeed = array("\r\n", "\n", "\r");
			// 置き換え後の<br>配列
			$html_br  = array("<br>", "<br>", "<br>");
			//置き換え処理
			$string = str_replace($linefeed, $html_br , $p_string);
		}
		return  $string;
	}
  	//////////////////////////////////////////////////////////////////////////////
	// 二重送信防止
	// 引数:
	// 戻値:
	function doubleSubmit($p_token = null){
	    if(!is_null($p_token)){
	        // Tokenの比較
	        if($_SESSION['dstoken'] == $p_token){
	            unset($p_token);
	            unset($_SESSION['dstoken']);
	            return true;
	        }else{
	            return false;
	        }
	    }
	    // Tokenの発行＆Sessionに保持
	    $dstoken = mt_rand (); //ランダムな数値を生成する関数用意
	    $_SESSION['dstoken'] = $dstoken;
	    return $dstoken;
	}

	//////////////////////////////////////////////////////////////////////////////
	// <BR>を改行コードに変換する
	// 引数:文字列
	// 戻値:変換後文字列
	function br4nl ($p_string) {
		if (strlen($p_string) == 0) {
			return "";
		} else {
			//置き換え処理
			$string = str_replace("<br>", "\n" , $p_string);
		}
		return  $string;
	}
	
	//////////////////////////////////////////////////////////////////////////////
	// NULL（長さ0）だったら、指定の文字に変換する
	// 引数:文字列
	// 戻値:変換後文字列
	function nVal ($p_string,$p_rep = "") {
		if (strlen($p_string) == 0) {
			return $p_rep;
		} else {
			return $p_string;
		}
	}
	//////////////////////////////////////////////////////////////////////////////
	// 画像を削除する
	// 引数:ファイル名
	// 戻値:結果
	function unlinkimage ($p_filename,$p_mode) {
		if (strlen($p_filename) == 0) {
			return false;
		} else {
			//削除
			if ($p_mode==0){
				if (unlink($p_filename)){
					$ret = TRUE;
				}else{
					$ret = FALSE;
				}
			}elseif($p_mode==1){
				if (unlink(DIR_IMG_TMP . $p_filename)){
					$ret = TRUE;
				}else{
					$ret = FALSE;
				}
			}
		}
		return  $ret;
	}

	//////////////////////////////////////////////////////////////////////////////
	// 画像を削除する
	// 引数:ファイル名
	// 戻値:結果
	function unlinkfile ($p_filename) {
		if (strlen($p_filename) == 0) {
			return false;
		} else {
			//削除
			$ret = unlink($p_filename);
		}
		return  $ret;
	}
/*function setMemoryForImage( $filename ){
    $imageInfo = getimagesize($filename);
    $MB = 1048576;  // number of bytes in 1M
    $K64 = 65536;    // number of bytes in 64K
    $TWEAKFACTOR = 1.5;  // Or whatever works for you
    $memoryNeeded = round( ( $imageInfo[0] * $imageInfo[1]
                                           * $imageInfo['bits']
                                           * $imageInfo['channels'] / 8
                             + $K64
                           ) * $TWEAKFACTOR
                         );
    //ini_get('memory_limit') only works if compiled with "--enable-memory-limit" also
    //Default memory limit is 8MB so well stick with that.
    //To find out what yours is, view your php.ini file.
    $memoryLimit = 8 * $MB;
    if (function_exists('memory_get_usage') &&
        memory_get_usage() + $memoryNeeded > $memoryLimit)
    {
        $newLimit = $memoryLimitMB + ceil( ( memory_get_usage()
                                            + $memoryNeeded
                                            - $memoryLimit
                                            ) / $MB
                                        );
        ini_set( 'memory_limit', $newLimit . 'M' );
        return true;
    }else{
        return false;
    }
}
*/
	//////////////////////////////////////////////////////////////////////////////
	// 画像をリサイズして一時フォルダに登録する
	// 引数:$_FILES
	// 戻値:結果
	function uploadimage ($p_field,$p_picid,$p_files,$p_x=0,$p_y=0) {
		//画像処理
		//uniqidを生成
		$sId = uniqid('K_');
		//画像処理
		if ($p_files[$p_field]["name"][$p_picid]<>""){
			//拡張子取出
			ereg("^(.+)\.(.+)\$", $p_files[$p_field]["name"][$p_picid], $sKakucho);
			//$sFilename = $p_files[$p_field]["name"];
			$sFilename = $sId.".".$sKakucho[2];
			$sFilepath = DIR_IMG_TMP . $sId.".".$sKakucho[2];
			//ファイル移動
			if ((move_uploaded_file($p_files[$p_field]["tmp_name"][$p_picid],$sFilepath)==False)){
				$err = $p_files[$p_field]['error'];
				if ($err == UPLOAD_ERR_INI_SIZE){
					$arrRtn['ERR'] = "※アップロードされたファイルは、PHPで指定されたファイルサイズ制限を超えています。";
				}else if ($err == UPLOAD_ERR_FORM_SIZE){
					$arrRtn['ERR'] = "※アップロードされたファイルは、HTML フォームで指定されたファイルサイズ制限 を超えています。";
				}else if ($err == UPLOAD_ERR_PARTIAL){
					$arrRtn['ERR'] = "※アップロードされたファイルは一部のみしかアップロードされていません。";
				}else if ($err == UPLOAD_ERR_NO_FILE){
					$arrRtn['ERR'] = "※ファイルはアップロードされませんでした。";
				}else{
					$arrRtn['ERR'] = "※ファイルはエラーによりアップロードされませんでした。(".$err.")";
				}
				return  $arrRtn;
			}else{
				//mode変更
				chmod($sFilepath, 0604);
				//携帯画像作成
				//ファイルタイプで処理振り分け(jpeg,gif,pngのみ許可)
				/*
				$arrImgsize = getimagesize($sFilepath);
				if ($arrImgsize[2] == IMAGETYPE_JPEG) {
					//setMemoryForImage($sFilepath);
					$sImg_in = imagecreatefromjpeg($sFilepath);		//イメージ読み込み
				}else if ($arrImgsize[2] == IMAGETYPE_GIF) {
					$sImg_in = imagecreatefromgif($sFilepath);		//イメージ読み込み
				}else if ($arrImgsize[2] == IMAGETYPE_PNG) {
					$sImg_in = imagecreatefrompng($sFilepath);		//イメージ読み込み
				}else{
					$arrRtn['ERR'] = "※画像形式JPEGのみアップロード可能です。";
					return  $arrRtn;
				}
				$nX = imagesx($sImg_in);				//横幅取得
				$nY = imagesy($sImg_in);				//縦幅取得
				*/
				
				//size check
				/*
				if (strlen($p_x) > 0){
					if ($nX > $p_x){
						$arrRtn['ERR'] = "※サイズがオーバーしています";
						return $arrRtn;
					}
				}
				if (strlen($p_y) > 0){
					if ($nY > $p_y){
						$arrRtn['ERR'] = "※サイズがオーバーしています";
						return $arrRtn;
					}
				}
				*/
				//高さが指定サイズより大きい場合に縮小処理
				/*
				if (( $p_y > 0 )&&( $nY > $p_y )){
					$nResizeY = $p_y;
					$nResizeX = ($p_y * $nX) / $nY;			//縦縮小サイズを求める
					
					$sImg_out = imagecreatetruecolor($nResizeX,$nResizeY);	//変換画像のイメージID作成
					//imagecopyresized($sImg_out,$sImg_in,0,0,0,0,$nResizeX,$nResizeY,$nX,$nY);	//画像の複製とサイズ変更
					ImageCopyResampled($sImg_out,$sImg_in,0,0,0,0,$nResizeX,$nResizeY,$nX,$nY);
					//ファイルタイプで処理振り分け(jpegのみ許可)
					imagejpeg($sImg_out,$sFilepath);		//リサイズして保存
					
					$nX = $nResizeX;				//横幅取得
					$nY = $nResizeY;				//縦幅取得
				}else if (( $p_x > 0 )&&( $nX > $p_x )){
					$nResizeX = $p_x;
					$nResizeY = ($p_x * $nY) / $nX;			//縦縮小サイズを求める
					
					$sImg_out = imagecreatetruecolor($nResizeX,$nResizeY);	//変換画像のイメージID作成
					//imagecopyresized($sImg_out,$sImg_in,0,0,0,0,$nResizeX,$nResizeY,$nX,$nY);	//画像の複製とサイズ変更
					ImageCopyResampled($sImg_out,$sImg_in,0,0,0,0,$nResizeX,$nResizeY,$nX,$nY);
					//ファイルタイプで処理振り分け(jpegのみ許可)
					imagejpeg($sImg_out,$sFilepath);		//リサイズして保存
					
					$nX = $nResizeX;				//横幅取得
					$nY = $nResizeY;				//縦幅取得
				}
				*/
				//イメージの情報を格納
				$arrRtn['IMG_N_NEW'] 	= $sFilename;
				$arrRtn['IMG_H_NEW'] 	= $nY;
				$arrRtn['IMG_W_NEW'] 	= $nX;

				imagedestroy($sImg_in);					//メモリ解放
				imagedestroy($sImg_out);				//メモリ解放
				return  $arrRtn;
			}
		}else{
			$arrRtn['ERR'] = "※画像が指定されていません。";
			return  $arrRtn;
		}
	}
	//////////////////////////////////////////////////////////////////////////////
	// ファイルを一時フォルダに登録する
	// 引数:$_FILES
	// 戻値:結果
	function uploadfile ($p_field, $p_files, $p_id) {
		//uniqidを生成
		//$sId = uniqid();
		//ファイル処理
		if (strlen($p_files[$p_field]["name"])>0){
			//拡張子取出
			//ereg("^(.+)\.(.+)\$", $p_files[$p_field]["name"], $sKakucho);
			$sFilename = $p_files[$p_field]["name"];
			$sFilepath = DIR_FILE_TMP . $p_files[$p_field]["name"];
			//ファイル移動
			if ((move_uploaded_file($p_files[$p_field]["tmp_name"],$sFilepath)===False)){
				$err = $p_files[$p_field]['error'];
				if ($err == UPLOAD_ERR_INI_SIZE){
					$arrRtn['ERR'] = "※アップロードされたファイルは、PHPで指定されたファイルサイズ制限を超えています。";
				}else if ($err == UPLOAD_ERR_FORM_SIZE){
					$arrRtn['ERR'] = "※アップロードされたファイルは、HTML フォームで指定されたファイルサイズ制限 を超えています。";
				}else if ($err == UPLOAD_ERR_PARTIAL){
					$arrRtn['ERR'] = "※アップロードされたファイルは一部のみしかアップロードされていません。";
				}else if ($err == UPLOAD_ERR_NO_FILE){
					$arrRtn['ERR'] = "※ファイルはアップロードされませんでした。";
				}else{
					$arrRtn['ERR'] = "※ファイルはエラーによりアップロードされませんでした。(".$err.")";
				}
				return  $arrRtn;
			}else{
				//mode変更
				chmod($sFilepath, 0604);
				//イメージの情報を格納
				$arrRtn["FILE_NEW"] 	= $sFilename;
				$arrRtn["FILE_SIZE_NEW"] 	= $p_files[$p_field]["size"];
				return  $arrRtn;
			}
		}else{
			$arrRtn['ERR'] = "※ファイルが指定されていません。";
			return  $arrRtn;
		}
	}
	//////////////////////////////////////////////////////////////////////////////
	// 画像を本フォルダに移動して、一時ファイルを削除する
	// 引数:ファイル名1:一時ファイルフルパス
	// 　　 p_type:1:画像,2:PDF
	// 戻値:結果
	function moveimage ($p_filename, $p_forfilename) {
		//移動(PC)
		if (copy($p_filename ,$p_forfilename)){
				//削除(PC)
				unlink($p_filename);
		}
		return  false;
	}
	//////////////////////////////////////////////////////////////////////////////
	// ファイルを本フォルダに移動して、一時ファイルを削除する
	// 引数:ファイル名1:一時ファイル名
	// 戻値:結果
	function movefile ($p_filename) {
		//移動(PC)
		if (copy(DIR_FILE_TMP .$p_filename , DIR_FILE . $p_filename)){
			//削除(PC)
			unlink(DIR_FILE_TMP . $p_filename);
		}
		return  false;
	}

	//////////////////////////////////////////////////////////////////
	// CSV用にデータ加工
	// 引数:String データ
	// 戻値:String 加工後のデータ
	function convertToCsv ($p_string) {
		$string = $p_string;
		$string = str_replace('"', '”', $string);
		$string = str_replace("\r", "", $string);
		$string = str_replace("\n", "", $string);
		$string = '"' . $string . '"';
		return $string;
	}

	//////////////////////////////////////////////////////////////////////////////
	// ログイン
	// 引数:
	// 		USR_USER_ID
	// 		USR_LOGIN_PW
	// 戻値:true Or false
	function doLogin ($p_USR_ID = null, $p_LOGIN_PW = null) {
		$bAuth 		= doAuth($p_USR_ID, $p_LOGIN_PW);
		if ($bAuth) {
			// SESSIONセット
			$_SESSION['LOGIN'] = 1;
			$_SESSION['USER'] = $p_USR_ID;
			return true;
		} else {
			$_SESSION['LOGIN'] = null;
			return false;
		}
	}
	//////////////////////////////////////////////////////////////////////////////
	// ログアウト
	// 引数:
	// 戻値:true Or false
	function doLogout () {
		$_SESSION = array();
		return true;
	}
	//////////////////////////////////////////////////////////////////////////////
	// 認証処理
	// 引数:AUT_USERID
	// 		AUT_USERID
	// 		AUT_PASSWORD
	// 戻値:true Or false
	function doAuth ($p_AUT_USERID = null, $p_AUT_PASSWORD = null) {
		$bAuth = false;
		if (strlen($p_AUT_USERID) == 0 || strlen($p_AUT_PASSWORD) == 0) {
			return false;
		}
		if ( array_key_exists($p_AUT_USERID, $GLOBALS['_ADMIN'])){
			//パスワードチェック
			if ($p_AUT_PASSWORD == $GLOBALS['_ADMIN'][$p_AUT_USERID][0]){
				$bAuth = true;
			}else{
				return false;
			}
		}else{
			$bAuth = false;
		}

		if (!$bAuth) {
			return false;
		}

		return true;
	}
	//////////////////////////////////////////////////////////////////////////////
	// USER_AGENTのセット
	// 引数:
	// 戻値:true Or false
	function setAgent () {
		// SESSIONセット
		$_SESSION['AGENT'] = $_SERVER['HTTP_USER_AGENT'];
		$_SESSION['ADDR'] = $_SERVER['REMOTE_ADDR'];
		return true;
	}
	//////////////////////////////////////////////////////////////////////////////
	// USER_AGENTのチェック
	// 引数:
	// 戻値:true Or false
	function chkAgent () {
		// SESSIONセット
		//if(($_SESSION['AGENT'] == $_SERVER['HTTP_USER_AGENT'])&&($_SESSION['ADDR'] == $_SERVER['REMOTE_ADDR'])){
		if(($_SESSION['AGENT'] == $_SERVER['HTTP_USER_AGENT'])){
			return true;
		}else{
			return false;
		}
	}
	//////////////////////////////////////////////////////////////////////////////
	// ログインしているかどうかのチェック
	// 引数:
	// 戻値:結果
	function isLogin () {
		if ($_SESSION['LOGIN'] == null){
			return	false;
		}else{
			return  true;
		}
	}
	////////////////////////////////////////////////////////////////////////////////
	// 不要タグを除去する<>内が日本語の場合は取り除かない
	// 引数:String タグ入りテキスト
	// 戻値:String タグ除去後テキスト
	function removeTags ($p_text) {
		//タグ除去
		$text = mb_eregi_replace('<([^a-z/].*)>', '&lt;\\1&gt;', $p_text);
		$text = strip_tags($text);
		return $text;
	}
	//////////////////////////////////////////////////////////////////////////////
	// 文字コードを変換する
	// 引数:文字列
	// 戻値:変換後文字列
	function mbConv ($p_string,$p_enc = 0) {
		if (strlen($p_string) == 0) {
			return "";
		} else {
			//置き換え処理
			if ($p_enc == 0){
				$string = str_replace('～','~',mb_convert_encoding($p_string,"UTF-8","auto"));
			}else{
				$string = $p_string;
			}
		}
		//return  $string;
		return $string;
	}
	////////////////////////////////////////////////////////////////////////////////
	// USER-AGENTより絵文字を返す、不明な場合はDocomo形式
	// 引数: String 絵文字名
	// 戻値: String 絵文字 or ""
	function getEmoji($p_name){

		// 絵文字配列定義
		$emoji = array(
			"home" 		=> array("&#63684;","\x1B\$GV\x0F"),
			"free" 		=> array("&#63867;","\x1B\$F6\x0F"),
			"id" 		=> array("&#63868;","\x1B\$FI\x0F"),
			"key" 		=> array("&#63869;","\x1B\$G_\x0F"),
			"enter" 	=> array("&#63870;","\x1B\$F[\x0F"),
			"cl" 		=> array("&#63872;",""),
			"end" 		=> array("&#63837;","\x1B\$E\\\x0F"),
			"search" 	=> array("&#63873;","\x1B\$E4\x0F"),
			"new" 		=> array("&#63874;","\x1B\$F2\x0F"),
			"twin" 		=> array("&#63903;","\x1B\$GU\x0F"),
			"ques" 		=> array("&#63912;","\x1B\$G@\x0F"),
			"pen" 		=> array("&#63826;","\x1B\$O!\x0F"),
			"seal" 		=> array("&#63829;","\x1B\$Fs\x0F"),
			"time" 		=> array("&#63838;","\x1B\$GD\x0F"),
			"mail" 		=> array("&#63863;","\x1B\$E#\x0F"),
			"camera" 	=> array("&#63714;","\x1B\$G(\x0F"),
			"com" 		=> array("&#63685;","\x1B\$GX\x0F"),
			"rule" 		=> array("&#63716;","\x1B\$F)\x0F"),
			"int1" 		=> array("&#63879;","\x1B\$F<\x0F"),
			"int2" 		=> array("&#63880;","\x1B\$F=\x0F"),
			"int3" 		=> array("&#63881;","\x1B\$F>\x0F"),
			"int4" 		=> array("&#63882;","\x1B\$F?\x0F"),
			"int5" 		=> array("&#63883;","\x1B\$F@\x0F"),
			"int6" 		=> array("&#63884;","\x1B\$FA\x0F"),
			"int7" 		=> array("&#63885;","\x1B\$FB\x0F"),
			"int8" 		=> array("&#63886;","\x1B\$FC\x0F"),
			"int9" 		=> array("&#63887;","\x1B\$FD\x0F"),
			"int0" 		=> array("&#63888;","\x1B\$FE\x0F"),
			"tel" 		=> array("&#63720;","\x1B\$G)\x0F"),
			"fax" 		=> array("&#63720;","\x1B\$G+\x0F"),
			"frdl" 		=> array("&#63876;","\x1B\$F1\x0F"),
			"ok" 		=> array("&#63920;","\x1B\$Fm\x0F"),
			"light" 	=> array("&#63904;","\x1B\$E/\x0F"),
			"exc" 		=> array("&#63911;","\x1B\$GA\x0F"),
			"prsnt" 	=> array("&#63718;","\x1B\$E2\x0F"),
			"sun" 		=> array("&#63647;","\x1B\$Gj\x0F"),
			"dia" 		=> array("&#63728;","\x1B\$F-\x0F")
		);

		$clsCareer 	= getCareerInstance();

		if ($clsCareer->isYahoo()) {
			// Yahooケータイ
			$type 	= 1;
		} else {
			// その他の携帯
			$type 	= 0;
		}

		return $emoji[$p_name][$type];
	}
	////////////////////////////////////////////////////////////////////////////////
	// キャリアクラスのインスタンスを取得
	// 引数:
	// 戻値:Object cls_career
	function getCareerInstance () {
		session_start();
		if (strcmp(get_class($_SESSION['class.career']), 'cls_career') == 0) {
			$clsCareer 					= $_SESSION['class.career'];
		} else {
			$clsCareer 					= new cls_career();
			$_SESSION['class.career'] 	= $clsCareer;
		}
		return $clsCareer;
	}
	
	////////////////////////////////////////////////////////////////////////////////
	// ページング処理
	// $p_AllCnt	:全件数
	// $p_LIMIT	:1ページ表示件数
	// $p_page	:現在ページ
	function getPaging ($p_AllCnt, $p_LIMIT, $p_page=1){
		//pagingリンク作成
		//総ページ数
		$nAllPage = ceil($p_AllCnt / $p_LIMIT);
		//Javascript
		$sJscr = 'getData(';
		//画像
		$sLeft  = '<li><a href="javascript:'.$sJscr.($p_page-1).')"><i class="icon-chevron-left"></i>&nbsp;</a></li>';
		$sLeft2   = '<li><a href="javascript:'.$sJscr.'1'.')"><i class="icon-step-backward"></i></a></li>';
		$sLeftx = '<li class="active"><a href="#"><i class="icon-chevron-left"></i></a></li>';
		$sLeft2x  = '<li class="active"><a href="#"><i class="icon-step-backward"></i></a></li>';
		$sRight   = '<li><a href="javascript:'.$sJscr.($p_page+1).')"><i class="icon-chevron-right"></i></a></li>';
		$sRight2  = '<li><a href="javascript:'.$sJscr.$nAllPage.')"><i class="icon-step-forward"></i></a></li>';
		$sRightx  = '<li class="active"><a href="#"><i class="icon-chevron-right"></i></a></li>';
		$sRight2x = '<li class="active"><a href="#"><i class="icon-step-forward"></i></a></li>';
		//最新、前へリンク作成
		if ($p_page == 1){
			//*** 1ページ目はそれ以前のページがないので「<<」「<」は不要
			$sNewest = $sLeft2x;
			$sNew = $sLeftx;
		}else if ($p_page == 2){
			//*** 2ページ目は前のページが1ページしかないので「<」は不要
			$sNewest = $sLeft2x;
			$sNew = $sLeft;
		}else if ($p_page > 2){
			//*** 3ページ目以降は前に2ページ以上あるので「<<」「<」を表示
			$sNewest = $sLeft2;
			$sNew = $sLeft;
		}
		//最後、次へリンク作成
		if ($p_page == $nAllPage){
			//*** 現在ページが最終ページの場合それ以降のページがないので「>」「>>」は不要
			$sOldest = $sRight2x;
			$sOld = $sRightx;
		}else if ($p_page == ($nAllPage - 1)){
			//*** 現在ページが最終ページ-1の場合それ以降のページが1ページしかないので「>>」は不要
			$sOldest = $sRight2x;
			$sOld = $sRight;
		}else if ($p_page < ($nAllPage - 1)){
			//*** 現在ページが最終ページ-1より少ない場合それ以降に2ページ以上あるため「>」「>>」を表示
			$sOldest = $sRight2;
			$sOld = $sRight;
		}else{
			$sOldest = $sRight2x;
			$sOld = $sRightx;
		}
		$sLink = "";
		if ($nAllPage < 6 ){
		//総ページ数が5ページ以下の場合　1 2 3 4 5 
			$i = 1;
			while ($i <= $nAllPage){
				if ($i == $p_page){
					$sLink .= '<li class="active"><a href="#">'.$p_page.'</a></li>';
				}else{
					$sLink .= '<li><a href="javascript:'.$sJscr.$i.')">'.$i.'</a></li>';
				}
				$i++;
			}
		}else if (($nAllPage > 5 )&&($p_page < 4)){
		//総ページ数が6ページ以上で、表示ページが3ページ以下の場合 1 2 3 4 5
			$i = 1;
			while ($i <= 5){
				if ($i == $p_page){
					$sLink .= '<li class="active"><a href="#">'.$p_page.'</a></li>';
				}else{
					$sLink .= '<li><a href="javascript:'.$sJscr.$i.')">'.$i.'</a></li>';
				}
				$i++;
			}
		}else if (($nAllPage > 5 )&&($p_page >= 4)&&($p_page <= ($nAllPage - 2))){
		//総ページ数が6ページ以上で、表示ページが4ページ以上かつ総ページ-2以下の場合 2 3 4 5 6
			$i = ($p_page - 2);
			while ($i <= ($p_page + 2)){
				if ($i == $p_page){
					$sLink .= '<li class="active"><a href="#">'.$p_page.'</a></li>';
				}else{
					$sLink .= '<li><a href="javascript:'.$sJscr.$i.')">'.$i.'</a></li>';
				}
				$i++;
			}

		}else if (($p_page >= ($nAllPage - 4) )&&($p_page <= $nAllPage)){
		//表示ページが、総ページ数から総ページ-4までの場合
			$i = ($nAllPage - 4);
			while ($i <= $nAllPage){
				if ($i == $p_page){
					$sLink .= '<li class="active"><a href="#">'.$p_page.'</a></li>';
				}else{
					$sLink .= '<li><a href="javascript:'.$sJscr.$i.')">'.$i.'</a></li>';
				}
				$i++;
			}
		}else if(($p_page > 5 )&&($p_page < ($nAllPage - 4))){
		//表示ページが、6ページ以上、総ページ-5までの場合
			$i = ($p_page - 2);
			while (($i <= ($p_page + 2))||($i <= ($nAllPage))){
				if ($i == $p_page){
					$sLink .= '<li class="active"><a href="#">'.$p_page.'</a></li>';
				}else{
					$sLink .= '<li><a href="javascript:'.$sJscr.$i.')">'.$i.'</a></li>';
				}
				$i++;
			}
		}

		$paging = '<div class="pagination"><ul>'.$sNewest.$sNew.$sLink.$sOld.$sOldest.'</ul>'.
			"&nbsp;&nbsp;&nbsp;&nbsp;".'総データ数'.":".$p_AllCnt.'&nbsp;'.'件'.'&nbsp;&nbsp;'.'総ページ数'. ":".$nAllPage.'</div>';
		return $paging;
	
	}
  	//////////////////////////////////////////////////////////////////////////////
	// クッキーの値取得
	// 引数:クッキー名1
	// 		クッキー名2
	// 戻値:クッキーの値
	function getCookie ($p_name1, $p_name2) {
		return $_COOKIE[$p_name1][$p_name2];
	}

?>