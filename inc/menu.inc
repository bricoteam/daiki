          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class=""><a href="<?=URL_MANAGE?>"><h4><i class="icon-home"></i>HOME</h4></a></li>
              <li class="nav-header"><i class="icon-list-alt"></i>見積書</li>
              <li><a href="<?=URL_MANAGE?>input01/">一覧</a></li>
              <li><a href="<?=URL_MANAGE?>input01/edit.php?m=new">新規作成</a></li>
              <li class="nav-header"><i class="icon-file"></i>作業完了通知書</li>
              <li><a href="<?=URL_MANAGE?>input02/">一覧</a></li>
              <li><a href="<?=URL_MANAGE?>input02/edit.php?m=new">新規作成</a></li>
              <li class="nav-header"><i class="icon-download-alt"></i>注文一覧</li>
              <li><a href="<?=URL_MANAGE?>output01/">出力</a></li>
              <li class="nav-header"><i class="icon-calendar"></i>作業予定表</li>
              <li><a href="<?=URL_MANAGE?>input03/">一覧</a></li>
              <li class="nav-header"><i class="icon-calendar"></i>配車予定表</li>
              <li><a href="<?=URL_MANAGE?>input04/">一覧</a></li>
              <li class="nav-header"><i class="icon-book"></i>マスタ管理</li>
              <li><a href="<?=URL_MANAGE?>master01/">区分マスタ登録</a></li>
              <li><a href="<?=URL_MANAGE?>master02/">内訳項目マスタ登録</a></li>
              <li><a href="<?=URL_MANAGE?>master03/">社員マスタ登録</a></li>
              <!--li><a href="<?=URL_MANAGE?>master04/">協力会社マスタ登録</a></li-->
              <li><a href="<?=URL_MANAGE?>master05/">車両マスタ登録</a></li>
            </ul>
          </div><!--/.well -->
