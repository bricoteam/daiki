     <!-- Footer
      ================================================== -->
      <hr>
      <footer class="footer">
        <p class="pull-right"><a href="#" class="btn btn-info"><i class="icon-arrow-up icon-white"></i>&nbsp;TOP</a></p>
        <p>Copyright&copy;<?=date('Y')?>Daiki Electric Co.,LTD. All Rights Reserved.</p>
      </footer>

    </div><!-- /container -->



    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
    <script src="<?=URL_COMMON?>js/google-code-prettify/prettify.js"></script>
    <script src="<?=URL_COMMON?>js/bootstrap-transition.js"></script>
    <script src="<?=URL_COMMON?>js/bootstrap-alert.js"></script>
    <script src="<?=URL_COMMON?>js/bootstrap-modal.js"></script>
    <script src="<?=URL_COMMON?>js/bootstrap-dropdown.js"></script>
    <script src="<?=URL_COMMON?>js/bootstrap-scrollspy.js"></script>
    <script src="<?=URL_COMMON?>js/bootstrap-tab.js"></script>
    <script src="<?=URL_COMMON?>js/bootstrap-tooltip.js"></script>
    <script src="<?=URL_COMMON?>js/bootstrap-popover.js"></script>
    <script src="<?=URL_COMMON?>js/bootstrap-button.js"></script>
    <script src="<?=URL_COMMON?>js/bootstrap-collapse.js"></script>
    <script src="<?=URL_COMMON?>js/bootstrap-carousel.js"></script>
    <script src="<?=URL_COMMON?>js/bootstrap-typeahead.js"></script>
    <script src="<?=URL_COMMON?>js/application.js"></script>

  </body>
</html>
