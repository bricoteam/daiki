<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <title><?=SITE_NAME?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?=URL_COMMON?>css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 70px;
        padding-bottom: 40px;
/*        padding-right: 40px;*/
      }
      .sidebar-nav {
        padding: 9px 0;
      }
    </style>
    <link href="<?=URL_COMMON?>css/bootstrap-responsive.css" rel="stylesheet">
    <link href="<?=URL_COMMON?>css/jquery.notifyBar.css" rel="stylesheet" type="text/css" media="screen"  />

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="<?=URL_COMMON?>js/jquery.js"></script>
    <script src="<?=URL_COMMON?>js/jquery.notifyBar.js"></script>
    <script>
        function formdate( object ){
        	m_d = object.value.split("/");
        	str = "2007/";
        	str += Math.floor(m_d[0]/10)%10;
        	str += m_d[0]%10;
        	str += "/";
        	str += Math.floor(m_d[1]/10)%10;
        	str += m_d[1]%10;
        	object.value = str;
        }
    </script>
    <script type="text/javascript">
        function dateFormat(obj){
            var date = new Date();
            var nowY = date.getFullYear();
            var str0=obj.value;
            var str;
            var s = str0.indexOf("/");
            var sla = str0.split("/");
            if (s != -1){
            //スラッシュありの場合
                if (sla.length == 2){
                    //要素数2:MM/DDの場合
                    str = ""+Math.floor(sla[0]/10)%10+sla[0]%10+Math.floor(sla[1]/10)%10+sla[1]%10;
                }else if (sla.length == 3){
                    //要素数3:YY/MM/DDの場合
                    str = ""+Math.floor(sla[0]/10)%10+sla[0]%10+Math.floor(sla[1]/10)%10+sla[1]%10+Math.floor(sla[2]/10)%10+sla[2]%10;
                }else{
                    //その他
                    return;
                }
            }else{
            //スラッシュなしの場合
                str = str0;
            }
            if(str==""){
                return;
            }else if(str.match(/[0-9]{8}/)){
                str1=str.substring(0,4)+"/"+str.substring(4,6)+"/"+str.substring(6,8);
                obj.value=str1;
            }else if(str.match(/[0-9]{6}/)){
                str1='20'+str.substring(0,2)+"/"+str.substring(2,4)+"/"+str.substring(4,6);
                obj.value=str1;
            }else if(str.match(/[0-9]{4}/)){
                str1=nowY+"/"+str.substring(0,2)+"/"+str.substring(2,4);
                obj.value=str1;
            }else{
                return;
            }
        }
        function toUpper(obj){
            var str0=obj.value;
            str = str0.toUpperCase();
            obj.value=str;
        }
        function chg_Hankaku(textid){
         var before=textid.value;
         var str = "";
       //  before.split("").forEach(function(o){
       //   str += o.replace(/[Ａ-Ｚａ-ｚ０-９]/g, function(s) {
       //      return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);
       //   });
       //  });
       var ngchr = [
           '１','２','３','４','５','６','７','８','９','０',
           'Ａ','Ｂ','Ｃ','Ｄ','Ｅ','Ｆ','Ｇ','Ｅ','Ｆ','Ｇ','Ｈ','Ｉ','Ｊ','Ｋ','Ｌ',
           'Ｍ','Ｎ','Ｏ','Ｐ','Ｑ','Ｒ','Ｓ','Ｔ','Ｕ','Ｖ','Ｗ','Ｘ','Ｙ','Ｚ',
           'ａ','ｂ','ｃ','ｄ','ｅ','ｆ','ｇ','ｈ','ｉ','ｊ','ｋ','ｌ','ｍ','ｎ','ｏ','ｐ',
           'ｑ','ｒ','ｓ','ｔ','ｕ','ｖ','ｗ','ｘ','ｙ','ｚ','／','‐','―','－','─',
       ];
       var trnchr = [
           '1','2','3','4','5','6','7','8','9','0',
           'A','B','C','D','E','F','G','E','F','G','H','I','J','K','L',
           'M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
           'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p',
           'q','r','s','t','u','v','w','x','y','z','/','-','-','-','-',
       ];
       before.split("").forEach(function(o){
           for(var i=0; i<=ngchr.length;i++){
               o = o.replace( ngchr[i], trnchr[i], 'mg' );
           }
           str += o;
         });
         textid.value = str;
       }
    </script>
