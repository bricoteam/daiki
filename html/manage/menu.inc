  </head>

  <body data-spy="scroll" data-target=".subnav" data-offset="50">
    <div class="container">


    <!-- Navbar
      ================================================== -->
      <div class="navbar navbar-fixed-top">
          <div class="navbar-inner">
          <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </a>
            <a class="brand" href="<?=URL_MANAGE?>"><?=SITE_NAME?></a>
            <div class="nav-collapse">
              <ul class="nav">
                <li class="active">
                  <a href="#">HOME</a>
                </li>
              </ul>
              <p class="navbar-text pull-right"><?= $_ADMIN[$_SESSION['USER']][1] ?>さんでログイン中&nbsp;<a href="<?=URL_MANAGE?>logout.php" class="btn btn-danger">ログアウト</a></p>
            </div>
          </div>
        </div>
      </div>

      <div class="container-fluid">

        <!-- Masthead
        ================================================== -->
        <h2>HOME</h2>
        <p class="lead">管理画面メニュー</p>

        <div class="well">
          <h2>入力／出力管理</h2>
          <p>見積データ、作業完了データの入力と出力管理画面</p>
          <div class="pull-left">	
            <div class="row">
              <a href="input01/" class="btn btn-success span4 btn-large">
                見積書入力
              </a>
            </div>
            &nbsp;
            <div class="row">
              <a href="input02/" class="btn btn-success span4 btn-large">
                作業完了通知書入力
              </a>
            </div>
            &nbsp;
            <div class="row">
              <a href="output01/" class="btn btn-success span4 btn-large">
                注文一覧出力
              </a>
            </div>
          </div>
          <div class="pull-left" style="margin-left:60px;">	
            <div class="row">
              <a href="input03/" class="btn btn-success span4 btn-large">
                作業予定表入力
              </a>
            </div>
            &nbsp;
            <div class="row">
              <a href="input04/" class="btn btn-success span4 btn-large">
                配車予定表入力
              </a>
            </div>
          </div>
          <div style="clear:both;"> </div> 
        </div>
	
	

        <div class="well">
          <h2>マスタ管理</h2>
          <p>各種マスタの登録・編集・削除管理画面</p>
          <div class="pull-left">	
            <div class="row">
              <a href="master01/" class="btn btn-warning span4 btn-large">
                区分マスタ登録
              </a>
            </div>
            &nbsp;
            <div class="row">
              <a href="master02/" class="btn btn-warning span4 btn-large">
                内訳項目マスタ登録
              </a>
            </div>
          </div>
          <div class="pull-left" style="margin-left:60px;">	
            <div class="row">
              <a href="master03/" class="btn btn-warning span4 btn-large">
                社員マスタ登録
              </a>
            </div>
            &nbsp;
            <!--div class="row">
              <a href="master04/" class="btn btn-warning span4 btn-large">
                協力会社マスタ登録
              </a>
            </div-->
            &nbsp;
            <div class="row">
              <a href="master05/" class="btn btn-warning span4 btn-large">
                車両マスタ登録
              </a>
            </div>
          </div>
          <div style="clear:both;"> </div> 
        </div>
