<?
	/***************************************************************************
	 * Name 		:login.php
	 * Description 		:ログイン
	 * Include		:func.common.inc
	 * 			 	func.field.inc
	 * 				func.fieldcheck.inc
	 * 			 	class.cls_spot.inc
	 * Trigger		:
	 * Create		:2009/10/01 Brico Suzuki
	 * LastModify		:
	 *
	 *
	 *
	 **************************************************************************/
	if ($_SERVER[SERVER_NAME] == 'daiki.bricoleur.in'){
		require_once('ini.inc');
	}
	include_once 'func.common.inc';
	include_once 'func.field.inc';
	include_once 'func.fieldcheck.inc';

	session_start();
	$data = $_REQUEST;
	//ログインチェック
	$blogin = isLogin();
	if ($blogin){
		header("Location: index.php");
	}

	//id,passwordが入力済み
	if ((strlen($data['id'])>0)||(strlen($data['password'])>0)){
		$blogin = doLogin ($data['id'],$data['password']);
		if ($blogin){
			if ($data['save'] == '1') {
				// ログイン情報クッキー保存(1ヶ月)
				setcookie('daiki[id]', $data['id'], time() + 60 * 60 * 24 * 30, '/');
				setcookie('daiki[password]', $data['password'], time() + 60 * 60 * 24 * 30, '/');
				setcookie('daiki[save]', '1', time() + 60 * 60 * 24 * 30, '/');
			} else {
				// クッキー削除
				setcookie('daiki[id]', '', time() - 3600, '/');
				setcookie('daiki[password]', '', time() - 3600, '/');
				setcookie('daiki[save]', '', time() - 3600, '/');
			}
			//informationページへ飛ばす
			header("Location: index.php");
			exit;
		}else{
			setcookie('daiki[id]', '', time() - 3600);
			setcookie('daiki[password]', '', time() - 3600);
			setcookie('daiki[save]', '', time() - 3600);
			unset($_COOKIE);
			$sMessage = 'IDまたはPASSWORDが間違っています';
		}
	}else{
		if ($data['m']=="1"){
			setcookie('daiki[id]', '', time() - 3600);
			setcookie('daiki[password]', '', time() - 3600);
			setcookie('daiki[save]', '', time() - 3600);
			unset($_COOKIE);
			$sMessage = 'IDまたはPASSWORDが間違っています';
		}
	}
	//id,password未入力またはログイン失敗の場合はログイン画面を
	doLogout();
	include_once "header.inc";
	include_once "login.inc";
	include_once "footer.inc";
?>
