<script type="text/javascript">
<?php
	if (count($arrErrors)>0){
?>
    $(function() { 
        $.notifyBar({ html: "エラーがあります" });
    });
<?php
	}
?>
</script>
<script type="text/javascript" src="<?=URL_COMMON?>js/manage.js"></script>
<script language="javascript">
<!--
function dispCal() {
	var f = document.fInput;
	f.submit();
	return false;
}
function dispCal2(p) {
	var f = document.fCal;
	if (p == 0){
		f.sel_nengetsu.value = f.before_nengetsu.value;
	}else{
		f.sel_nengetsu.value = f.next_nengetsu.value;
	}
	f.submit();
	return false;
}
function doSearch() {
	var f = document.fList;
	f.m.value = 'search';
	f.submit();
	return false;
}
function doDelete(uid) {
	if (confirm('削除してよろしいですか？')){
		var f = document.fList;
		f.m.value = 'del';
		f.HDR_UID.value = uid;
		f.submit();
	}
	return false;
}
function getData(page){
	var f = document.fList;
	f.p.value = page;
	f.uid.value = '';
	f.m.value = '';
	f.submit();
	return false;
}
function doShow(uid){
	var f = document.fList;
	f.uid.value = uid;
	f.m.value = 'show';
	f.submit();
	return false;
}
function editWork(ymd){
	//var childWindow = window.open('<?=URL_MANAGE?>input04/edit.php?ymd='+ymd, '_self');
	var f = document.fControl;
	f.ymd.value = ymd;
	f.submit();
	return false;

}
function openPdf(){
	var date = new Date();
	var now = date.getTime();
	var f = document.fOutput;
	
	for (var i = 0; i < f.range.length; i++)
	if(f.range[i].checked == true){
		var range = f.range[i].value;
	}
	var childWindow = window.open('<?=URL_MANAGE?>pdf/rep05.php?span='+f.out_span.value+'&h='+now+'&range='+range+'&from='+f.date_from.value+'&to='+f.date_to.value, '_blank');
}

    //-->
    </script>
 
  </head>

  <body data-spy="scroll" data-target=".subnav" data-offset="50" >


  <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?=URL_MANAGE?>"><?=SITE_NAME?></a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="">
                <a href="<?=URL_MANAGE?>"><i class="icon-home icon-white"></i>&nbsp;HOME</a>
              </li>
              <li class="active">
                <a href="#">配車予定表入力</a>
              </li>
            </ul>
            <p class="navbar-text pull-right"><i class="icon-user icon-white"></i>&nbsp;<?= $_ADMIN[$_SESSION['USER']][1] ?>さんでログイン中&nbsp;<a href="<?=URL_MANAGE?>logout.php" class="btn btn-danger">ログアウト&nbsp;<i class="icon-share-alt icon-white"></i></a></p>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span2">
          <?php
	  include 'menu.inc';
	  ?>
        </div><!--/span-->
        <div class="span9">
          <h2>配車予定表入力</h2>
          <p class="lead"></p>
      <?php if (count($arrErrors) > 0){ ?>
          <div class="alert alert-error ">
            <a class="close" data-dismiss="alert">×</a>
	    <ul>
            <li>
            <?=implode('</li><li>',$arrErrors)?>
	    </li>
	    </ul>
          </div>
      <?php } ?>
          <form class="well form-inline" name="fOutput" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" >
            <input type="hidden" name="m" value="" />
            <fieldset>
            <div class="control-group">
            <h3>出力</h3>
		<input type="radio" id="r04" name="range" value="0" <?=($data['range']=="0")?'checked':''?> <?=($data['range']=="")?'checked':''?> />
              <label class="control-label" for="input01">本日から</label>
                <?=getSelectBox ($_OUTPUT_SPAN, 'out_span', $data['out_span'],null,' class="span1h"')?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="radio" id="r04" name="range" value="1" <?=($data['range']=="1")?'checked':''?>  />
<input type="text" name="date_from" value="<?=mbConv(htmlspecialchars(br4nl(($data['date_from'])),ENT_QUOTES),$enc)?>" class="span1h" placeholder="fromを入力" style="ime-mode: disabled;" id="date_from" onChange="dateFormat(this);" />　～　<input type="text" name="date_to" value="<?=mbConv(htmlspecialchars(br4nl(($data['date_to'])),ENT_QUOTES),$enc)?>" class="span1h" placeholder="toを入力" style="ime-mode: disabled;" id="date_to" onChange="dateFormat(this);" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn" href="javascript:void(0);" onclick="openPdf();"><i class="icon-print"></i>&nbsp;予定表PDF出力</a>
            </div>
            </fieldset>
          </form>	
          <form class="well form-inline" name="fInput" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" >
            <input type="hidden" name="m" value="" />
            <fieldset>
            <div class="control-group">
            <h3>入力</h3>
              <label class="control-label" for="input01">年月度</label>
                <?=getSelectBox ($arrMonth, 'sel_nengetsu', (strlen($data['sel_nengetsu'])==0)?$getudo['y'].$getudo['m']:$data['sel_nengetsu'],null,' class="span2"')?>
            <a class="btn" href="javascript:void(0);" onclick="dispCal();">カレンダーを表示</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </div>
            </fieldset>
          </form>	
          <form name="fCal" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>" >
            <input type="hidden" name="sel_nengetsu" value="">
            <a class="btn" href="javascript:void(0);" onclick="dispCal2(0);">　<<　前の年月度　</a>
            <input type="hidden" name="before_nengetsu" value="<?=$before_nengetsu?>">
            <input type="hidden" name="next_nengetsu" value="<?=$next_nengetsu?>">
            <h3 style="display:inline;margin-left:15px;margin-right:15px;"><?=$sel_nen?>年<?=$sel_getsu?>月度</h3>
            <a class="btn" href="javascript:void(0);" onclick="dispCal2(1);">　次の年月度　>>　</a>
          </form>
          <form name="fControl" method="POST" action="detail.php"  >
            <input type="hidden" name="ymd" value="<?=$data['ymd']?>" />
            <input type="hidden" name="m" value="" />
            <input type="hidden" name="dstoken" value="<?=$dstoken?>" />
          </form>

<?=$clsSch->getCalendar($sel_nen2,$sel_getsu2,2)?><?=$clsSch->getCalendar($sel_nen,$sel_getsu,1)?>
<div style="clear:both;"></div>