<script type="text/javascript">
<?php
	if (count($arrErrors)>0){
?>
    $(function() { 
        $.notifyBar({ html: "入力エラーがあります" });
    });
<?php
	}else if ($f_add === true){
?>
    $(function() { 
        $.notifyBar({ html: "登録しました" });
    });
<?php
	}
?>
</script>
<script type="text/javascript" src="<?=URL_COMMON?>js/manage.js"></script>
<script language="javascript">
<!--
function doCommit() {
	var f = document.fList;
	f.m.value = 'commit';
	f.submit();
	return false;
}
function doCopy() {
	var f = document.fList;
	f.m.value = 'copy';
	f.submit();
	return false;
}
function doList() {
	var f = document.fList;
	f.action = 'index.php';
	f.m.value = '';
	f.HDR_UID.value = '';
	f.dstoken.value = '';
	f.mode.value = '';
	f.submit();
	return false;
}
function doBackSearch() {
	var f = document.fList;
	f.action = 'index.php';
	f.m.value = '';
	f.HDR_UID.value = '';
	f.dstoken.value = '';
	f.mode.value = '';
	f.back.value = '1';
	f.submit();
	return false;
}

function setFocus(id)
{
document.getElementById(id).focus();
}
function setRadio(id)
{
document.getElementById(id).checked = true;
}
function openPdf(stmp){
	var date = new Date();
	var now = date.getTime();
	var childWindow = window.open('<?=URL_MANAGE?>pdf/rep02.php?id=<?=$data['HDR_UID']?>&stmp='+stmp+'&h='+now, '_blank');
}
    //-->
    </script>

  </head>

  <body>


  <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?=URL_MANAGE?>"><?=SITE_NAME?></a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="">
                <a href="<?=URL_MANAGE?>"><i class="icon-home icon-white"></i>&nbsp;HOME</a>
              </li>
              <li class="active">
                <a href="#">作業完了通知書入力</a>
              </li>
            </ul>
            <p class="navbar-text pull-right"><i class="icon-user icon-white"></i>&nbsp;<?= $_ADMIN[$_SESSION['USER']][1] ?>さんでログイン中&nbsp;<a href="<?=URL_MANAGE?>logout.php" class="btn btn-danger">ログアウト&nbsp;<i class="icon-share-alt icon-white"></i></a></p>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span2">
          <?php
	  include 'menu.inc';
	  ?>
        </div><!--/span-->
        <div class="span9">
          <h2>作業完了通知書入力(<?=($sDBmode=="new")?'新規':'変更'?>)</h2>
          <p class="lead"></p>
      <?php if (count($arrErrors) > 0){ ?>
          <div class="alert alert-error ">
            <a class="close" data-dismiss="alert">×</a>
	    <ul>
            <li>
            <?=implode('</li><li>',$arrErrors)?>
	    </li>
	    </ul>
          </div>
      <?php } ?>
          <form  class="form-horizontal" name="fList" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" >
          <fieldset>
            <input type="hidden" name="m" value="" />
            <input type="hidden" name="back" value="" />
            <input type="hidden" name="dstoken" value="<?=$dstoken?>" />
            <input type="hidden" name="HDR_UID" value="<?=$data['HDR_UID']?>" />
            <input type="hidden" name="HDR_NO_COPY" value="<?=$data['HDR_NO_COPY']?>" />
            <input type="hidden" name="mode" value="<?=$sDBmode?>" />
<?php
if ($sDBmode=="new"){
?>
           <div class="row-fluid">
            <div class="control-group well BKyellow span5">
              <span onclick="setFocus('input01');">見積書番号</span>
              <div class="input-append">
                <input type="text" name="COPY_HDR_NO" value="<?=mbConv(htmlspecialchars(br4nl(($data['COPY_HDR_NO'])),ENT_QUOTES),$enc)?>" class="span1h" placeholder="見積書番号" style="ime-mode: disabled;" id="input01" onChange="toUpper(this);chg_Hankaku(this);" /><a href="javascript:void(0);return false;" onclick="doCopy();" class="btn" >表示</a>
              </div>
            </div>
            <div class="control-group well span5 " style="float:right;margin-right:0px;">
              <div class="btn-group" >
              <a class="btn btn-primary" href="javascript:void(0);" onclick="doCommit();return false;">&nbsp;&nbsp;&nbsp;<i class="icon-ok icon-white"></i>&nbsp;登録&nbsp;&nbsp;&nbsp;</a>
              <a class="btn" href="javascript:void(0);" onClick="openPdf(0);"><i class="icon-print"></i>&nbsp;PDF(印無)</a>
              <a class="btn" href="javascript:void(0);" onClick="openPdf(1);"><i class="icon-print"></i>&nbsp;PDF(印有)</a>
              </div>
            </div>
           </div>
<?php
}else{
?>
           <div class="row-fluid">
            <div class="control-group well BKyellow span5">
              <span>見積書番号</span>
              <div class="input-append">
                <input type="hidden" name="HDR_NO" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_NO'])),ENT_QUOTES),$enc)?>" />
                <strong><?=mbConv(htmlspecialchars(br4nl(($data['HDR_NO'])),ENT_QUOTES),$enc)?></strong>
              </div>
            </div>
            <div class="control-group well span5 " style="float: right;">
              <div class="btn-group" >
              <a class="btn btn-primary" href="javascript:void(0);" onclick="doCommit();return false;">&nbsp;&nbsp;&nbsp;<i class="icon-ok icon-white"></i>&nbsp;登録&nbsp;&nbsp;&nbsp;</a>
              <a class="btn" href="javascript:void(0);" onClick="openPdf(0);"><i class="icon-print"></i>&nbsp;PDF(印無)</a>
              <a class="btn" href="javascript:void(0);" onClick="openPdf(1);"><i class="icon-print"></i>&nbsp;PDF(印有)</a>
              </div>
            </div>
           </div>
<?php
}
?>
            <div class="control-group">
              <label class="control-label" for="input02">発行日付</label>
              <div class="controls">
                <input type="text" name="HDR_DATE_COMPREP" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_DATE_COMPREP'])),ENT_QUOTES),$enc)?>" class="span1h" placeholder="発行日付を入力" style="ime-mode: disabled;" id="input02" onChange="dateFormat(this);" />&nbsp;<span class="pink">※必須</span>
                <span class="offset1">
                  作&nbsp;成&nbsp;日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><?=(strlen($data['HDR_DATE_COMPLETE_CREATE_F'])==0)?date('Y/m/d'):$data['HDR_DATE_COMPLETE_CREATE_F']?></strong>
                </span>
              </div>
            </div>
	    <!--相手先情報-->
            <div class="row-fluid well">
              <div class="control-group">
                <label class="control-label" for="input05">相手先名</label>
                <div class="controls">
                  <input type="text" name="HDR_CUSTOMER_NAME" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_CUSTOMER_NAME'])),ENT_QUOTES),$enc)?>" class="span4" placeholder="相手先名を入力" style="ime-mode: active;" id="input05" onChange="chg_Hankaku(this);" />&nbsp;<span class="pink">※必須</span>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="input06">相手先部署</label>
                <div class="controls">
                  <input type="text" name="HDR_CUSTOMER_DEPT" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_CUSTOMER_DEPT'])),ENT_QUOTES),$enc)?>" class="span4" placeholder="相手先部署を入力" style="ime-mode: active;" id="input06" onChange="chg_Hankaku(this);" />&nbsp;御中&nbsp;<span class="pink">※必須</span>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="input07">貴社注文番号</label>
                <div class="controls">
                  <input type="text" name="HDR_ORDER_NO" value="<?=$data['HDR_ORDER_NO']?>" class="span2" placeholder="オーダーNo." style="ime-mode: disabled;" id="input07" onChange="toUpper(this);" />
                  &nbsp;<input type="text" name="HDR_ORDER_NO_B" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_ORDER_NO_B'])),ENT_QUOTES),$enc)?>" class="span1" placeholder="連番" style="ime-mode: disabled;" id="input08" onChange="toUpper(this);" />&nbsp;<span class="pink">※必須</span>
                </div>
              </div>
              <div class="control-group" style="margin-bottom:0;">
                <label class="control-label" for="input09">所属</label>
                <div class="controls">
                  <input type="text" name="HDR_DEPT" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_DEPT'])),ENT_QUOTES),$enc)?>" class="span2" placeholder="所属を入力" style="ime-mode: active;" id="input09" onChange="chg_Hankaku(this);" />&nbsp;<span class="pink">※必須</span>
                  <span class="offset1">
                  <span onclick="setFocus('input10');">担当</span>&nbsp;&nbsp;&nbsp;<!--/label-->
                    <input type="text" name="HDR_CHARGER" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_CHARGER'])),ENT_QUOTES),$enc)?>" class="span2" placeholder="担当を入力" style="ime-mode: active;" id="input10" onChange="chg_Hankaku(this);" />&nbsp;<span class="pink">※必須</span>
                  </span>
                </div>
              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="input11">件名</label>
              <div class="controls">
                <input type="text" name="HDR_TITLE_1" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_TITLE_1'])),ENT_QUOTES),$enc)?>" class="span5" placeholder="件名１行目を入力" style="ime-mode: active;" id="input11" onChange="chg_Hankaku(this);" />&nbsp;<span class="pink">※必須</span><br />
                <input type="text" name="HDR_TITLE_2" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_TITLE_2'])),ENT_QUOTES),$enc)?>" class="span5" placeholder="件名２行目を入力" style="ime-mode: active;margin-top:2px;" id="input12" onChange="chg_Hankaku(this);" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="input13">作業内容<br />(納入品名など)</label>
              <div class="controls">
                <textarea name="HDR_WORK_CONTENTS"class="span5" placeholder="作業内容を入力" style="ime-mode: active;" id="input13" rows="2" onChange="chg_Hankaku(this);"><?=mbConv(htmlspecialchars(br4nl(nVal($data['HDR_WORK_CONTENTS'])),ENT_QUOTES),$enc)?></textarea>&nbsp;<span class="pink">※必須</span>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="input14">作業期間<br />(作業完了期日)</label>
              <div class="controls">
                <input type="text" name="HDR_DATE_TERM_F" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_DATE_TERM_F'])),ENT_QUOTES),$enc)?>" class="span1h" placeholder="作業期間FROM" style="ime-mode: disabled;" id="input14" onChange="dateFormat(this);" />
                <span >
                  &nbsp;<span class="pink">※必須</span>&nbsp;&nbsp;&nbsp;&nbsp;～&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="HDR_DATE_TERM_T" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_DATE_TERM_T'])),ENT_QUOTES),$enc)?>" class="span1h" placeholder="作業期間TO" style="ime-mode: disabled;" id="input15" onChange="dateFormat(this);" />
                </span>&nbsp;<span class="pink">※必須</span>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="input15">作業報告書・<br />試験成績書など</label>
              <div class="controls" style="margin-top:3px;">
                作業報告書&nbsp;&nbsp;&nbsp;&nbsp;<?=getRadioBtn ($_REPORT, 'HDR_REPORT', nVal($data['HDR_REPORT'],'0'))?>&nbsp;&nbsp;&nbsp;&nbsp;
		（&nbsp;&nbsp;<?=getRadioBtn ($_CLIP, 'HDR_REPORT_CLIP', $data['HDR_REPORT_CLIP'])?>&nbsp;&nbsp;）</span>
		<br />
                試験成績書&nbsp;&nbsp;&nbsp;&nbsp;<?=getRadioBtn ($_REPORT, 'HDR_SCORE', nVal($data['HDR_SCORE'],'0'))?>&nbsp;&nbsp;&nbsp;&nbsp;
		（&nbsp;&nbsp;<?=getRadioBtn ($_CLIP, 'HDR_SCORE_CLIP', $data['HDR_SCORE_CLIP'])?>&nbsp;&nbsp;）</span>
              </div>
            </div>
           <div class="row-fluid">
            <div class="control-group well BKyellow span4">
              <span onclick="setFocus('input16');">（参考）当案件注文（又は決定）価格</span><br />
              <div class="input-append">
                <input type="text" name="HDR_ORDER_TOTAL" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_ORDER_TOTAL'])),ENT_QUOTES),$enc)?>" class="span2" placeholder="当案件注文金額" style="ime-mode: disabled;" id="input16" />&nbsp;円&nbsp;<span class="pink">※必須</span>
              </div>
            </div>
           </div>
            <div class="well row-fluid">
              <a href="javascript:void(0);" onclick="doCommit();return false;" class="btn btn-primary span3">登録</a>&nbsp;&nbsp;&nbsp;
              <a href="javascript:void(0);" onclick="doList();return false;" class="btn">一覧へ戻る</a>
              <a href="javascript:void(0);" onclick="doBackSearch();return false;" class="btn">検索結果へ戻る</a>
            </div>
          </fieldset>
          </form>	
        </div>
      </div>
	
