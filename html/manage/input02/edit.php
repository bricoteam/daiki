<?
	/***************************************************************************
	 * Name 		:edit.php
	 * Description 		:登録(新規コンテンツ)
	 * Include		:func.common.inc
	 * 			 	func.field.inc
	 * 				func.fieldcheck.inc
	 * 			 	class.cls_estimate.inc
	 * Trigger		:
	 * Create		:2009/10/01 Brico Suzuki
	 * LastModify		:
	 *
	 *
	 *
	 **************************************************************************/
	if ($_SERVER[SERVER_NAME] == 'daiki.bricoleur.in'){
		require_once('ini.inc');
	}
	include_once 'func.common.inc';
	include_once 'func.field.inc';
	include_once 'func.fieldcheck.inc';

	include_once 'class.cls_estimate.inc';

	session_start();
	$data = $_REQUEST;
	
	//ログインチェック
	$blogin = isLogin();
	if (!($blogin)){
		header("Location: ".URL_LOGIN);
		exit;
	}
	$incFile = "form.inc";

	$err = 0;
	$enc = 0;
	$bDouble = false;
	
	$clsEst 	= new cls_estimate();

	$f_add = false;
	$f_del = false;
	$f_order = false;
	//「新規登録」ボタンが押された場合
	switch ($data['m']) {
	case 'commit':
		$sDBmode 	= $data['mode'];
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])===true){
			$clsEst->setData($data,1);
			$clsEst->setWhere();
			$arrErrors = $clsEst->isValidData2();
			if (count($arrErrors) == 0){
				if ($sDBmode == "upd"){;
					$ContId = $clsEst->doUpdate2();
					$f_upd = true;
				}else{
					$ContId = $clsEst->doInsert2();
					$f_add = true;
				}
				$sDBmode = "upd";
				$clsEst->setData($data,1);
				$clsEst->setWhere();
				$data 	= $clsEst->getInfo();
				$f_add = true;
			}
		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	case 'copy':
		$sDBmode 	= $data['mode'];
		//複写NO保存
		$copyNo = $data['COPY_HDR_NO'];
		$clsEst->setData($data,2);
		$clsEst->setWhere();
		$row 	= $clsEst->getInfo();
		if (strlen($row['HDR_UID']) > 0){
			if ($row['HDR_COMPLETE_FLG']=='0'){
				//データがあるとき
				$copyno = $data['COPY_HDR_NO'];
				$data = $row;
				$data['COPY_HDR_NO'] = $copyno;
				$data['HDR_ORDER_TOTAL'] = $row['HDR_TOTAL'];
				$data['HDR_CUSTOMER_DEPT'] = '東日本本部　資材部　外注課';
			}else{
				$arrErrors['ERR']='作業完了通知書作成済みです。一覧から編集してください。';
			}
		}else{
			$arrErrors['ERR']='複写元の見積書番号が存在しません';
		}
		break;
	//「参照」リンクが押された場合
	case 'new':
		//$sDBmode=updで登録画面を表示
		$sDBmode 	= "new";
		break;
	default:
		//$sDBmode=updで登録画面を表示
		$sDBmode 	= "upd";
		//★二重投稿チェック
		$data['HDR_COMPLETE_FLG'] = "1";
		$clsEst->setData($data,1);
		$clsEst->setWhere();
		$data 	= $clsEst->getInfo();
		if (strlen($data['HDR_UID']) == 0){
			$sErrorMsg = "作業完了通知書存在エラー";
			$sErrorMsg2 = "ご指定の作業完了通知書が存在しません。";
			$incFile	= 'error.inc';
		}
		break;
	}

	//★二重投稿エラー
	if ($bDouble===true){
		$sErrorMsg = "二重送信エラー";
		$sErrorMsg2 = "ブラウザの「戻る」や「進む」機能で二重に同じ処理をすることはできません。";
		$incFile	= 'error.inc';
	}

	include_once "header.inc";
	//★
	if (($incFile == 'nodata.inc')||($incFile == 'error.inc')){
		include_once $incFile;
		include_once "footer.inc";
	}else{
		$dstoken = doubleSubmit();	//新しいトークン発行
		include_once $incFile;
		include_once "footer.inc";
	}
	$clsEst->close();


///テスト用
/*
$rows = array(0=>array('UCW_NAME'=>'諸　経　費'),1=>array('UCW_NAME'=>'人　件　費'),2=>array('UCW_NAME'=>'機　器　損　料'));
include_once "header.inc";
include_once $incFile;
include_once "footer.inc";
*/

?>
