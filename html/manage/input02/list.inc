<script type="text/javascript">
<?php
	if (count($arrErrors)>0){
?>
    $(function() { 
        $.notifyBar({ html: "エラーがあります" });
    });
<?php
	}else if ($f_del){
?>
    $(function() { 
        $.notifyBar({ html: "作業完了通知書データを削除しました。" });
    });
<?php
	}
?>
</script>
<script type="text/javascript" src="<?=URL_COMMON?>js/manage.js"></script>
<script language="javascript">
<!--
function doSearch() {
	var f = document.fList;
	f.m.value = 'search';
	f.submit();
	return false;
}
function doDelete(uid) {
	if (confirm('削除してよろしいですか？')){
		var f = document.fList;
		f.m.value = 'del';
		f.HDR_UID.value = uid;
		f.submit();
	}
	return false;
}
function getData(page){
	var f = document.fList;
	f.p.value = page;
	f.uid.value = '';
	f.m.value = '';
	f.submit();
	return false;
}
function doShow(uid){
	var f = document.fList;
	f.uid.value = uid;
	f.m.value = 'show';
	f.submit();
	return false;
}
    //-->
    </script>

  </head>

  <body data-spy="scroll" data-target=".subnav" data-offset="50" >


  <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?=URL_MANAGE?>"><?=SITE_NAME?></a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="">
                <a href="<?=URL_MANAGE?>"><i class="icon-home icon-white"></i>&nbsp;HOME</a>
              </li>
              <li class="active">
                <a href="#">作業完了通知書入力</a>
              </li>
            </ul>
            <p class="navbar-text pull-right"><i class="icon-user icon-white"></i>&nbsp;<?= $_ADMIN[$_SESSION['USER']][1] ?>さんでログイン中&nbsp;<a href="<?=URL_MANAGE?>logout.php" class="btn btn-danger">ログアウト&nbsp;<i class="icon-share-alt icon-white"></i></a></p>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span2">
          <?php
	  include 'menu.inc';
	  ?>
        </div><!--/span-->
        <div class="span9">
          <h2>作業完了通知書一覧&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="edit.php?m=new" class="btn btn-primary"><i class="icon-pencil icon-white"></i>&nbsp;新規作成</a></h2>
          <p class="lead"></p>
      <?php if (count($arrErrors) > 0){ ?>
          <div class="alert alert-error ">
            <a class="close" data-dismiss="alert">×</a>
	    <ul>
            <li>
            <?=implode('</li><li>',$arrErrors)?>
	    </li>
	    </ul>
          </div>
      <?php } ?>
          <form class="well form-horizontal" name="fList" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" >
            <input type="hidden" name="m" value="" />
            <input type="hidden" name="p" value="" />
            <input type="hidden" name="dstoken" value="<?=$dstoken?>" />
            <input type="hidden" name="HDR_UID" value="<?=$data['HDR_UID']?>" />
            <input type="hidden" name="kno" value="" />
            <input type="hidden" name="uid" value="" />
            <input type="hidden" name="pri" value="" />
            <input type="hidden" name="move" value="" />
            <input type="hidden" name="mode" value="<?=$sDBmode?>" />
            <fieldset>
            <div class="control-group">
              <label class="control-label" for="input01">見積書番号</label>
              <div class="controls">
                <input type="text" name="SEL_HDR_NO" value="<?=$data['SEL_HDR_NO']?>" class="span1h" placeholder="見積書番号" style="ime-mode: disabled;" onChange="toUpper(this);chg_Hankaku(this);" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="input01">件名</label>
              <div class="controls">
                <input type="text" name="SEL_HDR_TITLE" value="<?=$data['SEL_HDR_TITLE']?>" class="span4" placeholder="件名" style="ime-mode: active;" onChange="chg_Hankaku(this);" />
              </div>
            </div>
            <a href="javascript:void(0);" onclick="doSearch();return false;" class="btn btn-primary span3 offset1"><i class="icon-search icon-white"></i>　検　索</a>
            </fieldset>
          </form>	
      <?=$paging ?>
      <!--pageSet -->
      <table class="table table-bordered table-striped">
        <tr>
          <th class="span1h">見積書番号</th>
          <th class="span1h">発行日付</th>
          <th class="span1h">作成日付</th>
          <th class="">件名</th>
          <th class="span1h">編集</th>
          <th class="span1h">削除</th>
        </tr>
<?php
$cnt = 0;
if ((is_array($rows))&&(count($rows)>0)){
foreach($rows as $row){
	$cnt++;
?>
        <tr>
          <td><?=$row['HDR_NO']?></td>
          <td><?=$row['HDR_DATE_ESTIMATE_F']?></td>
          <td><?=$row['HDR_DATE_COMPLETE_CREATE_F']?></td>
          <td><?=$row['HDR_TITLE_1'].'<br />'.$row['HDR_TITLE_2']?></td>
          <td><a href="edit.php?uid=<?=$row['HDR_UID']?>" class="btn btn-success"><i class="icon-pencil icon-white"></i>編集</a></td>
          <td><a href="javascript:void(0);" onclick="doDelete('<?=$row['HDR_UID']?>');return false;" class="btn btn-danger"><i class="icon-trash icon-white"></i>削除</a></td>
        </tr>
<?php
}
}
?>
      </table>
