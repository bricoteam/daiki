<?
	/***************************************************************************
	 * Name 		:index.php
	 * Description 		:登録(新規コンテンツ)
	 * Include		:func.common.inc
	 * 			 	func.field.inc
	 * 				func.fieldcheck.inc
	 * 			 	class.cls_contents.inc
	 * Trigger		:
	 * Create		:2009/10/01 Brico Suzuki
	 * LastModify		:
	 *
	 *
	 *
	 **************************************************************************/
	if ($_SERVER[SERVER_NAME] == 'daiki.bricoleur.in'){
		require_once('ini.inc');
	}
	include_once 'func.common.inc';
	include_once 'func.field.inc';
	include_once 'func.fieldcheck.inc';

	include_once 'class.cls_shain.inc';

	session_start();
	$data = $_REQUEST;
	
	//ログインチェック
	$blogin = isLogin();
	if (!($blogin)){
		header("Location: ".URL_LOGIN);
		exit;
	}
	$incFile = "list.inc";

	$err = 0;
	$enc = 0;
	$bDouble = false;
	

	$clsShn 	= new cls_shain();

	$f_add = false;
	$f_del = false;
	$f_order = false;
	//「新規登録」ボタンが押された場合
	switch ($data['m']) {
	case 'commit':
		$sDBmode 	= $data['mode'];
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])){
			$clsShn->setData($data,1);
			$clsShn->setWhere();
			$arrErrors = $clsShn->isValidData();
			if (count($arrErrors) == 0){
				if ($sDBmode == "upd"){;
					$ContId = $clsShn->doUpdate();
					$f_upd = true;
				}else{
					$ContId = $clsShn->doInsert();
					$f_add = true;
				}
				$sDBmode = "";
				$data['SHN_UID'] = "";
				$data['SHN_NAME'] = "";
				$data['SHN_NICKNAME'] = "";
				$data['SHN_HEADOFFICE'] = "";
			}
		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	//「参照」リンクが押された場合
	case 'show':
		//$sDBmode=updで登録画面を表示
		$sDBmode 	= "upd";
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])){
			$clsShn->setData($data,1);
			$clsShn->setWhere();
			$data 	= $clsShn->getInfo();
			$b_show = true;
		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	//一覧から「削除」リンクが押された場合
	case 'del':
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])){
			$clsShn->setData($data,1);
			//削除処理
			$ContId = $clsShn->doDelete($data['SHN_UID']);
		
			$f_del = true;

			$data['SHN_UID'] = "";
		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	//一覧から「↑」「↓」リンクが押された場合
	case 'move':
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])){
			$clsShn->setData($data,1);
			//削除処理
			if (!($clsShn->modPriority())){
				$err = 1;
			};
			//メッセージ設定
			if ($err==1){
				$arrErrors['ERROR'] = "system error: 順序の変更に失敗しました。";
			}else{
				$f_order = true;
						}
		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	default:
		break;
	}
	//一覧を表示
	$incFile	= 'list.inc';
	$clsShn->setData($data,0);
	$clsShn->setWhere();
	$rows 	= $clsShn->getList();

	//★二重投稿エラー
	if ($bDouble===true){
		$sErrorMsg = "二重送信エラー";
		$sErrorMsg2 = "ブラウザの「戻る」や「進む」機能で二重に同じ処理をすることはできません。";
		$incFile	= 'error.inc';
	}

	include_once "header.inc";
	//★
	if (($incFile == 'nodata.inc')||($incFile == 'error.inc')){
		include_once $incFile;
		include_once "footer.inc";
	}else{
		$dstoken = doubleSubmit();	//新しいトークン発行
		include_once $incFile;
		include_once "footer.inc";
	}
	$clsShn->close();


///テスト用
/*
$rows = array(0=>array('SHN_NAME'=>'諸　経　費'),1=>array('SHN_NAME'=>'人　件　費'),2=>array('SHN_NAME'=>'機　器　損　料'));
include_once "header.inc";
include_once $incFile;
include_once "footer.inc";
*/

?>
