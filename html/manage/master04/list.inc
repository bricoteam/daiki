<script type="text/javascript">
<?php
	if (count($arrErrors)>0){
?>
    $(function() { 
        $.notifyBar({ html: "入力エラーがあります" });
    });
<?php
	}else if ($f_del){
?>
    $(function() { 
        $.notifyBar({ html: "社員を削除しました。" });
    });
<?php
	}else if ($f_add){
?>
    $(function() { 
        $.notifyBar({ html: "社員を追加しました。" });
    });
<?php
	}else if ($f_upd){
?>
    $(function() { 
        $.notifyBar({ html: "社員を変更しました。" });
    });
<?php
	}else if ($f_order){
?>
    $(function() { 
        $.notifyBar({ html: "順番を変更しました。" });
    });
<?php
	}
?>
</script>
<script type="text/javascript" src="<?=URL_COMMON?>js/manage.js"></script>
<script language="javascript">
<!--
function doCommit() {
	var f = document.fList;
	f.m.value = 'commit';
	f.submit();
	return false;
}
function doShow(uid) {
	var f = document.fList;
	f.m.value = 'show';
	f.COP_UID.value = uid;
	f.submit();
	return false;
}
function doDelete(uid) {
	if (confirm('削除してよろしいですか？')){
		var f = document.fList;
		f.m.value = 'del';
		f.COP_UID.value = uid;
		f.submit();
	}
	return false;
}
function doMove(uid,pri,move) {
	var f = document.fList;
	f.m.value = 'move';
	f.uid.value = uid;
	f.pri.value = pri;
	f.move.value = move;
	f.submit();
	return false;
}
    
    //-->
    </script>

  </head>

  <body data-spy="scroll" data-target=".subnav" data-offset="50" >


  <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?=URL_MANAGE?>"><?=SITE_NAME?></a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="">
                <a href="<?=URL_MANAGE?>"><i class="icon-home icon-white"></i>&nbsp;HOME</a>
              </li>
              <li class="active">
                <a href="#">協力会社マスタ登録</a>
              </li>
            </ul>
            <p class="navbar-text pull-right"><i class="icon-user icon-white"></i>&nbsp;<?= $_ADMIN[$_SESSION['USER']][1] ?>さんでログイン中&nbsp;<a href="<?=URL_MANAGE?>logout.php" class="btn btn-danger">ログアウト&nbsp;<i class="icon-share-alt icon-white"></i></a></p>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span2">
          <?php
	  include 'menu.inc';
	  ?>
        </div><!--/span-->
        <div class="span9">
          <h2>協力会社マスタ登録</h2>
          <p class="lead"></p>
      <?php if (count($arrErrors) > 0){ ?>
          <div class="alert alert-error ">
            <a class="close" data-dismiss="alert">×</a>
	    <ul>
            <li>
            <?=implode('</li><li>',$arrErrors)?>
	    </li>
	    </ul>
          </div>
      <?php } ?>
          <form class="well form-inline" name="fList" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" >
            <input type="hidden" name="m" value="" />
            <input type="hidden" name="dstoken" value="<?=$dstoken?>" />
            <input type="hidden" name="COP_UID" value="<?=$data['COP_UID']?>" />
            <input type="hidden" name="uid" value="" />
            <input type="hidden" name="pri" value="" />
            <input type="hidden" name="move" value="" />
            <input type="hidden" name="mode" value="<?=$sDBmode?>" />
            <label for="input01">社員名</label>
            <input id="input01" type="text" name="COP_NAME" value="<?=mbConv(htmlspecialchars(br4nl(($data['COP_NAME'])),ENT_QUOTES),$enc)?>" class="span2" placeholder="社員名を入力" style="ime-mode: active;" />&nbsp;&nbsp;
	    <label for="input02">略称</label>
            <input id="input02" type="text" name="COP_NICKNAME" value="<?=mbConv(htmlspecialchars(br4nl(($data['COP_NICKNAME'])),ENT_QUOTES),$enc)?>" class="span2" placeholder="略称を入力" style="ime-mode: active;" />&nbsp;&nbsp;
	    <label for="input03">表示</label>
            <input id="input03" type="checkbox" name="COP_DISP" value="1" <?=($data['COP_DISP']=="1")?'checked="on"':''?> />
	    <div style="margin-top:5px;"><a href="javascript:void(0);" onclick="doCommit();return false;" class="btn btn-primary span1 offset6">登録</a></div>
	    <div style="clear:both;"></div>
          </form>	
	
          <!--pageSet -->
          <table class="table table-bordered table-striped">
            <tr>
              <th class="span1h">表示順序</th>
              <th class="span1">表示</th>
              <th class="span2">社員名</th>
              <th class="span2">略称</th>
              <th class="span1h">編集</th>
              <th class="span1h">削除</th>
            </tr>
<?php
$cnt = 0;
if ((is_array($rows))&&(count($rows)>0)){
foreach($rows as $row){
	$cnt++;
?>
            <tr>
              <td>
                  <a href="javascript:void(0);" onclick="doMove('<?=$row['COP_UID']?>','<?=$row['COP_PRIORITY']?>','up');return false;"><i class="icon-arrow-up"></i>上</a>&nbsp;/
                  <a href="javascript:void(0);" onclick="doMove('<?=$row['COP_UID']?>','<?=$row['COP_PRIORITY']?>','down');return false;">下<i class="icon-arrow-down"></i></a>
                </td>
              <td><?=$_FLG_VALUE[$row['COP_DISP']]?></td>
              <td><?=$row['COP_NAME']?></td>
              <td><?=$row['COP_NICKNAME']?></td>
              <td><a href="javascript:void(0);" onclick="doShow('<?=$row['COP_UID']?>');return false;" class="btn btn-success"><i class="icon-pencil icon-white"></i>編集</a></td>
              <td><a href="javascript:void(0);" onclick="doDelete('<?=$row['COP_UID']?>');return false;" class="btn btn-danger"><i class="icon-trash icon-white"></i>削除</a></td>
            </tr>
<?php
}
}
?>
          </table>
        </div>
      </div>