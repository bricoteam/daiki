    <script type="text/javascript" src="<?=URL_COMMON?>js/manage.js"></script>
    <script language="javascript">
    <!--
    function doCommit() {
    	var f = document.fLogin;
    	f.submit();
    	return false;
    }
    
    //-->
    </script>

  </head>

  <body data-spy="scroll" data-target=".subnav" data-offset="50">


  <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?=URL_MANAGE?>"><?=SITE_NAME?></a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="active">
                <a href="#">ログイン</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="container">

      <!-- Masthead
      ================================================== -->
      <header class="jumbotron subhead" id="overview">
        <h2>ログイン</h2>
        <p class="lead">担当者ごとのIDとパスワードを入力してログインしてください</p>
        <form class="well form-inline" name="fLogin" method="POST" action="login.php">
          <input type="hidden" name="m" value="1" />
          <input name="id" type="text" value="<?=((strlen($data['id'])>0)?$data['id']:htmlspecialchars(getCookie('daiki', 'id'))) ?>" class="input-small" placeholder="ID" style="ime-mode: disabled;">
          <input name="password" type="password" value="<?=((strlen($data['password'])>0)?$data['password']:htmlspecialchars(getCookie('daiki', 'password'))) ?>" class="input-small" placeholder="Password" style="ime-mode: disabled;">
          <label class="checkbox">
          <input type="checkbox" name="save" value="1" id="memory" class="mr5" <?=getCookie('daiki', 'save') == '1' ? ' checked' : '' ?>> IDとパスワードを保存
          </label>
          <a href="javascript:void(0);" onclick="doCommit();return false;" class="btn btn-primary">ログイン</a>
        </form>
      </header>
      <?php if (strlen($sMessage) > 0){ ?>
        <div class="alert alert-error span4">
          <a class="close" data-dismiss="alert">×</a>
          <?=$sMessage?>
        </div><br /><br />
      <?php } ?>
