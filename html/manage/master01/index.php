<?
	/***************************************************************************
	 * Name 		:index.php
	 * Description 		:登録(新規コンテンツ)
	 * Include		:func.common.inc
	 * 			 	func.field.inc
	 * 				func.fieldcheck.inc
	 * 			 	class.cls_contents.inc
	 * Trigger		:
	 * Create		:2009/10/01 Brico Suzuki
	 * LastModify		:
	 *
	 *
	 *
	 **************************************************************************/
	if ($_SERVER[SERVER_NAME] == 'daiki.bricoleur.in'){
		require_once('ini.inc');
	}
	include_once 'func.common.inc';
	include_once 'func.field.inc';
	include_once 'func.fieldcheck.inc';

	include_once 'class.cls_kubun.inc';

	session_start();
	$data = $_REQUEST;
	
	//ログインチェック
	$blogin = isLogin();
	if (!($blogin)){
		header("Location: ".URL_LOGIN);
		exit;
	}
	$incFile = "list.inc";

	$err = 0;
	$enc = 0;
	$bDouble = false;
	

	$clsKbn 	= new cls_kubun();

	$f_add = false;
	$f_del = false;
	$f_order = false;
	//「新規登録」ボタンが押された場合
	switch ($data['m']) {
	case 'commit':
		$sDBmode 	= $data['mode'];
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])){
			$clsKbn->setData($data,1);
			$clsKbn->setWhere();
			$arrErrors = $clsKbn->isValidData();
			if (count($arrErrors) == 0){
				if ($sDBmode == "upd"){;
					$ContId = $clsKbn->doUpdate();
					$f_upd = true;
				}else{
					$ContId = $clsKbn->doInsert();
					$f_add = true;
				}
				$sDBmode = "";
				$data['KBN_UID'] = "";
				$data['KBN_NAME'] = "";
			}
		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	//「参照」リンクが押された場合
	case 'show':
		//$sDBmode=updで登録画面を表示
		$sDBmode 	= "upd";
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])){
			$clsKbn->setData($data,1);
			$clsKbn->setWhere();
			$data 	= $clsKbn->getInfo();
			$b_show = true;
		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	//一覧から「削除」リンクが押された場合
	case 'del':
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])){
			$clsKbn->setData($data,1);
			//削除処理
			$ContId = $clsKbn->doDelete($data['KBN_UID']);
		
			$f_del = true;

			$data['KBN_UID'] = "";
		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	//一覧から「↑」「↓」リンクが押された場合
	case 'move':
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])){
			$clsKbn->setData($data,1);
			//削除処理
			if (!($clsKbn->modPriority())){
				$err = 1;
			};
			//メッセージ設定
			if ($err==1){
				$arrErrors['ERROR'] = "system error: 順序の変更に失敗しました。";
			}else{
				$f_order = true;
						}
		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	default:
		break;
	}
	//一覧を表示
	$incFile	= 'list.inc';
	$clsKbn->setData($data,0);
	$clsKbn->setWhere();
	$rows 	= $clsKbn->getList();

	//★二重投稿エラー
	if ($bDouble===true){
		$sErrorMsg = "二重送信エラー";
		$sErrorMsg2 = "ブラウザの「戻る」や「進む」機能で二重に同じ処理をすることはできません。";
		$incFile	= 'error.inc';
	}

	include_once "header.inc";
	//★
	if (($incFile == 'nodata.inc')||($incFile == 'error.inc')){
		include_once $incFile;
		include_once "footer.inc";
	}else{
		$dstoken = doubleSubmit();	//新しいトークン発行
		include_once $incFile;
		include_once "footer.inc";
	}
	$clsKbn->close();


///テスト用
/*
$rows = array(0=>array('KBN_NAME'=>'諸　経　費'),1=>array('KBN_NAME'=>'人　件　費'),2=>array('KBN_NAME'=>'機　器　損　料'));
include_once "header.inc";
include_once $incFile;
include_once "footer.inc";
*/

?>
