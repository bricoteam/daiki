<script type="text/javascript">
<?php
	if (count($arrErrors)>0){
?>
    $(function() { 
        $.notifyBar({ html: "入力エラーがあります" });
    });
<?php
	}else if ($f_del){
?>
    $(function() { 
        $.notifyBar({ html: "区分を削除しました。" });
    });
<?php
	}else if ($f_add){
?>
    $(function() { 
        $.notifyBar({ html: "区分を追加しました。" });
    });
<?php
	}else if ($f_upd){
?>
    $(function() { 
        $.notifyBar({ html: "区分を変更しました。" });
    });
<?php
	}else if ($f_order){
?>
    $(function() { 
        $.notifyBar({ html: "順番を変更しました。" });
    });
<?php
	}
?>
</script>
<script type="text/javascript" src="<?=URL_COMMON?>js/manage.js"></script>
<script language="javascript">
<!--
function doCommit() {
	var f = document.fList;
	f.m.value = 'commit';
	f.submit();
	return false;
}
function doShow(uid) {
	var f = document.fList;
	f.m.value = 'show';
	f.KBN_UID.value = uid;
	f.submit();
	return false;
}
function doDelete(uid) {
	if (confirm('削除してよろしいですか？\n※区分に紐づく内訳項目も削除されます')){
		var f = document.fList;
		f.m.value = 'del';
		f.KBN_UID.value = uid;
		f.submit();
	}
	return false;
}
function doMove(uid,pri,move) {
	var f = document.fList;
	f.m.value = 'move';
	f.uid.value = uid;
	f.pri.value = pri;
	f.move.value = move;
	f.submit();
	return false;
}
    
    //-->
    </script>

  </head>

  <body data-spy="scroll" data-target=".subnav" data-offset="50" >


  <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?=URL_MANAGE?>"><?=SITE_NAME?></a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="">
                <a href="<?=URL_MANAGE?>"><i class="icon-home icon-white"></i>&nbsp;HOME</a>
              </li>
              <li class="active">
                <a href="#">区分マスタ登録</a>
              </li>
            </ul>
            <p class="navbar-text pull-right"><i class="icon-user icon-white"></i>&nbsp;<?= $_ADMIN[$_SESSION['USER']][1] ?>さんでログイン中&nbsp;<a href="<?=URL_MANAGE?>logout.php" class="btn btn-danger">ログアウト&nbsp;<i class="icon-share-alt icon-white"></i></a></p>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span2">
          <?php
	  include 'menu.inc';
	  ?>
        </div><!--/span-->
        <div class="span9">
          <h2>区分マスタ登録</h2>
          <p class="lead"></p>
      <?php if (count($arrErrors) > 0){ ?>
          <div class="alert alert-error ">
            <a class="close" data-dismiss="alert">×</a>
	    <ul>
            <li>
            <?=implode('</li><li>',$arrErrors)?>
	    </li>
	    </ul>
          </div>
      <?php } ?>
          <form class="well form-inline" name="fList" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" >
            <input type="hidden" name="m" value="" />
            <input type="hidden" name="dstoken" value="<?=$dstoken?>" />
            <input type="hidden" name="KBN_UID" value="<?=$data['KBN_UID']?>" />
            <input type="hidden" name="uid" value="" />
            <input type="hidden" name="pri" value="" />
            <input type="hidden" name="move" value="" />
            <input type="hidden" name="mode" value="<?=$sDBmode?>" />
            <label>区分名</label>
            <input type="text" name="KBN_NAME" value="<?=mbConv(htmlspecialchars(br4nl(($data['KBN_NAME'])),ENT_QUOTES),$enc)?>" class="span3" placeholder="区分名を入力" style="ime-mode: active;" />
            <a href="javascript:void(0);" onclick="doCommit();return false;" class="btn btn-primary">登録</a>
          </form>	
	
          <!--pageSet -->
          <table class="table table-bordered table-striped">
            <tr>
              <th class="span1">表示順序</th>
              <th class="span4">区分名</th>
              <th class="span1">編集</th>
              <th class="span1">削除</th>
            </tr>
<?php
$cnt = 0;
if ((is_array($rows))&&(count($rows)>0)){
foreach($rows as $row){
	$cnt++;
?>
            <tr>
              <td>
                  <a href="javascript:void(0);" onclick="doMove('<?=$row['KBN_UID']?>','<?=$row['KBN_PRIORITY']?>','up');return false;"><i class="icon-arrow-up"></i>上</a>&nbsp;/
                  <a href="javascript:void(0);" onclick="doMove('<?=$row['KBN_UID']?>','<?=$row['KBN_PRIORITY']?>','down');return false;">下<i class="icon-arrow-down"></i></a>
                </td>
              <td><?=$row['KBN_NAME']?></td>
              <td><a href="javascript:void(0);" onclick="doShow('<?=$row['KBN_UID']?>');return false;" class="btn btn-success"><i class="icon-pencil icon-white"></i>編集</a></td>
              <td><a href="javascript:void(0);" onclick="doDelete('<?=$row['KBN_UID']?>');return false;" class="btn btn-danger"><i class="icon-trash icon-white"></i>削除</a></td>
            </tr>
<?php
}
}
?>
          </table>
        </div>
      </div>