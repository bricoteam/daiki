<?
	/***************************************************************************
	 * Name 		:index.php
	 * Description 		:登録(新規コンテンツ)
	 * Include		:func.common.inc
	 * 			 	func.field.inc
	 * 				func.fieldcheck.inc
	 * 			 	class.cls_contents.inc
	 * Trigger		:
	 * Create		:2009/10/01 Brico Suzuki
	 * LastModify		:
	 *
	 *
	 *
	 **************************************************************************/
	if ($_SERVER[SERVER_NAME] == 'daiki.bricoleur.in'){
		require_once('ini.inc');
	}
	include_once 'func.common.inc';
	include_once 'func.field.inc';
	include_once 'func.fieldcheck.inc';

	include_once 'class.cls_estimate.inc';

	session_start();
	$data = $_REQUEST;

	//ログインチェック
	$blogin = isLogin();
	if (!($blogin)){
		header("Location: ".URL_LOGIN);
		exit;
	}
	$incFile = "list.inc";

	$err = 0;
	$enc = 0;
	$bDouble = false;
	
	$clsEst 	= new cls_estimate();

	$f_add = false;
	$f_del = false;
	$f_order = false;
	//「新規登録」ボタンが押された場合
	switch ($data['m']) {
/*	case 'search':
		$clsEst->setData($data,0);
		//一覧を表示
		$arrErrors = $clsEst->isValidSearchData();
		if (count($arrErrors) == 0){
			$clsEst->setWhere();
			$rows 	= $clsEst->getList();
		}
		$incFile	= 'list.inc';
		break;
*/
	//一覧から「削除」リンクが押された場合
	case 'del':
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])){
			$clsEst->setData($data,0);
			//削除処理
			if ($clsEst->doDelete($data['HDR_UID'])){
				$f_del = true;
				$data = array();
				$clsEst->setData($data,0);
				$clsEst->setWhere();
				$rows 	= $clsEst->getList();
				$data['HDR_UID'] = "";
			}else{
				$arrErrors['ERR']='作業完了通知書が存在します。先に作業完了通知書を削除してください。';
			}
		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	default:
		break;
	}
	
	if ($data['back']==1){
		$page = $_SESSION['est']['p'];
	}else{
		$page = $data['p'];
	}

	if (strlen($page)==0){
		$nPage = 1;
		$data['p'] = 1;
	}else{
		$nPage = $page;
	}


	$clsEst->setData($data, 0);
	$clsEst->setWhere();
	$rows = $clsEst->getList(CNT_EST_LIST,$data);
	$nRtn = $clsEst->getListCount();

	//テスト用
/*		$nRtn = 200;
	$rows[] = array(
		'HDR_DATE_ESTIMATE_F' => '2012/05/12',
		'HDR_NO' => 'A11366-01',
		'HDR_TITLE_1' => 'ＪＲＥ新盛岡ＳＳ　２７５ｋⅤ　ＧＣＢ',
		'HDR_TITLE_2' => '２７１内部解放点検９０１'
	);
*/

	if (($nRtn !== false)&&($nRtn > 0)){
		$paging = getPaging ($nRtn, CNT_EST_LIST, $nPage);
	}
	$incFile	= 'list.inc';

	//★二重投稿エラー
	if ($bDouble===true){
		$sErrorMsg = "二重送信エラー";
		$sErrorMsg2 = "ブラウザの「戻る」や「進む」機能で二重に同じ処理をすることはできません。";
		$incFile	= 'error.inc';
	}

	include_once "header.inc";
	//★
	if (($incFile == 'nodata.inc')||($incFile == 'error.inc')){
		include_once $incFile;
		include_once "footer.inc";
	}else{
		$dstoken = doubleSubmit();	//新しいトークン発行
		include_once $incFile;
		include_once "footer.inc";
	}
	$clsEst->close();



?>
