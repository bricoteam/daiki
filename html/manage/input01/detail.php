<?
	/***************************************************************************
	 * Name 		:detail.php
	 * Description 		:登録(新規コンテンツ)
	 * Include		:func.common.inc
	 * 			 	func.field.inc
	 * 				func.fieldcheck.inc
	 * 			 	class.cls_estimate.inc
	 * Trigger		:
	 * Create		:2009/10/01 Brico Suzuki
	 * LastModify		:
	 *
	 *
	 *
	 **************************************************************************/
	if ($_SERVER[SERVER_NAME] == 'daiki.bricoleur.in'){
		require_once('ini.inc');
	}
	include_once 'func.common.inc';
	include_once 'func.field.inc';
	include_once 'func.fieldcheck.inc';

	include_once 'class.cls_estimate.inc';
	include_once 'class.cls_kubun.inc';
	include_once 'class.cls_uchiwake.inc';

	session_start();
	$data = $_REQUEST;

	//ログインチェック
	$blogin = isLogin();
	if (!($blogin)){
		header("Location: ".URL_LOGIN);
		exit;
	}
	
	//区分リスト取得
	$clsKbn 	= new cls_kubun();
	$arrKbn	= $clsKbn->getAllList();
	$clsKbn->close();


	$clsUcw 	= new cls_uchiwake();

	$incFile = "detail.inc";

	$err = 0;
	$enc = 0;
	$bDouble = false;
	
	$clsEst 	= new cls_estimate();

	$f_add = false;
	$f_del = false;
	$f_order = false;
	//「新規登録」ボタンが押された場合
	switch ($data['m']) {
	case 'commitList':
		//ヘッダーからきた場合。
		$sDBmode 	= $data['mode'];
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])===true){
			$clsEst->setData($data,1);
			$clsEst->setWhere();
			$arrErrors = $clsEst->isValidData();
			if (count($arrErrors) == 0){
				if ($sDBmode == "upd"){;
					$ContId = $clsEst->doUpdate();
				}else{
					$ContId = $clsEst->doInsert();
					$data['HDR_UID'] = $ContId;
				}
				$clsEst->setData($data,1);
				$clsEst->setWhere();
				$data 	= $clsEst->getInfo();
				//明細未入力状態の時で、コピー元見積書番号がある場合は元から明細をコピーして登録
				if ((strlen($data['HDR_ITEM_TOTAL'])==0)&&(strlen($data['HDR_NO_COPY'])>0)){
					$head = $clsEst->copyDetail($data['HDR_UID'],$data['HDR_NO'],$data['HDR_NO_COPY']);
					//明細合計なんかを反映
					$data['HDR_VARIOUS_KIND'] = $head['HDR_VARIOUS_KIND'];
					$data['HDR_VARIOUS_RATE'] = $head['HDR_VARIOUS_RATE'];
					$data['HDR_VARIOUS_TOTAL'] = $head['HDR_VARIOUS_TOTAL'];
					$data['HDR_ITEM_TOTAL'] = $head['HDR_ITEM_TOTAL'];
					$data['HDR_TOTAL'] = $head['HDR_TOTAL'];
				}else if (strlen($data['HDR_ITEM_TOTAL'])==0){
				//明細未入力の状態で、コピー元が無い場合、諸経費掛率デフォルト設定
					$data['HDR_VARIOUS_KIND'] = 0;
					$data['HDR_VARIOUS_RATE'] = '12.0';
					$data['HDR_VARIOUS_TOTAL'] = 0;
					$data['HDR_ITEM_TOTAL'] = 0;
					$data['HDR_TOTAL'] = 0;
				}
				$rows 	= $clsEst->getDetailList();
			}else{
				unset($_SESSION['http']);
				$_SESSION['http'] = $data;
				header("Location: edit.php");
			
			}
		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	case 'commit':
		//諸経費更新
		$sDBmode 	= $data['mode'];
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])===true){
			$clsEst->setData($data,1);
			$clsEst->setWhere();
			$arrErrors = $clsEst->isValidDataTotal();
			if (count($arrErrors) == 0){
				$ContId = $clsEst->doUpdateTotal();
				$f_upd = true;
			}
			$clsEst->setData($data,1);
			$clsEst->setWhere();
			$data 	= $clsEst->getInfo();
			$rows 	= $clsEst->getDetailList();
		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
//commitHeader追加
	case 'commitHeader':
		//諸経費更新
		$sDBmode 	= $data['mode'];
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])===true){
			$clsEst->setData($data,1);
			$clsEst->setWhere();
			$arrErrors = $clsEst->isValidDataTotal();
			if (count($arrErrors) == 0){
				$ContId = $clsEst->doUpdateTotal();
				$f_upd = true;
				unset($_SESSION['http']);
				//追加：8/8
				$data['mode']="upd";
				$_SESSION['http'] = $data;
				header("Location: edit.php");
			}else{
			$clsEst->setData($data,1);
			$clsEst->setWhere();
			$data 	= $clsEst->getInfo();
			$rows 	= $clsEst->getDetailList();
			}
		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	case 'reload':
		//諸経費更新
		$sDBmode 	= $data['mode'];
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])===true){
			$clsEst->setData($data,1);
			$clsEst->setWhere();
			$arrErrors = $clsEst->isValidDataTotal();
			if (count($arrErrors) == 0){
				$ContId = $clsEst->doUpdateTotal();
			}
			$clsEst->setData($data,1);
			$clsEst->setWhere();
			$data 	= $clsEst->getInfo();
			$rows 	= $clsEst->getDetailList();
		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	case 'add':
		$sDBmode 	= $data['mode'];
		$sDBmode2 	= $data['mode2'];
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])===true){
			$clsEst->setData($data,1);
			$arrErrors = $clsEst->isValidDataDetail();
			if (count($arrErrors) == 0){
				if ($sDBmode2 == "upd"){;
					$ContId = $clsEst->doUpdateDetail();
				}else{
					$ContId = $clsEst->doInsertDetail();
				}
				$sDBmode2 	= 'new';
				$f_add = true;
			}else{
				$bk['DTL_UID'] = $data['DTL_UID'];
				$bk['ENT_KBN_NO'] = $data['ENT_KBN_NO'];
				$bk['ENT_DTL_NAME'] = $data['ENT_DTL_NAME'];
				$bk['ENT_DTL_FORMAT'] = $data['ENT_DTL_FORMAT'];
				$bk['ENT_DTL_NUM'] = $data['ENT_DTL_NUM'];
				$bk['ENT_DTL_UNIT'] = $data['ENT_DTL_UNIT'];
				$bk['ENT_DTL_PRICE'] = $data['ENT_DTL_PRICE'];
				$bk['ENT_DTL_TOTAL'] = $data['ENT_DTL_TOTAL'];
				$bk['ENT_DTL_REMARK'] = $data['ENT_DTL_REMARK'];
				$bk['HDR_VARIOUS_RATE'] = $data['HDR_VARIOUS_RATE'];
			}
			$clsEst->setWhere();
			$data 	= $clsEst->getInfo();
			if (count($arrErrors) > 0){
				$data['DTL_UID'] = $bk['DTL_UID'];
				$data['ENT_KBN_NO'] = $bk['ENT_KBN_NO'];
				$data['ENT_DTL_NAME'] = $bk['ENT_DTL_NAME'];
				$data['ENT_DTL_FORMAT'] = $bk['ENT_DTL_FORMAT'];
				$data['ENT_DTL_NUM'] = $bk['ENT_DTL_NUM'];
				$data['ENT_DTL_UNIT'] = $bk['ENT_DTL_UNIT'];
				$data['ENT_DTL_PRICE'] = $bk['ENT_DTL_PRICE'];
				$data['ENT_DTL_TOTAL'] = $bk['ENT_DTL_TOTAL'];
				$data['ENT_DTL_REMARK'] = $bk['ENT_DTL_REMARK'];
				$data['HDR_VARIOUS_RATE'] = $bk['HDR_VARIOUS_RATE'];
			}
			$rows 	= $clsEst->getDetailList();

		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	case 'addmaster':
		//明細登録と同時にそのデータを内訳項目マスタに登録
		$sDBmode 	= $data['mode'];
		$sDBmode2 	= $data['mode2'];
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])===true){
			$clsEst->setData($data,1);
			$arrErrors = $clsEst->isValidDataDetail();
			if (count($arrErrors) == 0){

				$ContId = $clsEst->doInsertDetail();
				//マスタ登録★
				$clsEst->doInsertUchiwakeMaster();
					
				$sDBmode2 	= 'new';
				$f_add = true;
			}else{
				$bk['DTL_UID'] = $data['DTL_UID'];
				$bk['ENT_KBN_NO'] = $data['ENT_KBN_NO'];
				$bk['ENT_DTL_NAME'] = $data['ENT_DTL_NAME'];
				$bk['ENT_DTL_FORMAT'] = $data['ENT_DTL_FORMAT'];
				$bk['ENT_DTL_NUM'] = $data['ENT_DTL_NUM'];
				$bk['ENT_DTL_UNIT'] = $data['ENT_DTL_UNIT'];
				$bk['ENT_DTL_PRICE'] = $data['ENT_DTL_PRICE'];
				$bk['ENT_DTL_TOTAL'] = $data['ENT_DTL_TOTAL'];
				$bk['ENT_DTL_REMARK'] = $data['ENT_DTL_REMARK'];
				$bk['HDR_VARIOUS_RATE'] = $data['HDR_VARIOUS_RATE'];
			}
			$clsEst->setWhere();
			$data 	= $clsEst->getInfo();
			if (count($arrErrors) > 0){
				$data['DTL_UID'] = $bk['DTL_UID'];
				$data['ENT_KBN_NO'] = $bk['ENT_KBN_NO'];
				$data['ENT_DTL_NAME'] = $bk['ENT_DTL_NAME'];
				$data['ENT_DTL_FORMAT'] = $bk['ENT_DTL_FORMAT'];
				$data['ENT_DTL_NUM'] = $bk['ENT_DTL_NUM'];
				$data['ENT_DTL_UNIT'] = $bk['ENT_DTL_UNIT'];
				$data['ENT_DTL_PRICE'] = $bk['ENT_DTL_PRICE'];
				$data['ENT_DTL_TOTAL'] = $bk['ENT_DTL_TOTAL'];
				$data['ENT_DTL_REMARK'] = $bk['ENT_DTL_REMARK'];
				$data['HDR_VARIOUS_RATE'] = $bk['HDR_VARIOUS_RATE'];
			}
			$rows 	= $clsEst->getDetailList();

		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	case 'master':
		$sDBmode 	= $data['mode'];
		$sDBmode2 	= $data['mode2'];
		$bkShokeihi = $data['HDR_VARIOUS_RATE'];
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])===true){
			$clsEst->setData($data,1);
			$clsEst->setWhere();
			$arrErrors = $clsEst->isValidDataTotal();
			if (count($arrErrors) == 0){
				$rtn = $clsEst->doAddFromMaster();
				$ContId = $clsEst->doUpdateTotal();
			}
			$data 	= $clsEst->getInfo();
			$rows 	= $clsEst->getDetailList();
		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	case 'move':
		$sDBmode 	= $data['mode'];
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])){
			$clsEst->setData($data,1);
			$arrErrors = $clsEst->isValidDataTotal();
			if (count($arrErrors) == 0){
				//削除処理
				if (!($clsEst->modPriority())){
					//メッセージ設定
					$arrErrors['ERROR'] = "system error: 順序の変更に失敗しました。";
				}else{
					$f_order = true;
				}
				$ContId = $clsEst->doUpdateTotal();
			}
			$clsEst->setWhere();
			$data 	= $clsEst->getInfo();
			$rows 	= $clsEst->getDetailList();
		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	//一覧から「削除」リンクが押された場合
	case 'del':
		$sDBmode 	= $data['mode'];
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])){
			$clsEst->setData($data,1);
			$clsEst->setWhere();
			$arrErrors = $clsEst->isValidDataTotal();
			if (count($arrErrors) == 0){
				//削除処理
				$ContId = $clsEst->doDeleteDetail();
				$ContId = $clsEst->doUpdateTotal();
				$f_del = true;
			}
			$data 	= $clsEst->getInfo();
			$rows 	= $clsEst->getDetailList();
		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	//一覧から「全明細削除」リンクが押された場合
	case 'delall':
		$sDBmode 	= $data['mode'];
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])){
			$clsEst->setData($data,1);
			$clsEst->setWhere();
			$arrErrors = $clsEst->isValidDataTotal();
			if (count($arrErrors) == 0){
				//削除処理
				$ContId = $clsEst->doDeleteDetailAll();
				$ContId = $clsEst->doUpdateTotal();
				$f_del = true;
			}
			$data 	= $clsEst->getInfo();
			$rows 	= $clsEst->getDetailList();
		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	//一覧から「編集」リンクが押された場合
	case 'show':
		$sDBmode 	= $data['mode'];
		$clsEst->setData($data,1);
		$clsEst->setWhere();
		$arrErrors = $clsEst->isValidDataTotal();
		if (count($arrErrors) == 0){
			//削除処理
			$dinfo = $clsEst->getDetailInfo();
			$ContId = $clsEst->doUpdateTotal();
		}
		$data 	= $clsEst->getInfo();
		$rows 	= $clsEst->getDetailList();
		$data['DTL_UID'] = $dinfo['DTL_UID'];
		$data['ENT_KBN_NO'] = $dinfo['DTL_KBN_NO'];
		$data['ENT_DTL_NAME'] = $dinfo['DTL_NAME'];
		$data['ENT_DTL_FORMAT'] = $dinfo['DTL_FORMAT'];
		$data['ENT_DTL_NUM'] = $dinfo['DTL_NUM'];
		$data['ENT_DTL_UNIT'] = $dinfo['DTL_UNIT'];
		$data['ENT_DTL_PRICE'] = $dinfo['DTL_PRICE'];
		$data['ENT_DTL_TOTAL'] = $dinfo['DTL_TOTAL'];
		$data['ENT_DTL_REMARK'] = $dinfo['DTL_REMARK'];
		$sDBmode2 	= "upd";
		
		break;
	default:
		$sDBmode 	= $data['mode'];
		//$sDBmode=updで登録画面を表示
//		$sDBmode2 	= "upd";
		//★二重投稿チェック
		$clsEst->setData($data,1);
		$clsEst->setWhere();
		$data 	= $clsEst->getInfo();
		if (strlen($data['HDR_UID']) == 0){
			$sErrorMsg = "見積書番号存在エラー";
			$sErrorMsg2 = "ご指定の見積書番号が存在しません。";
			$incFile	= 'error.inc';
		}
		break;
	}

	//★二重投稿エラー
	if ($bDouble===true){
		$sErrorMsg = "二重送信エラー";
		$sErrorMsg2 = "ブラウザの「戻る」や「進む」機能で二重に同じ処理をすることはできません。";
		$incFile	= 'error.inc';
	}

	include_once "header.inc";
	//★
	if (($incFile == 'nodata.inc')||($incFile == 'error.inc')){
		include_once $incFile;
		include_once "footer.inc";
	}else{
		$dstoken = doubleSubmit();	//新しいトークン発行
		include_once $incFile;
		include_once "footer.inc";
	}
	$clsEst->close();
	$clsUcw->close();


///テスト用
/*
$rows = array(0=>array('UCW_NAME'=>'諸　経　費'),1=>array('UCW_NAME'=>'人　件　費'),2=>array('UCW_NAME'=>'機　器　損　料'));
include_once "header.inc";
include_once $incFile;
include_once "footer.inc";
*/

?>
