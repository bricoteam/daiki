<script type="text/javascript">
<?php
	if (count($arrErrors)>0){
?>
    $(function() { 
        $.notifyBar({ html: "入力エラーがあります" });
    });
<?php
	}else if ($f_add === true){
?>
    $(function() { 
        $.notifyBar({ html: "登録しました" });
    });
<?php
	}else if ($f_upd === true){
?>
    $(function() { 
        $.notifyBar({ html: "登録しました" });
    });
<?php
	}else if ($f_order){
?>
    $(function() { 
        $.notifyBar({ html: "順番を変更しました。" });
    });
<?php
	}else if ($f_del){
?>
    $(function() { 
        $.notifyBar({ html: "削除しました。" });
    });
<?php
	}
?>
</script>
<script type="text/javascript" src="<?=URL_COMMON?>js/manage.js"></script>
<script language="javascript">
<!--
function doCommit() {
	var f = document.fList;
	f.m.value = 'commit';
	f.submit();
	return false;
}
function doReload() {
	var f = document.fList;
	f.m.value = 'reload';
	f.submit();
	return false;
}
function doDelete(hid,kno,did) {
	if (confirm('削除してよろしいですか？')){
		var f = document.fList;
		f.m.value = 'del';
		f.hid.value = hid;
		f.kno.value = kno;
		f.DTL_UID.value = did;
		f.submit();
	}
	return false;
}
function doDeleteAll(hid) {
	if (confirm('すべての明細を削除してよろしいですか？')){
		var f = document.fList;
		f.m.value = 'delall';
		f.hid.value = hid;
		f.submit();
	}
	return false;
}
function doShow(did) {
	var f = document.fList;
	f.m.value = 'show';
	f.DTL_UID.value = did;
	f.submit();
	return false;
}
function doCommitHeader() {
	var f = document.fList;
//	f.action = 'edit.php';
	f.m.value = 'commitHeader';
	f.submit();
	return false;
}
function doAddDetail() {
	var f = document.fList;
	f.m.value = 'add';
	f.submit();
	return false;
}
function doAddDetailMaster() {
	var f = document.fList;
	f.m.value = 'addmaster';
	f.submit();
	return false;
}
function doAddMaster() {
	var count = 0;
	for (var i=0;i<document.fList.elements['SEL_UID[]'].length;i++){
		if(document.fList.elements['SEL_UID[]'][i].checked){
			count++;
		}
	}
	if(count==0){
		alert("選択されていません。参照する行をチェックしてください。");
	}else{
		var f = document.fList;
		f.m.value = 'master';
		f.submit();
	}
	return false;
}
function doMove(hid,kno,did,pri,move) {
	var f = document.fList;
	f.m.value = 'move';
	f.hid.value = hid;
	f.kno.value = kno;
	f.did.value = did;
	f.pri.value = pri;
	f.move.value = move;
	f.submit();
	return false;
}
function doList() {
	var f = document.fList;
	f.action = 'index.php';
	f.m.value = '';
	f.HDR_UID.value = '';
	f.dstoken.value = '';
	f.mode.value = '';
	f.submit();
	return false;
}
function doBackSearch() {
	var f = document.fList;
	f.action = 'index.php';
	f.m.value = '';
	f.HDR_UID.value = '';
	f.dstoken.value = '';
	f.mode.value = '';
	f.back.value = '1';
	f.submit();
	return false;
}
function setFocus(id)
{
document.getElementById(id).focus();
}
function changeVK(){
	for (var i = 0; i < document.fList.HDR_VARIOUS_KIND.length; i++)
	if(document.fList.HDR_VARIOUS_KIND[i].checked == true)
	var rVK = document.fList.HDR_VARIOUS_KIND[i].value;
	if (rVK == '1'){
		document.getElementById('input09').value = '7.5';
	}else{
		document.getElementById('input09').value = '12';
	}
	doCalcVarious();
}

function doCalc()
{
	var total;
	var total2;
	if (document.getElementById('input06').value !== 0){
		total = document.getElementById('input06').value * document.getElementById('input04').value;
		total2 = Math.round(total/10);
		total = total2 * 10;
		document.getElementById('input07').value = total;
	}
}
function doCalcVarious()
{
	var total;
	var total2;
	var shokakari;

	for (var i = 0; i < document.fList.HDR_VARIOUS_KIND.length; i++)
	if(document.fList.HDR_VARIOUS_KIND[i].checked == true)
	var rVK = document.fList.HDR_VARIOUS_KIND[i].value;

	if (document.getElementById('input09').value.length == 0){
		document.getElementById('input09').value = 0;
	}
	//if ((document.getElementById('hidden_total').value > 0)&&(document.getElementById('input09').value >= 0)){
		if (rVK == '0'){
			shokakari = Math.round(((document.getElementById('hidden_labor_total').value) / 100) * document.getElementById('input09').value);
			total = parseInt(shokakari) + parseInt(document.getElementById('hidden_total').value);
		}else{
			shokakari = Math.round(((document.getElementById('hidden_total').value) / 100) * document.getElementById('input09').value);
			total = parseInt(shokakari) + parseInt(document.getElementById('hidden_total').value);
		}
		if (shokakari == 0){
			//諸経費ゼロ円なら調整しない
			total2 = total;
		}else{
			total2 = Math.round(total/1000);
			total2 = total2 * 1000;
			shokakari = parseInt(shokakari) + parseInt(total2 - total);
		}
		$('#total').html(number_format(total2));
		$('#various').html(number_format(shokakari));
	//}
	
}
function number_format(num){
 return num.toString().replace( /([0-9]+?)(?=(?:[0-9]{3})+$)/g , '$1,' );
}
function loadMaster(){
	var kbn;
<?php
$cntk = 0;
foreach($arrKbn as $key=>$val){
?>
	if (document.getElementById('input01').value == <?=$key?>){
		$('#SelTitle').html("<?=$val.'の内訳項目一覧'?>");
		kbn = <?=$key?>;
	}else
<?php
}
?>	{
		$('#SelTitle').html("");
		kbn = "999999999";
	}
	$('#SelUchiwake').load("getMaster.php",{k:kbn});
}
function openPdf(stmp){
	var date = new Date();
	var now = date.getTime();
	var childWindow = window.open('<?=URL_MANAGE?>pdf/rep01.php?id=<?=$data['HDR_UID']?>&stmp='+stmp+'&h='+now, '_blank');
}
    //-->
    </script>
  </head>

  <body>


  <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?=URL_MANAGE?>"><?=SITE_NAME?></a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="">
                <a href="<?=URL_MANAGE?>"><i class="icon-home icon-white"></i>&nbsp;HOME</a>
              </li>
              <li class="active">
                <a href="#">見積書入力</a>
              </li>
            </ul>
            <p class="navbar-text pull-right"><i class="icon-user icon-white"></i>&nbsp;<?= $_ADMIN[$_SESSION['USER']][1] ?>さんでログイン中&nbsp;<a href="<?=URL_MANAGE?>logout.php" class="btn btn-danger">ログアウト&nbsp;<i class="icon-share-alt icon-white"></i></a></p>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span2">
          <?php
	  include 'menu.inc';
	  ?>
        </div><!--/span-->
        <div class="span9">
          <h2>見積書入力(明細)</h2>
          <p class="lead"></p>
      <?php if (count($arrErrors) > 0){ ?>
          <div class="alert alert-error ">
            <a class="close" data-dismiss="alert">×</a>
	    <ul>
            <li>
            <?=implode('</li><li>',$arrErrors)?>
	    </li>
	    </ul>
          </div>
      <?php } ?>
          <form  class="form-horizontal" name="fList" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" >
          <fieldset>
            <input type="hidden" name="m" value="" />
            <input type="hidden" name="back" value="" />
            <input type="hidden" name="dstoken" value="<?=$dstoken?>" />
            <input type="hidden" name="HDR_UID" value="<?=$data['HDR_UID']?>" />
            <input type="hidden" name="HDR_NO" value="<?=$data['HDR_NO']?>" />
            <input type="hidden" name="mode" value="<?=$sDBmode?>" />
           <div class="row-fluid">
            <div class="control-group well BKyellow span5">
              <div style="float:left;margin-right:0px;">
              見積書番号&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><?=$data['HDR_NO']?></strong><br />
              受　付　日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><?=$data['HDR_DATE_ACCEPT']?></strong>
              </div>
              <div style="float:right;margin-right:0px;">
              作　成　日&nbsp;&nbsp;&nbsp;&nbsp;<strong><?=$data['HDR_DATE_CREATE_F']?></strong><br />
              見&nbsp;積&nbsp;日&nbsp;付&nbsp;&nbsp;&nbsp;<strong><?=$data['HDR_DATE_ESTIMATE']?></strong>
              </div>
            </div>
            <div class="control-group well span5 " style="float:right;margin-right:0px;">
              <div class="btn-group" >
              <a class="btn btn-primary" href="javascript:void(0);" onclick="doCommit();return false;">&nbsp;&nbsp;&nbsp;<i class="icon-ok icon-white"></i>&nbsp;登録&nbsp;&nbsp;&nbsp;</a>
              <a class="btn" href="javascript:void(0);" onClick="openPdf(0);"><i class="icon-print"></i>&nbsp;PDF(印無)</a>
              <a class="btn" href="javascript:void(0);" onClick="openPdf(1);"><i class="icon-print"></i>&nbsp;PDF(印有)</a>
              </div>
              <div><a href="javascript:void(0);" onclick="doCommitHeader();return false;" class="btn btn-primary" style="margin-top:3px;">&nbsp;&nbsp;登録してヘッダーへ&nbsp;&nbsp;</a>
	      <a href="javascript:void(0);" onclick="doBackSearch();return false;" class="btn">検索結果へ戻る</a>
              </div>
            </div>
           </div>

            <!--相手先情報-->
            <div class="row-fluid well">
                相手先名　　&nbsp;
                  <strong class="offset1"><?=mbConv(htmlspecialchars(br4nl(($data['HDR_CUSTOMER_NAME'])),ENT_QUOTES),$enc)?></strong>
                  &nbsp;&nbsp;<strong><?=mbConv(htmlspecialchars(br4nl(($data['HDR_CUSTOMER_CODE'])),ENT_QUOTES),$enc)?></strong><br />
                貴社注文番号
                  <strong class="offset1"><?=$data['HDR_ORDER_NO']?></strong>
                  &nbsp;&nbsp;<strong><?=mbConv(htmlspecialchars(br4nl(($data['HDR_ORDER_NO_B'])),ENT_QUOTES),$enc)?></strong>
            </div>

            <!--入力欄-->
            <div class="row-fluid well BKgreen">
              <div class="green">見積書明細入力</div><br />
              <input type="hidden" name="mode2" value="<?=$sDBmode2?>" />
              <input type="hidden" name="DTL_UID" value="<?=$data['DTL_UID']?>" />
              <input type="hidden" name="hid" value="" />
              <input type="hidden" name="kno" value="" />
              <input type="hidden" name="did" value="" />
              <input type="hidden" name="pri" value="" />
              <input type="hidden" name="move" value="" />




          <!-- sample modal content -->
          <div id="myModal" class="modal hide fade">
            <div class="modal-header">
              <a class="close" data-dismiss="modal" >&times;</a>
              <h3 id="SelTitle"></h3>
              <a href="#" class="btn btn-primary" onClick="doAddMaster();">選択した行を明細へ追加</a>
              <a href="#" class="btn" data-dismiss="modal" >キャンセル</a>
            </div>
            <div class="modal-body" id="SelUchiwake">
	    区分が選択されていません。区分を選択してから参照ボタンを押してください。
            </div>
            <div class="modal-footer">
              <a href="#" class="btn btn-primary" onClick="doAddMaster();">選択した行を明細へ追加</a>
              <a href="#" class="btn" data-dismiss="modal" >キャンセル</a>
            </div>
          </div>


              <div class="control-group">
                <div>
                <span onclick="setFocus('input01');">区分</span>
                  <?=getSelectBox ($arrKbn, 'ENT_KBN_NO', $data['ENT_KBN_NO'],'区分を選択',' id="input01" '.(($sDBmode2<>"upd")?'onChange="loadMaster();"':''))?>&nbsp;<span class="pink">※</span>
                  <?php
		  if ($sDBmode2=="upd"){
		  }else{
		  ?>
                  &nbsp;&nbsp;<a data-toggle="modal" href="#myModal" class="btn" >&nbsp;&nbsp;内訳項目マスタ一覧参照&nbsp;&nbsp;</a>
		<?php
		  }
		?>
                </div>
                <table style="margin-top:10px;">
                  <tr>
                    <th onclick="setFocus('input02');" style="font-weight:normal;">内訳項目名&nbsp;<span class="pink">※</span></th>
                    <th onclick="setFocus('input03');" style="font-weight:normal;">形式・形状</th>
                    <th onclick="setFocus('input04');" style="font-weight:normal;">数量&nbsp;<span class="pink">※</span></th>
                    <th onclick="setFocus('input05');" style="font-weight:normal;">単位&nbsp;<span class="pink">※</span></th>
                    <th onclick="setFocus('input06');" style="font-weight:normal;">単価</th>
                    <th onclick="setFocus('input07');" style="font-weight:normal;">金額&nbsp;<span class="pink">※</span></th>
                    <th onclick="setFocus('input08');" style="font-weight:normal;">備考</th>
                  </tr>
                  <tr>
                    <td><input type="text" name="ENT_DTL_NAME" value="<?=mbConv(htmlspecialchars(br4nl(($data['ENT_DTL_NAME'])),ENT_QUOTES),$enc)?>" class="span3" placeholder="内訳項目名" style="ime-mode: active;" id="input02" onChange="chg_Hankaku(this);" /></td>
                    <td><input type="text" name="ENT_DTL_FORMAT" value="<?=mbConv(htmlspecialchars(br4nl(($data['ENT_DTL_FORMAT'])),ENT_QUOTES),$enc)?>" class="span2" placeholder="形式・形状" style="ime-mode: active;" id="input03" onChange="chg_Hankaku(this);" /></td>
                    <td><input type="text" name="ENT_DTL_NUM" value="<?=mbConv(htmlspecialchars(br4nl(($data['ENT_DTL_NUM'])),ENT_QUOTES),$enc)?>" class="span1" placeholder="数量" style="ime-mode: disabled;" id="input04" onChange="doCalc();" /></td>
                    <td><input type="text" name="ENT_DTL_UNIT" value="<?=mbConv(htmlspecialchars(br4nl(($data['ENT_DTL_UNIT'])),ENT_QUOTES),$enc)?>" class="span1h" placeholder="単位" style="ime-mode: active;" id="input05" onChange="chg_Hankaku(this);" /></td>
                    <td><input type="text" name="ENT_DTL_PRICE" value="<?=mbConv(htmlspecialchars(br4nl(($data['ENT_DTL_PRICE'])),ENT_QUOTES),$enc)?>" class="span1h" placeholder="単価" style="ime-mode: disabled;" id="input06" onChange="doCalc();" /></td>
                    <td><input type="text" name="ENT_DTL_TOTAL" value="<?=mbConv(htmlspecialchars(br4nl(($data['ENT_DTL_TOTAL'])),ENT_QUOTES),$enc)?>" class="span1h" placeholder="金額" style="ime-mode: disabled;" id="input07" /></td>
                    <td><input type="text" name="ENT_DTL_REMARK" value="<?=mbConv(htmlspecialchars(br4nl(($data['ENT_DTL_REMARK'])),ENT_QUOTES),$enc)?>" class="span2" placeholder="備考" style="ime-mode: active;" id="input08" onChange="chg_Hankaku(this);" /></td>
                  </tr>
                </table>
              </div>
              <a href="javascript:void(0);" onclick="doAddDetail();return false;" class="btn btn-primary">&nbsp;&nbsp;&nbsp;<?=($sDBmode2=="upd")?'明細を変更':'新規明細を追加'?>&nbsp;&nbsp;&nbsp;</a>
                  <?php
		  if ($sDBmode2=="upd"){
		  ?>
                  &nbsp;&nbsp;<a href="javascript:void(0);" onclick="doReload();return false;" class="btn" >&nbsp;&nbsp;変更をキャンセル&nbsp;&nbsp;</a>
		<?php
		  }else{
		?>
                   &nbsp;&nbsp;<a href="javascript:void(0);" onclick="doAddDetailMaster();return false;" class="btn btn-warning" >&nbsp;新規明細を追加してマスタにも登録&nbsp;</a>
		<?php
		  }
		?>
           </div>



            <div class="row-fluid">
              
              <table class="table table-bordered table-striped span9" style="float:right;">
                <tr>
                  <th class="span2">諸経費区分</th>
                  <th class="span2">諸経費掛率</th>
                  <th class="span2">諸経費</th>
                  <th class="span2">明細合計</th>
                  <th class="span2">見積金額合計</th>
                </tr>
                <tr>
                  <input type="hidden" id="hidden_total" value="<?=nVal($data['HDR_ITEM_TOTAL'],0)?>" />
                  <input type="hidden" id="hidden_labor_total" value="<?=nVal($data['HDR_LABOR_TOTAL'],0)?>" />
                  <td class="span3"><label for="r01"><input type="radio" id="r01" name="HDR_VARIOUS_KIND" value="0" <?=($data['HDR_VARIOUS_KIND']=="0")?'checked':''?> onchange="changeVK();" />&nbsp;&nbsp;人件費に対して</label>
                                    <label for="r02"><input type="radio" id="r02" name="HDR_VARIOUS_KIND" value="1" <?=($data['HDR_VARIOUS_KIND']=="1")?'checked':''?> onchange="changeVK();" />&nbsp;&nbsp;明細合計に対して</label></td>
                  <td class="span2"><input type="text" name="HDR_VARIOUS_RATE" value="<?=$data['HDR_VARIOUS_RATE']?>" class="span1" placeholder="掛率" style="ime-mode: disabled;" id="input09" onchange="doCalcVarious();" />&nbsp;％</td>
                  <td class="span2" style="text-align:right;" id="various"><?=((strlen($data['HDR_VARIOUS_TOTAL'])>0)?number_format($data['HDR_VARIOUS_TOTAL']):'')?></td>
                  <td class="span2" style="text-align:right;"><?=((strlen($data['HDR_ITEM_TOTAL'])>0)?number_format($data['HDR_ITEM_TOTAL']):'')?></td>
                  <td class="span2" style="text-align:right;" id="total"><?=((strlen($data['HDR_TOTAL'])>0)?number_format($data['HDR_TOTAL']):'')?></td>
                </tr>
              </table>
            </div>
<?php
	//明細全部削除 2017/11/24 add suzuki
	if (($rows)&&(count($rows)>0)){
?>
            <div class="row-fluid">
              <a href="javascript:void(0);" onclick="doDeleteAll('<?=$data['HDR_UID']?>');return false;" class="btn btn-danger" style="float:right;"><i class="icon-trash icon-white"></i> 明細をすべて削除　</a>
            </div>
<?php
	}
$cnt = 0;
$bkKubun = "";
$total = 0;
if ((is_array($rows))&&(count($rows)>0)){
foreach($rows as $row){
	$cnt++;
	//
	if ($bkKubun <> $row['DTL_KBN_NO']){
		$bkKubun = $row['DTL_KBN_NO'];
		if ($cnt > 1){
?>
            <tr>
              <td colspan="5">
              </td>
              <td>小計</td>
              <td style="text-align:right;"><?=number_format($total)?></td>
              <td colspan="3"></td>
            </tr>
          </table>
        </div>
<?php
		$total = 0;
		}
?>
       <div>
          <h4><?=$arrKbn[$row['DTL_KBN_NO']]?></h4><br />
          <table class="table table-bordered table-striped">
            <tr>
              <th class="span1h">表示順序</th>
              <th class="span3">内訳項目名</th>
              <th class="span2">形式・形状</th>
              <th class="span1">数量</th>
              <th class="span1h">単位</th>
              <th class="span1h">単価</th>
              <th class="span1h">金額</th>
              <th class="span1h">備考</th>
              <th class="span1">編集</th>
              <th class="span1">削除</th>
            </tr>
<?php
	}
?>
            <tr>
              <td>
                  <a href="javascript:void(0);" onclick="doMove('<?=$row['DTL_HDR_UID']?>','<?=$row['DTL_KBN_NO']?>','<?=$row['DTL_UID']?>','<?=$row['DTL_PRIORITY']?>','up');return false;"><i class="icon-arrow-up"></i>上</a>&nbsp;/
                  <a href="javascript:void(0);" onclick="doMove('<?=$row['DTL_HDR_UID']?>','<?=$row['DTL_KBN_NO']?>','<?=$row['DTL_UID']?>','<?=$row['DTL_PRIORITY']?>','down');return false;">下<i class="icon-arrow-down"></i></a>
              </td>
              <td><?=nVal($row['DTL_NAME'],'&nbsp;')?></td>
              <td><?=$row['DTL_FORMAT']?></td>
              <td style="text-align:right;"><?=nVal($row['DTL_NUM'],'&nbsp;')?></td>
              <td><?=nVal($row['DTL_UNIT'],'&nbsp;')?></td>
              <td style="text-align:right;"><?=((strlen($row['DTL_PRICE'])>0)?number_format($row['DTL_PRICE']):'&nbsp;')?></td>
              <td style="text-align:right;"><?=((strlen($row['DTL_TOTAL'])>0)?number_format($row['DTL_TOTAL']):'&nbsp;')?></td>
              <td><?=nVal($row['DTL_REMARK'],'&nbsp;')?></td>
              <td><a href="javascript:void(0);" onclick="doShow('<?=$row['DTL_UID']?>');return false;" class="btn btn-success"><i class="icon-pencil icon-white"></i></a></td>
              <td><a href="javascript:void(0);" onclick="doDelete('<?=$row['DTL_HDR_UID']?>','<?=$row['DTL_KBN_NO']?>','<?=$row['DTL_UID']?>');return false;" class="btn btn-danger"><i class="icon-trash icon-white"></i></a></td>
            </tr>
<?php
	$total = $total + $row['DTL_TOTAL'];
}
?>
            <tr>
              <td colspan="5">
              </td>
              <td>小計</td>
              <td style="text-align:right;"><?=number_format($total)?></td>
              <td colspan="3"></td>
            </tr>
          </table>
        </div>
<?php
}
?>
            <div class="well row-fluid">
              <a href="javascript:void(0);" onclick="doCommitHeader();return false;" class="btn btn-primary span3">登録してヘッダーへ</a>&nbsp;&nbsp;&nbsp;
              <a href="javascript:void(0);" onclick="doList();return false;" class="btn">一覧へ戻る</a>
	      <a href="javascript:void(0);" onclick="doBackSearch();return false;" class="btn">検索結果へ戻る</a>
            </div>
          </fieldset>
          </form>	
        </div>
      </div>