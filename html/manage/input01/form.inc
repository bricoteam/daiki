<script type="text/javascript">
<?php
	if (count($arrErrors)>0){
?>
    $(function() { 
        $.notifyBar({ html: "入力エラーがあります" });
    });
<?php
	}else if ($f_add === true){
?>
    $(function() { 
        $.notifyBar({ html: "登録しました" });
    });
<?php
	}
?>
</script>
<script type="text/javascript" src="<?=URL_COMMON?>js/manage.js"></script>
<script language="javascript">
<!--
function doCommit() {
	var f = document.fList;
	f.m.value = 'commit';
	f.submit();
	return false;
}
function doCommitList() {
	var f = document.fList;
	f.action = 'detail.php';
	f.m.value = 'commitList';
	f.submit();
	return false;
}

function doCopy() {
	var f = document.fList;
	f.m.value = 'copy';
	f.submit();
	return false;
}
function doList() {
	var f = document.fList;
	f.action = 'index.php';
	f.m.value = '';
	f.HDR_UID.value = '';
	f.dstoken.value = '';
	f.mode.value = '';
	f.submit();
	return false;
}
function doBackSearch() {
	var f = document.fList;
	f.action = 'index.php';
	f.m.value = '';
	f.HDR_UID.value = '';
	f.dstoken.value = '';
	f.mode.value = '';
	f.back.value = '1';
	f.submit();
	return false;
}
function setFocus(id)
{
document.getElementById(id).focus();
}

function setCustomer(obj){
	//見積書番号の頭一桁から相手先名、取引先コードをセットする
	var str = obj.value;
	if (str.substring(0,1) == 'A'){
		//document.getElementById('input06').value = 'GTA1051';
		document.getElementById('input06').value = 'S0003701';
		
	}else if (str.substring(0,1) == 'F'){
		//document.getElementById('input06').value = 'GTA1051';
		document.getElementById('input06').value = 'S0003701';
		
	}else{
		document.getElementById('input06').value = '';
	}
<?php
foreach ($_CUSTOMER as $key => $val){
?>
	if (str.substring(0,1) == '<?=$key?>'){
		document.getElementById('input05').value = '<?=$val?>';
	}
<?php
}
?>
}
function openPdf(stmp){
	var date = new Date();
	var now = date.getTime();
	var childWindow = window.open('<?=URL_MANAGE?>pdf/rep01.php?id=<?=$data['HDR_UID']?>&stmp='+stmp+'&h='+now, '_blank');
}
    //-->
    </script>

  </head>

  <body>


  <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?=URL_MANAGE?>"><?=SITE_NAME?></a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="">
                <a href="<?=URL_MANAGE?>"><i class="icon-home icon-white"></i>&nbsp;HOME</a>
              </li>
              <li class="active">
                <a href="#">見積書入力</a>
              </li>
            </ul>
            <p class="navbar-text pull-right"><i class="icon-user icon-white"></i>&nbsp;<?= $_ADMIN[$_SESSION['USER']][1] ?>さんでログイン中&nbsp;<a href="<?=URL_MANAGE?>logout.php" class="btn btn-danger">ログアウト&nbsp;<i class="icon-share-alt icon-white"></i></a></p>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span2">
          <?php
	  include 'menu.inc';
	  ?>
        </div><!--/span-->
        <div class="span9">
          <h2>見積書入力(<?=($sDBmode=="new")?'新規':'変更'?>)</h2>
          <p class="lead"></p>
      <?php if (count($arrErrors) > 0){ ?>
          <div class="alert alert-error ">
            <a class="close" data-dismiss="alert">×</a>
	    <ul>
            <li>
            <?=implode('</li><li>',$arrErrors)?>
	    </li>
	    </ul>
          </div>
      <?php } ?>
          <form  class="form-horizontal" name="fList" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" >
          <fieldset>
            <input type="hidden" name="m" value="" />
            <input type="hidden" name="back" value="" />
            <input type="hidden" name="dstoken" value="<?=$dstoken?>" />
            <input type="hidden" name="HDR_UID" value="<?=$data['HDR_UID']?>" />
            <input type="hidden" name="HDR_NO_COPY" value="<?=$data['HDR_NO_COPY']?>" />
            <input type="hidden" name="mode" value="<?=$sDBmode?>" />
<?php
if ($sDBmode=="new"){
?>
           <div class="row-fluid">
            <div class="control-group well BKyellow span5">
              <span onclick="setFocus('input01');">複写見積書番号</span>
              <div class="input-append">
                <input type="text" name="COPY_HDR_NO" value="<?=mbConv(htmlspecialchars(br4nl(($data['COPY_HDR_NO'])),ENT_QUOTES),$enc)?>" class="span1h" placeholder="複写見積書番号" style="ime-mode: disabled;" id="input01" onChange="toUpper(this);chg_Hankaku(this);" /><a href="javascript:void(0);return false;" onclick="doCopy();" class="btn" >複写</a>
              </div>
            </div>
            <div class="control-group well span5 " style="float:right;margin-right:0px;">
              <div class="btn-group" >
              <a class="btn btn-primary" href="javascript:void(0);" onclick="doCommit();return false;">&nbsp;&nbsp;&nbsp;<i class="icon-ok icon-white"></i>&nbsp;登録&nbsp;&nbsp;&nbsp;</a>
              <a class="btn" href="javascript:void(0);" onClick="openPdf(0);"><i class="icon-print"></i>&nbsp;PDF(印無)</a>
              <a class="btn" href="javascript:void(0);" onClick="openPdf(1);"><i class="icon-print"></i>&nbsp;PDF(印有)</a>
              </div>
            </div>
           </div>
<?php
}else{
?>
           <div class="row-fluid">
            <div class="control-group well span5 " style="float: right;">
              <div class="btn-group" >
              <a class="btn btn-primary" href="javascript:void(0);" onclick="doCommit();return false;">&nbsp;&nbsp;&nbsp;<i class="icon-ok icon-white"></i>&nbsp;登録&nbsp;&nbsp;&nbsp;</a>
              <a class="btn" href="javascript:void(0);" onClick="openPdf(0);"><i class="icon-print"></i>&nbsp;PDF(印無)</a>
              <a class="btn" href="javascript:void(0);" onClick="openPdf(1);"><i class="icon-print"></i>&nbsp;PDF(印有)</a>
              </div>
            </div>
           </div>
<?php
}
?>
            <div class="control-group">
              <label class="control-label" for="input02">見積書番号</label>
              <div class="controls">
                <input type="text" name="HDR_NO" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_NO'])),ENT_QUOTES),$enc)?>" class="span1h" placeholder="見積書番号を入力" style="ime-mode: disabled;" id="input02" onChange="toUpper(this);chg_Hankaku(this);setCustomer(this);" />&nbsp;<span class="pink">※必須</span>
                <span class="offset1">
                  作&nbsp;成&nbsp;日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><?=(strlen($data['HDR_DATE_CREATE_F'])==0)?date('Y/m/d'):$data['HDR_DATE_CREATE_F']?></strong>
                </span>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="input03">受付日</label>
              <div class="controls">
                <input type="text" name="HDR_DATE_ACCEPT" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_DATE_ACCEPT'])),ENT_QUOTES),$enc)?>" class="span1h" placeholder="受付日を入力" style="ime-mode: disabled;" id="input03" onChange="dateFormat(this);" />&nbsp;<span class="pink">※必須</span>
                <span class="offset1">
                <span onclick="setFocus('input04');">見積日付</span>&nbsp;&nbsp;&nbsp;<!--/label-->
                  <input type="text" name="HDR_DATE_ESTIMATE" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_DATE_ESTIMATE'])),ENT_QUOTES),$enc)?>" class="span1h" placeholder="見積日付を入力" style="ime-mode: disabled;" id="input04" onChange="dateFormat(this);" />&nbsp;<span class="pink">※必須</span>
                </span>
              </div>
            </div>
	    <!--相手先情報-->
            <div class="row-fluid well">
              <div class="control-group">
                <label class="control-label" for="input05">相手先名</label>
                <div class="controls">
                  <input type="text" name="HDR_CUSTOMER_NAME" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_CUSTOMER_NAME'])),ENT_QUOTES),$enc)?>" class="span4" placeholder="相手先名を入力" style="ime-mode: active;" id="input05" onChange="chg_Hankaku(this);" />&nbsp;<span class="pink">※必須</span>&nbsp;<input type="text" name="HDR_CUSTOMER_CODE" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_CUSTOMER_CODE'])),ENT_QUOTES),$enc)?>" class="span1h" placeholder="取引先コード" style="ime-mode: disabled;" id="input06" onChange="toUpper(this);chg_Hankaku(this);" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="input07">貴社注文番号</label>
                <div class="controls">
                  <input type="text" name="HDR_ORDER_NO" value="<?=$data['HDR_ORDER_NO']?>" class="span2" placeholder="オーダーNo." style="ime-mode: disabled;" id="input07" onChange="toUpper(this);" />
                  &nbsp;<input type="text" name="HDR_ORDER_NO_B" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_ORDER_NO_B'])),ENT_QUOTES),$enc)?>" class="span1" placeholder="連番" style="ime-mode: disabled;" id="input08" onChange="toUpper(this);" />
                </div>
              </div>
              <div class="control-group" style="margin-bottom:0;">
                <label class="control-label" for="input09">所属</label>
                <div class="controls">
                  <input type="text" name="HDR_DEPT" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_DEPT'])),ENT_QUOTES),$enc)?>" class="span2" placeholder="所属を入力" style="ime-mode: active;" id="input09" onChange="chg_Hankaku(this);" />
                  <span class="offset1">
                  <span onclick="setFocus('input10');">担当</span>&nbsp;&nbsp;&nbsp;<!--/label-->
                    <input type="text" name="HDR_CHARGER" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_CHARGER'])),ENT_QUOTES),$enc)?>" class="span2" placeholder="担当を入力" style="ime-mode: active;" id="input10" onChange="chg_Hankaku(this);" />
                  </span>
                </div>
              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="input11">件名</label>
              <div class="controls">
                <input type="text" name="HDR_TITLE_1" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_TITLE_1'])),ENT_QUOTES),$enc)?>" class="span5" placeholder="件名１行目を入力" style="ime-mode: active;" id="input11" onChange="chg_Hankaku(this);" />&nbsp;<span class="pink">※必須</span><br />
                <input type="text" name="HDR_TITLE_2" value="<?=mbConv(htmlspecialchars(br4nl(($data['HDR_TITLE_2'])),ENT_QUOTES),$enc)?>" class="span5" placeholder="件名２行目を入力" style="ime-mode: active;margin-top:2px;" id="input12" onChange="chg_Hankaku(this);" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="input13">納期及び受渡場所</label>
              <div class="controls">
                <input type="text" name="HDR_DELIVERY_TIME_PLACE" value="<?=$data['HDR_DELIVERY_TIME_PLACE']?>" class="span4" placeholder="納期及び受渡場所を入力" style="ime-mode: active;" id="input13" onChange="chg_Hankaku(this);" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="input14">取引条件</label>
              <div class="controls">
                <input type="text" name="HDR_CONDITION" value="<?=$data['HDR_CONDITION']?>" class="span4" placeholder="取引条件を入力" style="ime-mode: active;" id="input14" onChange="chg_Hankaku(this);" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="input15">見積書有効期限</label>
              <div class="controls">
                <input type="text" name="HDR_EXPIRATION_DATE" value="<?=$data['HDR_EXPIRATION_DATE']?>" class="span2" placeholder="見積書有効期限を入力" style="ime-mode: active;" id="input14" onChange="chg_Hankaku(this);" />
              </div>
            </div>
            <div class="well row-fluid">
              <a href="javascript:void(0);" onclick="doCommitList();return false;" class="btn btn-primary span3">登録して明細入力へ</a>&nbsp;&nbsp;&nbsp;
              <a href="javascript:void(0);" onclick="doList();return false;" class="btn">一覧へ戻る</a>
              <a href="javascript:void(0);" onclick="doBackSearch();return false;" class="btn">検索結果へ戻る</a>
            </div>
          </fieldset>
          </form>	
        </div>
      </div>
	
