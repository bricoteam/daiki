<?
	/***************************************************************************
	 * Name 		:index.php
	 * Description 		:CMSメニュー
	 * Include		:func.common.inc
	 * 			 	func.field.inc
	 * 				func.fieldcheck.inc
	 * Trigger		:
	 * Create		:2012/05/01 Brico Suzuki
	 * LastModify		:
	 *
	 *
	 *
	 **************************************************************************/
	if ($_SERVER[SERVER_NAME] == 'daiki.bricoleur.in'){
		require_once('ini.inc');
	}
	include_once 'func.common.inc';
	include_once 'func.field.inc';
	include_once 'func.fieldcheck.inc';

	session_start();
	$data = $_REQUEST;
	
	//ログインチェック
	$blogin = isLogin();
	if (!($blogin)){
		header("Location: login.php");
		exit;
	}
	
	include_once "header.inc";
	include_once "menu.inc";
	include_once "footer.inc";
	

?>
