<script type="text/javascript">
<?php
	if (count($arrErrors)>0){
?>
    $(function() { 
        $.notifyBar({ html: "入力エラーがあります" });
    });
<?php
	}else if ($f_upd){
?>
    $(function() { 
        $.notifyBar({ html: "予定を登録しました。" });
    });
<?php
	}
?>
</script>
<script type="text/javascript" src="<?=URL_COMMON?>js/manage.js"></script>
<script language="javascript">
<!--
function doCommit() {
	var f = document.fList;
	f.m.value = 'commit';
	f.submit();
	return false;
}
function doCommitBk() {
	var f = document.fList;
	f.m.value = 'commitBk';
	f.submit();
	return false;
}

    //-->
    </script>

  </head>

  <body data-spy="scroll" data-target=".subnav" data-offset="50" >


  <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?=URL_MANAGE?>"><?=SITE_NAME?></a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="">
                <a href="<?=URL_MANAGE?>"><i class="icon-home icon-white"></i>&nbsp;HOME</a>
              </li>
              <li class="active">
                <a href="#">作業予定表入力（現場入力）</a>
              </li>
            </ul>
            <p class="navbar-text pull-right"><i class="icon-user icon-white"></i>&nbsp;<?= $_ADMIN[$_SESSION['USER']][1] ?>さんでログイン中&nbsp;<a href="<?=URL_MANAGE?>logout.php" class="btn btn-danger">ログアウト&nbsp;<i class="icon-share-alt icon-white"></i></a></p>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span2">
          <?php
	  include 'menu.inc';
	  ?>
        </div><!--/span-->
        <div class="span9">
          <h2>作業予定表入力（予定入力）</h2>
          <p class="lead"></p>
      <?php if (count($arrErrors) > 0){ ?>
          <div class="alert alert-error ">
            <a class="close" data-dismiss="alert">×</a>
	    <ul>
            <li>
            <?=implode('</li><li>',$arrErrors)?>
	    </li>
	    </ul>
          </div>
      <?php } ?>
          <div class="row-fluid">
            <div class="control-group well span5 " style="float:right;margin-right:0px;">
              <a class="btn btn-primary" href="javascript:void(0);" onclick="doCommit();return false;">&nbsp;&nbsp;<i class="icon-ok icon-white"></i>&nbsp;登録&nbsp;</a>
              <a class="btn btn-primary" href="javascript:void(0);" onclick="doCommitBk();return false;">&nbsp;登録して現場入力へ&nbsp;</a>
              <a class="btn" href="index.php?sel_nengetsu=<?=$sel_nengetsu?>">&nbsp;一覧へ戻る&nbsp;</a><div></div>
            </div>
          </div>
          <h2><?=$dispDate?></h2>
          <h3>昼間</h3>
          <form name="fList" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>"  >
            <input type="hidden" name="dstoken" value="<?=$dstoken?>" />
            <input type="hidden" name="m" value="" />
            <input type="hidden" name="dstoken" value="<?=$dstoken?>" />
            <input type="hidden" name="SPD_UID" value="<?=(($data['SPD_DAYNIGHT']=='0')?$data['SPD_UID']:'')?>" />
            <input type="hidden" name="SPD_DATE_SCHEDULE" value="<?=$data['SPD_DATE_SCHEDULE']?>" />
            <input type="hidden" name="ymd" value="<?=$data['ymd']?>" />
            <input type="hidden" name="uid" value="" />
            <input type="hidden" name="pri" value="" />
            <input type="hidden" name="move" value="" />
            <input type="hidden" name="mode" value="<?=$sDBmode?>" />
          <!--pageSet -->
          <table class="table table-bordered ">
            <tr class="BKgray">
              <th class="span3">現場名</th>
              <th class="span1" style="text-align:center;">人<br>数</th>
<?php
	//menbers
	foreach ($arrMember as $arrMem){
		$cls = "";
		if ($arrMem['HONSHA']=='1'){
			$cls = 'class="BKyellow"';
		}else if ($arrMem['KBN']=='1'){
			$cls = 'class="BKpblue"';
		}
?>
              <th style="text-align:center;" <?=$cls?>><?=preg_replace("/./u","$0<br>\n",$arrMem['NICK']);?></th>
<?php
	}
?>
            </tr>
<?php
$cnt = 0;
if ((is_array($rows))&&(count($rows)>0)){
foreach($rows as $row){
	$cnt++;
	$trclass = "";
	if ($row['SPD_HOLIDAY_FLG']=="1"){
		$trclass = ' class="BKppink"';
	//2017.02.13申請休追加
	}else if ($row['SPD_HOLIDAY_FLG']=="3"){
		$trclass = ' class="BKbpink"';
	}
?>
            <tr <?=$trclass?>>
              <td><?=$row['SPD_PLACE_NAME']?></td>
              <td style="text-align:center;"><?=$row['SPD_REQUIRE_NUM']?></td>
<?php
	//menbers
	foreach ($arrMember as $arrMem){
		$checked = '';
		if ($arrSchedule[$row['SPD_PLACE_NO']][$arrMem['KBN'].'|'.$arrMem['NO']]==1){
			$checked = ' checked="checked"';
		}
?>
              <td style="text-align:center;"><input type="checkbox" name="chk[<?=$row['SPD_PLACE_NO']?>][<?=$arrMem['KBN']?>|<?=$arrMem['NO']?>]" <?=$checked?> /></td>
<?php
	}
?>
            </tr>
<?php
}
}
?>
          </table>
          <h2><?=$dispDate?></h2>
          <h3>夜間</h3>
          <!--pageSet -->
          <table class="table table-bordered">
            <tr class="BKgray">
              <th class="span3">現場名</th>
              <th class="span1" style="text-align:center;">人<br>数</th>
<?php
	//menbers
	foreach ($arrMember as $arrMem){
		$cls = "";
		if ($arrMem['HONSHA']=='1'){
			$cls = 'class="BKyellow"';
		}else if ($arrMem['KBN']=='1'){
			$cls = 'class="BKpblue"';
		}
?>
              <th style="text-align:center;" <?=$cls?>><?=preg_replace("/./u","$0<br>\n",$arrMem['NICK']);?></th>
<?php
	}
?>
            </tr>
<?php
$cnt = 0;
if ((is_array($rows2))&&(count($rows2)>0)){
foreach($rows2 as $row){
	$cnt++;
	$trclass = "";
	//2017.02.13申請休追加
	if ($row['SPD_HOLIDAY_FLG']=="3"){
		$trclass = ' class="BKbpink"';
	}
?>
            <tr <?=$trclass?>>
              <td><?=$row['SPD_PLACE_NAME']?></td>
              <td style="text-align:center;"><?=$row['SPD_REQUIRE_NUM']?>&nbsp;</td>
<?php
	//menbers
	foreach ($arrMember as $arrMem){
		$checked = '';
		if ($arrSchedule[$row['SPD_PLACE_NO']][$arrMem['KBN'].'|'.$arrMem['NO']]==1){
			$checked = ' checked="checked"';
		}
?>
              <td style="text-align:center;"><input type="checkbox" name="chk[<?=$row['SPD_PLACE_NO']?>][<?=$arrMem['KBN']?>|<?=$arrMem['NO']?>]" <?=$checked?> /></td>
<?php
	}
?>
            </tr>
<?php
}
}
?>
          </table>
          </form>	
          <div class="row-fluid">
            <div class="control-group well span5 " style="float:right;margin-right:0px;">
              <a class="btn btn-primary" href="javascript:void(0);" onclick="doCommit();return false;">&nbsp;&nbsp;<i class="icon-ok icon-white"></i>&nbsp;登録&nbsp;</a>
              <a class="btn btn-primary" href="javascript:void(0);" onclick="doCommitBk();return false;">&nbsp;登録して現場入力へ&nbsp;</a>
              <a class="btn" href="index.php?sel_nengetsu=<?=$sel_nengetsu?>">&nbsp;一覧へ戻る&nbsp;</a><div></div>
            </div>
          </div>
        </div>
      </div>