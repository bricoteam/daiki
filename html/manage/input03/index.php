<?
	/***************************************************************************
	 * Name 		:index.php
	 * Description 		:登録(新規コンテンツ)
	 * Include		:func.common.inc
	 * 			 	func.field.inc
	 * 				func.fieldcheck.inc
	 * 			 	class.cls_schedule.inc
	 * Trigger		:
	 * Create		:2013/09/01 Brico Suzuki
	 * LastModify		:
	 *
	 *
	 *
	 **************************************************************************/
	if ($_SERVER[SERVER_NAME] == 'daiki.bricoleur.in'){
		require_once('ini.inc');
	}
	include_once 'func.common.inc';
	include_once 'func.field.inc';
	include_once 'func.fieldcheck.inc';

	include_once 'class.cls_schedule.inc';

	session_start();
	$data = $_REQUEST;

	//ログインチェック
	$blogin = isLogin();
	if (!($blogin)){
		header("Location: ".URL_LOGIN);
		exit;
	}
	$incFile = "list.inc";
	
	//年月度プルダウンを作成する
	//当月度を基準に過去半年分と、未来半年分
	$nowy = date("Y");
	$nowm = date("m");
	$nowd = date("d");
	if ($nowd < 16){
		$getudo['y'] = $nowy;
		$getudo['m'] = $nowm;
	}else{
		$getudo['y'] = date("Y", strtotime($nowy."-".$nowm."-01 +1 month"));
		$getudo['m'] = date("m", strtotime($nowy."-".$nowm."-01 +1 month"));
	}
	//過去Loop
	$a = (-6);
	while ($a < 7){
		if ($a < 0 ){
			$add = $a;
		}else{
			$add = "+".$a;
		}
		$key = date("Ym", strtotime($getudo['y']."-".$getudo['m']."-01 ".$add." month"));
		$arrMonth[$key] = date("Y年m月度", strtotime($getudo['y']."-".$getudo['m']."-01 ".$add." month"));
		$a++;
	}
	
	/*選択された年度*/
	if (strlen($data['sel_nengetsu'])> 0){
		$sel_nen = substr($data['sel_nengetsu'],0,4);
		$sel_getsu = substr($data['sel_nengetsu'],4,2);
	}else{
		$sel_nen = $getudo['y'];
		$sel_getsu = $getudo['m'];
	}
	$sel_nen2 = date("Y", strtotime($sel_nen."-".$sel_getsu."-01 -1 month"));
	$sel_getsu2 = date("m", strtotime($sel_nen."-".$sel_getsu."-01 -1 month"));
	
	$before_nengetsu = $sel_nen2.$sel_getsu2;
	$next_nengetsu = date("Ym", strtotime($sel_nen."-".$sel_getsu."-01 +1 month"));
	
	$data['sel_nengetsu'] = $sel_nen.$sel_getsu;

	$err = 0;
	$enc = 0;
	
	$clsSch 	= new cls_schedule();

	$incFile	= 'list.inc';


	include_once "header.inc";
	include_once $incFile;
	include_once "footer.inc";

	$clsSch->close();



?>
