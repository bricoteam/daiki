<?
	/***************************************************************************
	 * Name 		:detail.php
	 * Description 		:登録(新規コンテンツ)
	 * Include		:func.common.inc
	 * 			 	func.field.inc
	 * 				func.fieldcheck.inc
	 * 			 	class.cls_estimate.inc
	 * Trigger		:
	 * Create		:2009/10/01 Brico Suzuki
	 * LastModify		:
	 *
	 *
	 *
	 **************************************************************************/
	if ($_SERVER[SERVER_NAME] == 'daiki.bricoleur.in'){
		require_once('ini.inc');
	}
	include_once 'func.common.inc';
	include_once 'func.field.inc';
	include_once 'func.fieldcheck.inc';

	include_once 'class.cls_schedule.inc';

	session_start();
	$data = $_REQUEST;

	//ログインチェック
	$blogin = isLogin();
	if (!($blogin)){
		header("Location: ".URL_LOGIN);
		exit;
	}
	
	$clsSch 	= new cls_schedule();

	if (!isset($data['SPD_DATE_SCHEDULE'])){
		$sErrorMsg = "パラメータエラー";
		$sErrorMsg2 = "日付の引数が不正です。カレンダーで編集したい日付をクリックしてください。";
		$incFile = 'error.inc';
	}else{	

		$incFile = "detail.inc";

		$err = 0;
		$enc = 0;
		$bDouble = false;
	
		$f_upd = false;
		//「新規登録」ボタンが押された場合
		switch ($data['m']) {
		case 'commitBk':
			$sDBmode 	= $data['mode'];
			//★二重投稿チェック
			if (doubleSubmit($data['dstoken'])===true){
				$clsSch->setData($data,1);
				$ContId = $clsSch->doInsertSchedule($data['SPD_DATE_SCHEDULE']);
				header("Location: edit.php?ymd=".$data['ymd']);
			}else{
				//★二重投稿
				$bDouble = true;
			}
			break;
		case 'commit':
			//諸経費更新
			$sDBmode 	= $data['mode'];
			//★二重投稿チェック
			if (doubleSubmit($data['dstoken'])===true){
				$clsSch->setData($data,1);
				$ContId = $clsSch->doInsertSchedule($data['SPD_DATE_SCHEDULE']);
				$f_upd = true;

			}else{
				//★二重投稿
				$bDouble = true;
			}
			break;
		default:
			break;
		}
		
		$nowy = substr($data['ymd'],0,4);
		$nowm = substr($data['ymd'],4,2);
		$nowd = substr($data['ymd'],6,2);
		if ($nowd < 16){
			$sel_nengetsu = $nowy.$nowm;
		}else{
			$sel_nengetsu = date("Y", strtotime($nowy."-".$nowm."-01 +1 month")).date("m", strtotime($nowy."-".$nowm."-01 +1 month"));
		}
		$dispDate = substr($data['ymd'],0,4)."年 ".substr($data['ymd'],4,2)."月 ".substr($data['ymd'],6,2)."日";
		
		//休日データを登録する。
		$clsSch->setData($data,1);
		$clsSch->doInsertHoliday();
		
		//作業員リスト取得（登録済みデータと、マスタから）
		$arrMember = $clsSch->getMembersList();

		//現場名リスト取得
		$clsSch->setData($data,0);
		$clsSch->setWhere(0);
		$rows 	= $clsSch->getList(1);
		$clsSch->setWhere(1);
		$rows2 	= $clsSch->getList(1);
	
		//予定情報取得
		$arrSchedule = $clsSch->getScheduleList();

	}
	//★二重投稿エラー
	if ($bDouble===true){
		$sErrorMsg = "二重送信エラー";
		$sErrorMsg2 = "ブラウザの「戻る」や「進む」機能で二重に同じ処理をすることはできません。";
		$incFile	= 'error.inc';
	}

	include_once "header.inc";
	//★
	if (($incFile == 'nodata.inc')||($incFile == 'error.inc')){
		include_once $incFile;
		include_once "footer.inc";
	}else{
		$dstoken = doubleSubmit();	//新しいトークン発行
		include_once $incFile;
		include_once "footer.inc";
	}
	$clsSch->close();



?>
