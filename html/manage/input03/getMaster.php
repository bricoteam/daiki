<?
	/***************************************************************************
	 * Name 		:index.php
	 * Description 		:登録(新規コンテンツ)
	 * Include		:func.common.inc
	 * 			 	func.field.inc
	 * 				func.fieldcheck.inc
	 * 			 	class.cls_contents.inc
	 * Trigger		:
	 * Create		:2009/10/01 Brico Suzuki
	 * LastModify		:
	 *
	 *
	 *
	 **************************************************************************/
	if ($_SERVER[SERVER_NAME] == 'daiki.bricoleur.in'){
		require_once('ini.inc');
	}
	include_once 'func.common.inc';
	include_once 'func.field.inc';
	include_once 'func.fieldcheck.inc';

	include_once 'class.cls_kubun.inc';
	include_once 'class.cls_uchiwake.inc';

	session_start();
	$data['SEL_UCW_KBN_NO'] = $_REQUEST['k'];

	//ログインチェック
	$blogin = isLogin();
	if (!($blogin)){
		header("Location: ".URL_LOGIN);
		exit;
	}
	//区分リスト取得
	$clsKbn 	= new cls_kubun();
	$arrKbn	= $clsKbn->getAllList();
	$clsKbn->close();

	$clsUcw 	= new cls_uchiwake();

	$clsUcw->setData($data,1);
	//一覧を表示
	$clsUcw->setWhere();
	$rows 	= $clsUcw->getList();
	$clsUcw->close();



?>
          <!--pageSet -->
          <table class="table table-bordered table-striped">
            <tr>
              <th class="span1">選択</th>
              <th class="">内訳項目名</th>
              <th class="">形式・形状</th>
              <th class="">数量</th>
              <th class="">単位</th>
              <th class="">単価</th>
              <th class="">金額</th>
            </tr>
<?php
$cnt = 0;
if ((is_array($rows))&&(count($rows)>0)){
foreach($rows as $row){
	$cnt++;
?>
            <tr>
              <td><input type="checkbox" name="SEL_UID[]" value="<?=$row['UCW_UID']?>" /></td>
              <td><?=$row['UCW_NAME']?></td>
              <td><?=$row['UCW_FORMAT']?></td>
              <td><?=nVal($row['UCW_NUM'],'&nbsp;')?></td>
              <td><?=nVal($row['UCW_UNIT'],'&nbsp;')?></td>
              <td><?=((strlen($row['UCW_PRICE'])>0)?number_format($row['UCW_PRICE']):'&nbsp;')?></td>
              <td><?=((strlen($row['UCW_TOTAL'])>0)?number_format($row['UCW_TOTAL']):'&nbsp;')?></td>
            </tr>
<?php
}
}
?>
          </table>
<input style="display:none;" type="checkbox" name="SEL_UID[]" value="" />