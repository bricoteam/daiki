<?
	/***************************************************************************
	 * Name 		:index.php
	 * Description 		:登録(新規コンテンツ)
	 * Include		:func.common.inc
	 * 			 	func.field.inc
	 * 				func.fieldcheck.inc
	 * 			 	class.cls_contents.inc
	 * Trigger		:
	 * Create		:2009/10/01 Brico Suzuki
	 * LastModify		:
	 *
	 *
	 *
	 **************************************************************************/
	if ($_SERVER[SERVER_NAME] == 'daiki.bricoleur.in'){
		require_once('ini.inc');
	}
	include_once 'func.common.inc';
	include_once 'func.field.inc';
	include_once 'func.fieldcheck.inc';

	include_once 'class.cls_schedule.inc';

	session_start();
	$data = $_REQUEST;
	
	//ログインチェック
	$blogin = isLogin();
	if (!($blogin)){
		header("Location: ".URL_LOGIN);
		exit;
	}
	$incFile = "form.inc";

	$err = 0;
	$enc = 0;
	$bDouble = false;
	
	$clsSch 	= new cls_schedule();

	if ((!isset($data['ymd']))||(!isValidNum($data['ymd']))||(strlen($data['ymd'])<>8)){
		$sErrorMsg = "パラメータエラー";
		$sErrorMsg2 = "日付の引数が不正です。カレンダーで編集したい日付をクリックしてください。";
		$incFile = 'error.inc';
	}else{
		$dispDate = substr($data['ymd'],0,4)."年 ".substr($data['ymd'],4,2)."月 ".substr($data['ymd'],6,2)."日";
		$paramDate = substr($data['ymd'],0,4)."/".substr($data['ymd'],4,2)."/".substr($data['ymd'],6,2);
		$ymd = $data['ymd'];
		
		$f_add = false;
		$f_del = false;
		$f_delday = false;
		$f_order = false;
		//「新規登録」ボタンが押された場合
		switch ($data['m']) {
		case 'commit':
			$sDBmode 	= $data['mode'];
			//★二重投稿チェック
			if (doubleSubmit($data['dstoken'])){
				$clsSch->setData($data,1);
				$clsSch->setWhere($data['SPD_DAYNIGHT']);
				$arrErrors = $clsSch->isValidData();
				if (count($arrErrors) == 0){
					if ($sDBmode == "upd"){;
						$ContId = $clsSch->doUpdate();
						$f_upd = true;
					}else{
						$ContId = $clsSch->doInsert();
						$f_add = true;
					}
					$sDBmode = "";
					$data['SPD_UID'] = "";
					$data['SPD_PLACE_NAME'] = "";
					$data['SPD_REQUIRE_NUM'] = "";
				}else{
				}
			}else{
				//★二重投稿
				$bDouble = true;
			}
			break;
		//「参照」リンクが押された場合
		case 'show':
			//$sDBmode=updで登録画面を表示
			$sDBmode 	= "upd";
			//★二重投稿チェック
			if (doubleSubmit($data['dstoken'])){
				$clsSch->setData($data,1);
				$clsSch->setWhere($data['SPD_DAYNIGHT']);
				$data 	= $clsSch->getInfo();
				$data['ymd']= $ymd;
				$b_show = true;
			}else{
				//★二重投稿
				$bDouble = true;
			}
			break;
		//一覧から「削除」リンクが押された場合
		case 'del':
			//★二重投稿チェック
			if (doubleSubmit($data['dstoken'])){
				$clsSch->setData($data,1);
				//削除処理
				$ContId = $clsSch->doDelete($data['SPD_UID']);
		
				$f_del = true;

				$data['SPD_UID'] = "";
			}else{
				//★二重投稿
				$bDouble = true;
			}
			break;
		//一覧から「↑」「↓」リンクが押された場合
		case 'move':
			//★二重投稿チェック
			if (doubleSubmit($data['dstoken'])){
				$clsSch->setData($data,1);
				//削除処理
				if (!($clsSch->modPriority())){
					$err = 1;
				};
				//メッセージ設定
				if ($err==1){
					$arrErrors['ERROR'] = "system error: 順序の変更に失敗しました。";
				}else{
					$f_order = true;
				}
				$data['uid'] = "";

			}else{
				//★二重投稿
				$bDouble = true;
			}
			break;
		//「予定削除」リンクが押された場合
		case 'delday':
			//★二重投稿チェック
			if (doubleSubmit($data['dstoken'])){
				$clsSch->setData($data,1);
				//削除処理
				$ContId = $clsSch->doDeleteDate($data['SPD_DATE_SCHEDULE']);
		
				$f_deldate = true;
			}else{
				//★二重投稿
				$bDouble = true;
			}
			break;
		//「予定削除」リンクが押された場合
		case 'copy':
			//★二重投稿チェック
			if (doubleSubmit($data['dstoken'])){
				if (isValidDate($data['COPY_DATE_SCHEDULE'].' 00:00:00')){
					$clsSch->setData($data,1);
					//複写処理
					$ContId = $clsSch->doCopy($data['SPD_DATE_SCHEDULE'],$data['COPY_DATE_SCHEDULE']);
					$f_copy = true;
				}else{
					$arrErrors['ERROR'] = "複写対象の日付が不正です。";
				}
			}else{
				//★二重投稿
				$bDouble = true;
			}
			break;
		default:
		
			break;
		}
		if ($clsSch->isExistSchedule($data['ymd'])===false){
			$newDay = '1';
		}else{
			$newDay = '0';
		}
		//一覧を表示
		$incFile	= 'form.inc';
		$clsSch->setData($data,0);
		$clsSch->setWhere(0);
		$rows 	= $clsSch->getList();
		$clsSch->setWhere(1);
		$rows2 	= $clsSch->getList();

		//★二重投稿エラー
		if ($bDouble===true){
			$sErrorMsg = "二重送信エラー";
			$sErrorMsg2 = "ブラウザの「戻る」や「進む」機能で二重に同じ処理をすることはできません。";
			$incFile	= 'error.inc';
		}
	}
	
	$nowy = substr($data['ymd'],0,4);
	$nowm = substr($data['ymd'],4,2);
	$nowd = substr($data['ymd'],6,2);
	if ($nowd < 16){
		$sel_nengetsu = $nowy.$nowm;
	}else{
		$sel_nengetsu = date("Y", strtotime($nowy."-".$nowm."-01 +1 month")).date("m", strtotime($nowy."-".$nowm."-01 +1 month"));
	}
	//次の日前の日
	$beforeday = date("Ymd", strtotime($nowy."-".$nowm."-".$nowd." -1 day"));
	$nextday = date("Ymd", strtotime($nowy."-".$nowm."-".$nowd." +1 day"));
	
	include_once "header.inc";
	//★
	if (($incFile == 'nodata.inc')||($incFile == 'error.inc')){
		include_once $incFile;
		include_once "footer.inc";
	}else{
		$dstoken = doubleSubmit();	//新しいトークン発行
		include_once $incFile;
		include_once "footer.inc";
	}
	$clsSch->close();


?>
