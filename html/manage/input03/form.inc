<script type="text/javascript">
<?php
	if (count($arrErrors)>0){
?>
    $(function() { 
        $.notifyBar({ html: "入力エラーがあります" });
    });
<?php
	}else if ($f_del){
?>
    $(function() { 
        $.notifyBar({ html: "現場を削除しました。" });
    });
<?php
	}else if ($f_delday){
?>
    $(function() { 
        $.notifyBar({ html: "現場と予定を削除しました。" });
    });
<?php
	}else if ($f_add){
?>
    $(function() { 
        $.notifyBar({ html: "現場を追加しました。" });
    });
<?php
	}else if ($f_upd){
?>
    $(function() { 
        $.notifyBar({ html: "現場を変更しました。" });
    });
<?php
	}else if ($f_order){
?>
    $(function() { 
        $.notifyBar({ html: "順番を変更しました。" });
    });
<?php
	}
?>
</script>
<script type="text/javascript" src="<?=URL_COMMON?>js/manage.js"></script>
<script language="javascript">
<!--
function doCommit() {
	var f = document.fList;
	f.m.value = 'commit';
	f.submit();
	return false;
}
function doShow(uid) {
	var f = document.fList;
	f.m.value = 'show';
	f.SPD_UID.value = uid;
	f.submit();
	return false;
}
function doDelete(uid) {
	if (confirm('現場を削除してよろしいですか？\n現場に紐づく登録済みの作業予定データも削除されます。')){
		var f = document.fList;
		f.m.value = 'del';
		f.SPD_UID.value = uid;
		f.submit();
	}
	return false;
}
function doMove(uid,pri,move) {
	var f = document.fList;
	f.m.value = 'move';
	f.uid.value = uid;
	f.pri.value = pri;
	f.move.value = move;
	f.submit();
	return false;
}
function doCommit2() {
	var f = document.fList2;
	f.m.value = 'commit';
	f.submit();
	return false;
}
function doShow2(uid) {
	var f = document.fList2;
	f.m.value = 'show';
	f.SPD_UID.value = uid;
	f.submit();
	return false;
}
function doDelete2(uid) {
	if (confirm('現場を削除してよろしいですか？\n現場に紐づく登録済みの作業予定データも削除されます。')){
		var f = document.fList2;
		f.m.value = 'del';
		f.SPD_UID.value = uid;
		f.submit();
	}
	return false;
}
function doMove2(uid,pri,move) {
	var f = document.fList2;
	f.m.value = 'move';
	f.uid.value = uid;
	f.pri.value = pri;
	f.move.value = move;
	f.submit();
	return false;
}
function doCopy() {
	var f = document.fCopy;
	f.m.value = 'copy';
	f.submit();
	return false;
}
function doDelDay() {
	if (confirm('この日の現場データおよび予定データを全て削除してよろしいですか？')){
		var f = document.fControl;
		f.m.value = 'delday';
		f.submit();
	}
	return false;
}
function goYotei() {
	var f = document.fControl;
	f.action = 'detail.php';
	f.submit();
	return false;
}

    //-->
    </script>

  </head>

  <body data-spy="scroll" data-target=".subnav" data-offset="50" >


  <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?=URL_MANAGE?>"><?=SITE_NAME?></a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="">
                <a href="<?=URL_MANAGE?>"><i class="icon-home icon-white"></i>&nbsp;HOME</a>
              </li>
              <li class="active">
                <a href="#">作業予定表入力（現場入力）</a>
              </li>
            </ul>
            <p class="navbar-text pull-right"><i class="icon-user icon-white"></i>&nbsp;<?= $_ADMIN[$_SESSION['USER']][1] ?>さんでログイン中&nbsp;<a href="<?=URL_MANAGE?>logout.php" class="btn btn-danger">ログアウト&nbsp;<i class="icon-share-alt icon-white"></i></a></p>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span2">
          <?php
	  include 'menu.inc';
	  ?>
        </div><!--/span-->
        <div class="span9">
          <h2>作業予定表入力（現場入力）</h2>
          <p class="lead"></p>
      <?php if (count($arrErrors) > 0){ ?>
          <div class="alert alert-error ">
            <a class="close" data-dismiss="alert">×</a>
	    <ul>
            <li>
            <?=implode('</li><li>',$arrErrors)?>
	    </li>
	    </ul>
          </div>
      <?php } ?>
          <div class="row-fluid">
<?php
if ($newDay=="1"){
?>
            <div class="control-group well BKyellow span5"  style="float:left;margin-left:0px;margin-right:0px;padding-bottom:0px;">
            <form class="form-inline" name="fCopy" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>"  >
              <input type="hidden" name="SPD_DATE_SCHEDULE" value="<?=$paramDate?>" />
              <input type="hidden" name="ymd" value="<?=$data['ymd']?>" />
              <input type="hidden" name="m" value="" />
              <span onclick="setFocus('inputcopy');">過去の作業予定を複写</span>
              <div class="input-append">
                <input type="text" name="COPY_DATE_SCHEDULE" value="<?=mbConv(htmlspecialchars(br4nl(($data['COPY_DATE_SCHEDULE'])),ENT_QUOTES),$enc)?>" class="span1h" placeholder="複写対象日付" style="ime-mode: disabled;" id="inputcopy" onChange="dateFormat(this);" /><a href="javascript:void(0);return false;" onclick="doCopy();" class="btn" >複写</a>
              </div>
            </form>	
            </div>
<?php
}
?>
            <div class="control-group well span5 " style="float:right;margin-right:0px;padding-bottom:0px;">
            <form name="fControl" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>"  >
            <input type="hidden" name="SPD_DATE_SCHEDULE" value="<?=$paramDate?>" />
            <input type="hidden" name="ymd" value="<?=$data['ymd']?>" />
            <input type="hidden" name="m" value="" />
            <input type="hidden" name="dstoken" value="<?=$dstoken?>" />
              <a class="btn" href="javascript:void(0);" onclick="goYotei();return false;">&nbsp;予定入力へ&nbsp;</a>
              <a class="btn" href="index.php?sel_nengetsu=<?=$sel_nengetsu?>">&nbsp;一覧へ戻る&nbsp;</a>
              &nbsp;&nbsp;&nbsp;<a class="btn btn-danger" href="javascript:void(0);" onclick="doDelDay();return false;">&nbsp;<i class="icon-trash icon-white"></i>予定削除</a>
            </form>	
            </div>
          </div>
          <h2><?=$dispDate?>&nbsp;&nbsp;<a class="btn" style="font-weight:normal;" href="edit.php?ymd=<?=$beforeday?>">&nbsp;<<前の日&nbsp;</a>&nbsp;&nbsp;<a class="btn" style="font-weight:normal;" href="edit.php?ymd=<?=$nextday?>">&nbsp;次の日>>&nbsp;</a></h2>
          <h3>昼間</h3>
          <form class="well form-inline" name="fList" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>"  >
            <input type="hidden" name="m" value="" />
            <input type="hidden" name="dstoken" value="<?=$dstoken?>" />
            <input type="hidden" name="SPD_UID" value="<?=(($data['SPD_DAYNIGHT']=='0')?$data['SPD_UID']:'')?>" />
            <input type="hidden" name="SPD_DAYNIGHT" value="0" />
            <input type="hidden" name="SPD_DATE_SCHEDULE" value="<?=$paramDate?>" />
            <input type="hidden" name="ymd" value="<?=$data['ymd']?>" />
            <input type="hidden" name="uid" value="" />
            <input type="hidden" name="pri" value="" />
            <input type="hidden" name="move" value="" />
            <input type="hidden" name="mode" value="<?=$sDBmode?>" />
            <label for="input01">現場名<span class="pink">※</span></label>
            <input id="input01" type="text" name="SPD_PLACE_NAME" value="<?=(($data['SPD_DAYNIGHT']=='0')?mbConv(htmlspecialchars(br4nl(($data['SPD_PLACE_NAME'])),ENT_QUOTES),$enc):'')?>" class="span3" placeholder="現場名を入力" style="ime-mode: active;" />&nbsp;&nbsp;&nbsp;&nbsp;
	    <label for="input02">人数</label>
            <input id="input02" type="text" name="SPD_REQUIRE_NUM" value="<?=(($data['SPD_DAYNIGHT']=='0')?mbConv(htmlspecialchars(br4nl(($data['SPD_REQUIRE_NUM'])),ENT_QUOTES),$enc):'')?>" class="span1" placeholder="人数を入力" style="ime-mode: disabled;" />&nbsp;&nbsp;&nbsp;&nbsp;
	    <a href="javascript:void(0);" onclick="doCommit();return false;" class="btn btn-primary">&nbsp;&nbsp;登録&nbsp;&nbsp;</a>
          </form>	
          <!--pageSet -->
          <table class="table table-bordered table-striped">
            <tr>
              <th class="span1h">表示順序</th>
              <th class="span3">現場名</th>
              <th class="span1">人数</th>
              <th class="span1h">編集</th>
              <th class="span1h">削除</th>
            </tr>
<?php
$cnt = 0;
if ((is_array($rows))&&(count($rows)>0)){
foreach($rows as $row){
	$cnt++;
?>
            <tr>
              <td>
                  <a href="javascript:void(0);" onclick="doMove('<?=$row['SPD_UID']?>','<?=$row['SPD_PRIORITY']?>','up');return false;"><i class="icon-arrow-up"></i>上</a>&nbsp;/
                  <a href="javascript:void(0);" onclick="doMove('<?=$row['SPD_UID']?>','<?=$row['SPD_PRIORITY']?>','down');return false;">下<i class="icon-arrow-down"></i></a>
                </td>
              <td><?=$row['SPD_PLACE_NAME']?></td>
              <td><?=$row['SPD_REQUIRE_NUM']?></td>
              <td><a href="javascript:void(0);" onclick="doShow('<?=$row['SPD_UID']?>');return false;" class="btn btn-success"><i class="icon-pencil icon-white"></i>編集</a></td>
              <td><a href="javascript:void(0);" onclick="doDelete('<?=$row['SPD_UID']?>');return false;" class="btn btn-danger"><i class="icon-trash icon-white"></i>削除</a></td>
            </tr>
<?php
}
}
?>
          </table>
          <h3>夜間</h3>
          <form class="well form-inline" name="fList2" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>"  >
            <input type="hidden" name="m" value="" />
            <input type="hidden" name="dstoken" value="<?=$dstoken?>" />
            <input type="hidden" name="SPD_UID" value="<?=(($data['SPD_DAYNIGHT']=='1')?$data['SPD_UID']:'')?>" />
            <input type="hidden" name="SPD_DAYNIGHT" value="1" />
            <input type="hidden" name="SPD_DATE_SCHEDULE" value="<?=$paramDate?>" />
            <input type="hidden" name="ymd" value="<?=$data['ymd']?>" />
            <input type="hidden" name="uid" value="" />
            <input type="hidden" name="pri" value="" />
            <input type="hidden" name="move" value="" />
            <input type="hidden" name="mode" value="<?=$sDBmode?>" />
            <label for="input01">現場名<span class="pink">※</span></label>
            <input id="input01" type="text" name="SPD_PLACE_NAME" value="<?=(($data['SPD_DAYNIGHT']=='1')?mbConv(htmlspecialchars(br4nl(($data['SPD_PLACE_NAME'])),ENT_QUOTES),$enc):'')?>" class="span3" placeholder="現場名を入力" style="ime-mode: active;" />&nbsp;&nbsp;&nbsp;&nbsp;
	    <label for="input02">人数</label>
            <input id="input02" type="text" name="SPD_REQUIRE_NUM" value="<?=(($data['SPD_DAYNIGHT']=='1')?mbConv(htmlspecialchars(br4nl(($data['SPD_REQUIRE_NUM'])),ENT_QUOTES),$enc):'')?>" class="span1" placeholder="人数を入力" style="ime-mode: disabled;" />&nbsp;&nbsp;&nbsp;&nbsp;
	    <a href="javascript:void(0);" onclick="doCommit2();return false;" class="btn btn-primary">&nbsp;&nbsp;登録&nbsp;&nbsp;</a>
          </form>	
          <!--pageSet -->
          <table class="table table-bordered table-striped">
            <tr>
              <th class="span1h">表示順序</th>
              <th class="span3">現場名</th>
              <th class="span1">人数</th>
              <th class="span1h">編集</th>
              <th class="span1h">削除</th>
            </tr>
<?php
$cnt = 0;
if ((is_array($rows2))&&(count($rows2)>0)){
foreach($rows2 as $row){
	$cnt++;
?>
            <tr>
              <td>
                  <a href="javascript:void(0);" onclick="doMove2('<?=$row['SPD_UID']?>','<?=$row['SPD_PRIORITY']?>','up');return false;"><i class="icon-arrow-up"></i>上</a>&nbsp;/
                  <a href="javascript:void(0);" onclick="doMove2('<?=$row['SPD_UID']?>','<?=$row['SPD_PRIORITY']?>','down');return false;">下<i class="icon-arrow-down"></i></a>
                </td>
              <td><?=$row['SPD_PLACE_NAME']?></td>
              <td><?=$row['SPD_REQUIRE_NUM']?></td>
              <td><a href="javascript:void(0);" onclick="doShow2('<?=$row['SPD_UID']?>');return false;" class="btn btn-success"><i class="icon-pencil icon-white"></i>編集</a></td>
              <td><a href="javascript:void(0);" onclick="doDelete2('<?=$row['SPD_UID']?>');return false;" class="btn btn-danger"><i class="icon-trash icon-white"></i>削除</a></td>
            </tr>
<?php
}
}
?>
          </table>
      <div class="row-fluid">
            <div class="control-group well span5 ">
              <a class="btn" href="javascript:void(0);" onclick="goYotei();return false;">&nbsp;予定入力へ&nbsp;</a>
              <a class="btn" href="index.php?sel_nengetsu=<?=$sel_nengetsu?>">&nbsp;一覧へ戻る&nbsp;</a>
              &nbsp;&nbsp;&nbsp;<a class="btn btn-danger" href="javascript:void(0);" onclick="doDelDay();return false;">&nbsp;<i class="icon-trash icon-white"></i>予定削除</a>
            </div>
      </div>
        </div>
      </div>