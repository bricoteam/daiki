<?php
//============================================================+
// File name   : rep02.php
// Begin       : 2012-05-05
// Last Update : 2012-05-05
//
// Description : 作業完了通知書出力
//
// Author: Reiko Suzuki @ Bricoleur inc.
//
// (c) Copyright:
//               Daiki Electric Co. Ltd.
//               Bricoleur inc.
//============================================================+
if ($_SERVER[SERVER_NAME] == 'daiki.bricoleur.in'){
	require_once('ini.inc');
}
include_once 'func.common.inc';
include_once 'func.field.inc';
include_once 'func.fieldcheck.inc';

include_once 'class.cls_estimate.inc';

session_start();
$req = $_REQUEST;

//ログインチェック
$blogin = isLogin();
if (!($blogin)){
	header("Location: ".URL_LOGIN);
	exit;
}
require_once('config/lang/jpn.php');
require_once('tcpdf.php');
require_once('fpdi.php');

if (strlen($req['id'])==0){
	print 'パラメーターエラー：見積書番号が存在しません。';
	exit;
}

// extend TCPF with custom functions
class MYPDF extends FPDI {
	var $cellH = 9;
	
	// Load table data from file
	public function LoadDataHeader() {
		// Read file lines
		//$lines = file($file);
		//$data = array();
		//$data = explode(';', chop($lines[0]));
		$clsEst 	= new cls_estimate();
		$data = $clsEst->getHeaderData($_REQUEST['id']);
		$clsEst->close();

		return $data;
	}

	// Colored table
	public function OutCompRep($dataH) {
	
		//ヘッダー部分出力
		//出力日付
		$this->SetXY(100, 23.5);
		$this->Write(0, date('Y年　m月　d日',strtotime($dataH['HDR_DATE_COMPREP'])));

		
		//得意先名
		$this->SetFont('msgothic', '', 13);
		$this->SetXY(15, 30);
		$this->Write(0, $dataH['HDR_CUSTOMER_NAME']);

		//部署
		$this->SetFont('msgothic', '', 11);
		$this->SetXY(20, 35.5);
		$this->Write(0, $dataH['HDR_CUSTOMER_DEPT'].'　　御中');
		
		/**** 印ありの場合 ****/
		if ($_REQUEST['stmp']=='1'){
			//社印スタンプ169,78.5
			$this->ImageEps('kakuin.eps', 165, 40, 21, 21);
		}
		
		//注文番号
		$this->SetFont('msgothic', '', 11);
		$this->SetXY(76, 104);
		$this->Write(0, $dataH['HDR_ORDER_NO']);
		$this->SetXY(126, 104);
		$this->Write(0, $dataH['HDR_ORDER_NO_B']);
		$this->SetXY(146, 104);
		$this->Write(0, $dataH['HDR_CUSTOMER_CODE']);
		
		//件名
		$nFontSize1 = getJustFontSizePDF($this,$dataH['HDR_TITLE_1'], 130, 11);
		$nFontSize2 = getJustFontSizePDF($this,$dataH['HDR_TITLE_2'], 130, 11);
		$nFSize = (($nFontSize1 > $nFontSize2)?$nFontSize2:$nFontSize1);
		$this->SetFont('msgothic', '', $nFSize);
		$this->SetXY(61, 118);
		$this->Write(0, $dataH['HDR_TITLE_1']);
		$this->SetXY(61, 123);
		$this->Write(0, $dataH['HDR_TITLE_2']);

		//作業内容
		$this->SetFont('msgothic', '', 11);
		$this->SetXY(61, 135);
	//	$this->Write(0, $dataH[8]);
		$this->MultiCell(120, 20, str_replace("\\n","\n",$dataH['HDR_WORK_CONTENTS']),0, 'L');

		//作業期間From
		$this->SetXY(67, 152);
		$this->Write(0, date('Y年　m月　d日',strtotime($dataH['HDR_DATE_TERM_F'])));
		//作業期間To
		$this->SetXY(136, 152);
		$this->Write(0, date('Y年　m月　d日',strtotime($dataH['HDR_DATE_TERM_T'])));

		//客先担当
		$this->SetXY(80, 166.5);
		$this->Write(0, $dataH['HDR_DEPT'].'　'.$dataH['HDR_CHARGER']);

		//金額合計
		$this->SetFont('msgothic', '', 14);
		$this->SetXY(40, 210);
		$this->Cell(50, 8, number_format($dataH['HDR_ORDER_TOTAL']).' 円', '', 0, 'R', 0);
		
		//○図形
		$style = array('width' => 0.25, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
		$this->SetLineStyle($style);
		
		if ($dataH['HDR_REPORT'] == '0'){
			$this->Circle(113.5,183.5,3);
		}else{
			$this->Circle(125,183.5,3);
			if ($dataH['HDR_REPORT_CLIP'] == '1'){
				$this->Circle(141.5,183.5,3);
			}else if ($dataH['HDR_REPORT_CLIP'] == '2'){
				$this->Circle(156.5,183.5,3);
			}
		}
		if ($dataH['HDR_SCORE'] == '0'){
			$this->Circle(113.5,189.5,3);
		}else{
			$this->Circle(125,189.5,3);
			if ($dataH['HDR_SCORE_CLIP'] == '1'){
				$this->Circle(141.5,189.5,3);
			}else if ($dataH['HDR_SCORE_CLIP'] == '2'){
				$this->Circle(156.5,189.5,3);
			}
		}
	}
	
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('Daiki Electric Co. Ltd.');
$pdf->SetAuthor('Daiki Electric Co. Ltd.');
//$pdf->SetMargins(10, 26.5, 5);
$pdf->SetMargins(10, 27, 5);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);

//Header消去
$pdf->setPrintHeader(false);
//Footer消去
$pdf->setPrintFooter(false);
//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 12);

// ---------------------------------------------------------

// set font
$pdf->SetFont('msgothic', '', 10);

// add a page
$pdf->AddPage();

// set template
$pdf->setSourceFile('rep020.pdf');
// import page 1
$tplIdx = $pdf->importPage(1);
// use the imported page 
$pdf->useTemplate($tplIdx);


//Data loading
$dataH = $pdf->LoadDataHeader();

// print colored table
$pdf->OutCompRep($dataH);

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('rep02.pdf', 'I');

//フォントサイズ計算
function getJustFontSizePDF($p_Cls ,$p_sValue, $p_nWidth, $p_nMaxSize) {
	$nWidth = $p_nWidth - 1;
	$nMax 	= $p_nMaxSize * 10;
	$nSize 	= $p_nMaxSize;
	for ($i = 0; $i <= $nMax; $i++) {
		$nSize = $p_nMaxSize - ($i * 0.1);
		$p_Cls->SetFont('msgothic', '', $nSize);
		$nLen = $p_Cls->GetStringWidth($p_sValue);
		if ($nLen <= $nWidth) {
			break;
		}
	}
	return $nSize;
}

//改行文字列処理
function getStringDivid($p_string, $p_length){
	//p_lengthの長さに分割した文字を返す
	//p_length：文字列長（1bite文字の文字数分の長さ）
	
	//文字列の文字数
	$mojiNum = mb_strlen($p_string);
	$a = 0;
	$mojiBite =0;
	$rtn = 0;
	$return = array();
	//文字数分ループ
	while ($a < $mojiNum){
		//$a文字目は全角か半角か？半角なら1全角なら2加算
		if (strlen(mb_substr($p_string,$a,1,'UTF-8')) > 1){
			$mojiBite = $mojiBite + 2;
		}else{
			$mojiBite = $mojiBite + 1;
		}
		//指定の長さを超えたらそのときの文字数を保存してループ終了
		if ($mojiBite > $p_length){
			$rtn = $a;
			break;
		}
		$a++;
	}
	$return[] = mb_substr($p_string,0,$rtn,'UTF-8');
	$return[] = mb_substr($p_string,$rtn,$p_length,'UTF-8');
	return $return;
}

//============================================================+
// END OF FILE                                                
//============================================================+
