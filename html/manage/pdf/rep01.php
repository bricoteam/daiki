<?php
//============================================================+
// File name   : rep01.php
// Begin       : 2012-05-05
// Last Update : 2012-05-05
//
// Description : 見積書出力
//
// Author: Reiko Suzuki @ Bricoleur inc.
//
// (c) Copyright:
//               Daiki Electric Co. Ltd.
//               Bricoleur inc.
//============================================================+
if ($_SERVER[SERVER_NAME] == 'daiki.bricoleur.in'){
	require_once('ini.inc');
}
include_once 'func.common.inc';
include_once 'func.field.inc';
include_once 'func.fieldcheck.inc';

include_once 'class.cls_estimate.inc';

session_start();
$req = $_REQUEST;

//ログインチェック
$blogin = isLogin();
if (!($blogin)){
	header("Location: ".URL_LOGIN);
	exit;
}
require_once('config/lang/jpn.php');
require_once('tcpdf.php');
require_once('fpdi.php');

if (strlen($req['id'])==0){
	print 'パラメーターエラー：見積書番号が存在しません。';
	exit;
}

// extend TCPF with custom functions
class MYPDF extends FPDI {
	//Column titles
	var $header = array('内　訳　項　目', '形 状 ・ 寸 法', '数 量', '単 位','単　価','金　額','備　考');
	var $arrW = array(60, 45, 12, 14, 20, 22, 20);
	
	var $cellH = 9;
	var $cellL1 = 18;
	var $cellL2 = 28;
	var $choH = 4.5;
	var $cpY = 17.5;

	//Page header
	function Header() {
		// Header

		// set the sourcefile
		if ($this->getPage() > 1){
			$this->setSourceFile('rep012.pdf');
			$this->SetXY(-22, 10);
			$this->SetFont('msgothic', '', 10);
			$this->Cell(10, 8, $this->getPage(), '', 0, 'R', 0);
			$this->SetXY(10, $this->cpY);
			$this->SetFillColor(255, 255, 255);
			$this->SetTextColor(0,0,0);
			$this->SetLineWidth(0.2);
			$this->SetFont('msgothic', '', 11);
			$w =  $this->arrW;
			$num_headers = count($this->header);
			for($i = 0; $i < $num_headers; ++$i) {
				$this->Cell($w[$i], ($this->cellH)+0.2, $this->header[$i], 1, 0, 'C', 1);
			}
		}else{
			$this->setSourceFile('rep011.pdf');
		}
		// import page 1
		$tplIdx = $this->importPage(1);
		// use the imported page 
		$this->useTemplate($tplIdx);
	}
	// Load table data from file
	public function LoadData() {
		// Read file lines
		//$lines = file($file);
		//$data = array();
		//$cnt = 0;
		//foreach($lines as $line) {
		//	$cnt++;
		//	if ($cnt > 1){
		//		$data[] = explode(';', chop($line));
		//	}
		//}
		$clsEst 	= new cls_estimate();
		$data = $clsEst->getDetailData($_REQUEST['id']);
		$clsEst->close();
		return $data;
	}
	// Load table data from file
	public function LoadDataHeader() {
		// Read file lines
		//$lines = file($file);
		//$data = array();
		//$data = explode(';', chop($lines[0]));
		$clsEst 	= new cls_estimate();
		$data = $clsEst->getHeaderData($_REQUEST['id']);
		$clsEst->close();

		return $data;
	}

	// Colored table
	public function ColoredTable($data,$dataH) {
	
		//一行だけ次ページへいってしまうケースを防止
		$allcnt = count($data);
		//if (($allcnt == ($this->cellL1 + 1))||(($allcnt - $this->cellL1 - 1)%$this->cellL2 == 0)){
		if (($allcnt - ($this->cellL1 + 1))%$this->cellL2 == 0){
			$this->cellH = 8;
			$this->cellL1 = 21;
			$this->cellL2 = 32;
			$this->choH = 4;
			$this->cpY = 18.6;
		}
		//ヘッダー部分出力
		//見積書番号
		$this->SetTextColor(0,0,0);
		$this->SetXY(-38, 23);
		$this->SetFont('msgothic', '', 11);
		$this->Write(0, $dataH['HDR_NO']);
		//出力日付
		$this->SetXY(-47, 35);
		$this->Write(0, date('Y年　m月　d日',strtotime($dataH['HDR_DATE_ESTIMATE'])));
		//得意先名
		$this->SetFont('msgothic', '', 14);
		$this->SetXY(8, 47);
		$this->Write(0, $dataH['HDR_CUSTOMER_NAME'].'　殿');
		
		/**** 印ありの場合 ****/
		if ($_REQUEST['stmp']=='1'){
			//社印スタンプ169,78.5
			$this->ImageEps('kakuin.eps', 179, 48, 21, 21);
			//担当者スタンプ169,78.5
			$this->Image('t_stamp.png', 169, 78.5, 15, 15, 'PNG', '', '', true, 300, '', false, false, 0, false);
			$this->SetTextColor(255,0,0);
			//日付
			$this->SetFont('msgothic', '', 8);
			$this->SetXY(169, 84.5);
			$this->Write(0, date("'y.m.d",strtotime($dataH['HDR_DATE_ESTIMATE'])));
			//名前
			$this->SetFont('msgothic', '', 10);
			$this->SetXY(172, 88.5);
			$this->Write(0, $GLOBALS['_ADMIN'][$_SESSION['USER']][1]);
			$this->SetTextColor(0,0,0);
		}
		
		//金額合計
		$this->SetFont('msgothic', '', 11);
		$this->SetXY(50, 58);
		$this->Cell(30, 8, number_format($dataH['HDR_TOTAL']).' 円', '', 0, 'R', 0);
		//件名
		$nFontSize1 = getJustFontSizePDF($this,$dataH['HDR_TITLE_1'], 100, 11);
		$nFontSize2 = getJustFontSizePDF($this,$dataH['HDR_TITLE_2'], 80, 11);
		$nFSize = (($nFontSize1 > $nFontSize2)?$nFontSize2:$nFontSize1);
		$this->SetFont('msgothic', '', $nFSize);
		$this->SetXY(30, 70);
		$this->Write(0, $dataH['HDR_TITLE_1']);
		$this->SetXY(30, 74.5);
		$this->Write(0, $dataH['HDR_TITLE_2']);

		//客先担当
		$nFontSize = getJustFontSizePDF($this,$dataH['HDR_DEPT'].'　'.$dataH['HDR_CHARGER'], 40, 9);
		$this->SetXY(110, 75);
		$this->Write(0, '('.$dataH['HDR_DEPT'].'　'.$dataH['HDR_CHARGER'].' 殿)');


		//納期及び受け渡し場所
		$nFontSize = getJustFontSizePDF($this,$dataH['HDR_DELIVERY_TIME_PLACE'], 70, 10);
		$this->SetXY(42, 88.5);
		$this->Write(0, $dataH['HDR_DELIVERY_TIME_PLACE']);
		//取引条件
		/* add 2017.11.07 */
		$str_condition = $dataH['HDR_CONDITION'];
		if (substr($dataH['HDR_NO'],0,1) == "B"){
			$str_condition = ltrim($str_condition . " 消費税別途");
		}
		//upd 1 line
		//$nFontSize = getJustFontSizePDF($this,$dataH['HDR_CONDITION'], 70, 10);
		$nFontSize = getJustFontSizePDF($this,$str_condition, 70, 10);
		$this->SetXY(42, 94);
		//upd 1 line
		//$this->Write(0, $dataH['HDR_CONDITION']);
		$this->Write(0, $str_condition);
		//有効期限
		$nFontSize = getJustFontSizePDF($this,$dataH['HDR_EXPIRATION_DATE'], 70, 10);
		$this->SetXY(42, 99.5);
		$this->Write(0, $dataH['HDR_EXPIRATION_DATE']);
		//注文番号
		$this->SetFont('msgothic', '', 10);
		$this->SetXY(-55, 94.5);
		$this->Write(0, $dataH['HDR_ORDER_NO'].$dataH['HDR_ORDER_NO_B']);
		//オーダーNo.
		$this->SetXY(-55, 99.5);
		//$this->Write(0, $dataH['HDR_ORDER_NO']);
		$this->Write(0, "");
		//ページNo.
		$this->SetXY(-22, 101.5);
		$this->Cell(10, 8, '1', '', 0, 'R', 0);
		
		// Colors, line width and bold font
		$this->SetFillColor(255, 255, 255);
		$this->SetTextColor(0,0,0);
		$this->SetFont('msgothic', '', 11);
		$this->SetLineWidth(0.2);
		// Header
		$this->SetXY(10, 109);
		$this->SetLineStyle(array('color' => array(0, 0, 0)));
		$w =  $this->arrW;
		$num_headers = count($this->header);
		for($i = 0; $i < $num_headers; ++$i) {
			$this->Cell($w[$i], $this->cellH, $this->header[$i], 1, 0, 'C', 1);
		}
		// Color and font restoration
		$this->SetFillColor(230, 230, 230);
		$this->SetTextColor(0);
		$this->SetFont('');
		$this->SetLineWidth(0.2);
		// Data
		$fill = 0;
		$line1 = 'L';
		$line2 = 'LR';
		$line3 = 'R';
		$line1l = 'LB';
		$line2l = 'LRB';
		$line3l = 'RB';

		
		$cnt = 0;
		$line = $line2;
		$line_t1 = $line1;
		$line_t2 = $line3;
		
		foreach($data as $row) {
			$cnt++;
			//最後の行になったら、最終ページの残りの行を挿入する。
			if ($allcnt == $cnt){
				if ($cnt <= $this->cellL1){
					$l = $this->cellL1-$cnt;
				}else if (($cnt-$this->cellL1)%$this->cellL2 == 0){ 
					$l = 0;
				}else {
					$l = $this->cellL2-(($cnt-$this->cellL1)%$this->cellL2);
				}
				$a = 0;
				while ($a < $l){
					$this->Ln();
					$a++;
					//穴埋め最後の行は下線ありで
					if ($a == $l){
						$uline = $line2l;
					}else{
						$uline = $line;
					}
					$this->Cell($w[0], $this->cellH, '', $uline, 0, 'L', $fill);
					$this->Cell($w[1], $this->cellH, '', $uline, 0, 'L', $fill);
					$this->Cell($w[2], $this->cellH, '', $uline, 0, 'R', $fill);
					$this->Cell($w[3], $this->cellH, '', $uline, 0, 'R', $fill);
					$this->Cell($w[4], $this->cellH, '', $uline, 0, 'R', $fill);
					$this->Cell($w[5], $this->cellH, '', $uline, 0, 'R', $fill);
					$this->Cell($w[6], $this->cellH, '', $uline, 0, 'L', $fill);
					$fill=!$fill;
				}
			}
			//最後の行か、1ページ目の最後の行か、2ページ以降の最後の行か、小計行のときライン出力
			if (($allcnt == $cnt)||($cnt == $this->cellL1)||(($cnt-$this->cellL1)%$this->cellL2 == 0)||($row[7] == '2')){
				$line = $line2l;
				$line_t1 = $line1l;
				$line_t2 = $line3l;
			}
			$this->Ln();
			//内訳項目
			if ($row[7]=='1'){
				$nFontSize = getJustFontSizePDF($this,$row[0], $w[0], 9);
				$this->Cell($w[0], $this->cellH, $row[0], $line, 0, 'L', $fill);
			}else{
				$this->Cell(3, $this->cellH, '', $line_t1, 0, 'L', $fill);
				$nFontSize = getJustFontSizePDF($this,$row[0], $w[0]-4, 9);
				if ($nFontSize < 8){
					$befX = $this->getX();
					$befY = $this->getY();
					$this->Cell($w[0]-3, $this->cellH, '', $line_t2, 0, 'L', $fill);
					$aftX = $this->getX();
					$aftY = $this->getY();
					//bef位置に戻って半分を8ポイントで出力
					$this->SetXY($befX, $befY+0.5);
					$this->SetFont('msgothic', '', 9);
					$ret = getStringDivid($row[0],34);
					$this->Write(0, $ret[0]);
					//bef位置＋ずらして残りを出力
					$this->SetXY($befX, $befY+$this->choH);
					$this->Write(0, $ret[1]);
					//位置をaftに戻す
					$this->SetXY($aftX, $aftY);
				}else{
					$this->Cell($w[0]-3, $this->cellH, $row[0], $line_t2, 0, 'L', $fill);
				}
			}
			//形状・寸法
			$nFontSize = getJustFontSizePDF($this,$row[1], $w[1], 9);
			if ($nFontSize < 8){
				$befX = $this->getX();
				$befY = $this->getY();
				$this->Cell($w[1], $this->cellH, '', $line, 0, 'L', $fill);
				$aftX = $this->getX();
				$aftY = $this->getY();
				//bef位置に戻って半分を8ポイントで出力
				$this->SetXY($befX, $befY+0.5);
				$this->SetFont('msgothic', '', 9);
				$ret = getStringDivid($row[1],26);
				$this->Write(0, $ret[0]);
				//bef位置＋ずらして残りを出力
				$this->SetXY($befX, $befY+$this->choH);
				$this->Write(0, $ret[1]);
				//位置をaftに戻す
				$this->SetXY($aftX, $aftY);
			}else{
				$this->Cell($w[1], $this->cellH, $row[1], $line, 0, 'L', $fill);
			}
			//数量
			$nFontSize = getJustFontSizePDF($this,$row[2], $w[2], 9);
			$this->Cell($w[2], $this->cellH, ((strlen($row[2])>0)?($row[2]):''), $line, 0, 'R', $fill);
			//単位
			$nFontSize = getJustFontSizePDF($this,$row[3], $w[3], 9);
			$this->Cell($w[3], $this->cellH, $row[3], $line, 0, 'R', $fill);
			//単価
			$nFontSize = getJustFontSizePDF($this,$row[4], $w[4], 9);
			$this->Cell($w[4], $this->cellH, ((strlen($row[4])>0)?number_format($row[4]):''), $line, 0, 'R', $fill);
			//金額
			$nFontSize = getJustFontSizePDF($this,$row[5], $w[5], 9);
			$this->Cell($w[5], $this->cellH, ((strlen($row[5])>0)?number_format($row[5]):''), $line, 0, 'R', $fill);
			//備考
			$nFontSize = getJustFontSizePDF($this,$row[6], $w[6], 9);
			if ($nFontSize < 8){
				$befX = $this->getX();
				$befY = $this->getY();
				$this->Cell($w[6], $this->cellH, '', $line, 0, 'L', $fill);
				$aftX = $this->getX();
				$aftY = $this->getY();
				//bef位置に戻って半分を8ポイントで出力
				$this->SetXY($befX, $befY+0.5);
				$this->SetFont('msgothic', '', 9);
				$ret = getStringDivid($row[6],10);
				$this->Write(0, $ret[0]);
			//	$this->Write(0, $aftX.','.$aftY);
				//bef位置＋ずらして残りを出力
				$this->SetXY($befX, $befY+$this->choH);
				$this->Write(0, $ret[1]);
			//	$this->Write(0, $befX.','.$befY);
				//位置をaftに戻す
				$this->SetXY(10, $befY+$this->choH);
			}else{
				$this->Cell($w[6], $this->cellH, $row[6], $line, 0, 'L', $fill);
			}
			$fill=!$fill;
			$line = $line2;
			$line_t1 = $line1;
			$line_t2 = $line3;
		}
	}
	
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('Daiki Electric Co. Ltd.');
$pdf->SetAuthor('Daiki Electric Co. Ltd.');
//$pdf->SetMargins(10, 26.5, 5);
$pdf->SetMargins(10, 27, 5);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);

//Footer消去
$pdf->setPrintFooter(false);
//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 12);

// ---------------------------------------------------------

// set font
$pdf->SetFont('msgothic', '', 10);

// add a page
$pdf->AddPage();

//Data loading
$data = $pdf->LoadData();
//Data loading
$dataH = $pdf->LoadDataHeader();

// print colored table
$pdf->ColoredTable($data,$dataH);

// ---------------------------------------------------------

//Close and output PDF document
//$pdf->Output('reports/example_011.pdf', 'F');
$pdf->Output('rep01.pdf', 'I');

//フォントサイズ計算
function getJustFontSizePDF($p_Cls ,$p_sValue, $p_nWidth, $p_nMaxSize) {
	$nWidth = $p_nWidth - 1;
	$nMax 	= $p_nMaxSize * 10;
	$nSize 	= $p_nMaxSize;
	for ($i = 0; $i <= $nMax; $i++) {
		$nSize = $p_nMaxSize - ($i * 0.1);
		$p_Cls->SetFont('msgothic', '', $nSize);
		$nLen = $p_Cls->GetStringWidth($p_sValue);
		if ($nLen <= $nWidth) {
			break;
		}
	}
	return $nSize;
}

//改行文字列処理
function getStringDivid($p_string, $p_length){
	//p_lengthの長さに分割した文字を返す
	//p_length：文字列長（1bite文字の文字数分の長さ）
	
	//文字列の文字数
	$mojiNum = mb_strlen($p_string);
	$a = 0;
	$mojiBite =0;
	$rtn = 0;
	$return = array();
	//文字数分ループ
	while ($a < $mojiNum){
		//$a文字目は全角か半角か？半角なら1全角なら2加算
		if (strlen(mb_substr($p_string,$a,1,'UTF-8')) > 1){
			$mojiBite = $mojiBite + 2;
		}else{
			$mojiBite = $mojiBite + 1;
		}
		//指定の長さを超えたらそのときの文字数を保存してループ終了
		if ($mojiBite > $p_length){
			$rtn = $a;
			break;
		}
		$a++;
	}
	$return[] = mb_substr($p_string,0,$rtn,'UTF-8');
	$return[] = mb_substr($p_string,$rtn,$p_length,'UTF-8');
	return $return;
}

//============================================================+
// END OF FILE                                                
//============================================================+
