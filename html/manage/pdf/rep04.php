<?php
//============================================================+
// File name   : rep04.php
// Begin       : 2013-11-07
// Last Update : 2013-11-07
//
// Description : 勤務表出力
//
// Author: Reiko Suzuki @ Bricoleur inc.
//
// (c) Copyright:
//               Daiki Electric Co. Ltd.
//               Bricoleur inc.
//============================================================+
if ($_SERVER[SERVER_NAME] == 'daiki.bricoleur.in'){
	require_once('ini.inc');
}
include_once 'func.common.inc';
include_once 'func.field.inc';
include_once 'func.fieldcheck.inc';

include_once 'class.cls_schedule.inc';

session_start();
$req = $_REQUEST;

//ログインチェック
$blogin = isLogin();
if (!($blogin)){
	header("Location: ".URL_LOGIN);
	exit;
}
require_once('config/lang/jpn.php');
require_once('tcpdf.php');
require_once('fpdi.php');

$exit = false;
if (strlen($req['ym'])==0){
	print 'パラメーターエラー：対象年月度が選択されていません。<br>';
	$exit = true;
}else if (!isValidDate(substr($req['ym'],0,4).'/'.substr($req['ym'],4,2).'/01 00:00:00')){
	print 'パラメーターエラー：年度の指定が不正です。<br>';
	$exit = true;
}

if ($exit){
	exit;
}

// extend TCPF with custom functions
class MYPDF extends FPDI {

	var $tabXleft = 20;		//テーブルの左端
	var $tabYTop = 57;		//テーブルの上端
	var $genbaXleft = 53;		//昼間現場の左端
	var $genbaXleft2 = 126;	//夜間現場の左端

	var $arrHeader;	//見出し
	var $arrPlace;	//現場見出し
	var $arrSchedule;	//予定データ
	
	var $dateFrom;
	var $dateTo;
	
	var $pageLimit;

	var $sNen;
	var $sGetsu;

	//Page header
	function Header() {
		// Header
		$this->setSourceFile('rep040.pdf');
		$this->SetXY(20, 28);
		$this->SetFont('msgothic', '', 16);
		
		//★年号を取得
		$arrNengo = array(	0 => array('平成','19890108','20201231'),
					1 => array('平成平','20210101','20501231')
				);
		$title = "";
		$nengo = "";
		foreach ($arrNengo as $row){
			if (($row[1] <= $this->sNen.$this->sGetsu.'15')&&($row[2] >= $this->sNen.$this->sGetsu.'01')){
				$nengo = sprintf('%02d', (intval($this->sNen) - intval(substr($row[1],0,4)) + 1));
				$title = $row[0].(($nengo=="01")?"元":$nengo)."年".$this->sGetsu."月";
			}
		}
		//2018.06.04 brico add
		$title = $this->sNen."年".$this->sGetsu."月";
		
		$this->Write(12, $title);

		// import page 1
		$tplIdx = $this->importPage(1);
		// use the imported page 
		$this->useTemplate($tplIdx);
	}
	function AddPage($orientation = '', $format = '') {
		parent::AddPage($orientation, $format);
	}
	// set nengetudo
	public function SetNendo($p_nen,$p_getsu) {
		//期間を保存
		$this->sNen = $p_nen;
		$this->sGetsu = $p_getsu;
		return true;
	}


	// Load table data from file
	public function LoadData($p_from,$p_to,$p_kbn=null) {
		//期間を保存
		$this->dateFrom = $p_from;
		$this->dateTo = $p_to;
		$clsSch 	= new cls_schedule();
		//期間を抽出
		$this->arrSchedule = $clsSch->getScheduleListforRep($p_from,$p_to,$p_kbn);
		$clsSch->close();
		return true;
	}
	// Load table data from file
	public function LoadDataPlace($p_from,$p_to,$p_kbn=null) {
		$clsSch 	= new cls_schedule();
		//期間を抽出
		$this->arrPlace = $clsSch->getPlaceListforRep($p_from,$p_to,$p_kbn);
		$clsSch->close();
		return true;
	}
	// Load table data from file
	public function LoadDataHeader() {
		//社員読み込み
		$clsSch 	= new cls_schedule();
		//出力対象
		$arrShain1 = $clsSch->getShain();
	//	$arrShain2 = $clsSch->getShain(1);
	//	$this->arrHeader = array_merge($arrShain1 , $arrShain2);
		$this->arrHeader = $arrShain1;

		$clsSch->close();

		return true;
	}
	
	// Colored table
	public function OutputTable() {
		
		$clsSch 	= new cls_schedule();
		$arrYoubi = array(0=>"日",1=>"月",2=>"火",3=>"水",4=>"木",5=>"金",6=>"土");
		
		//日付ループ用配列の作成
		
		$this->dateTo = $this->sNen.'/'.$this->sGetsu.'/15';
		$plus = date('Y/m/d',strtotime($this->sNen.'/'.$this->sGetsu.'/01'.' -1 month'));
		$a = date('Y/m/d',strtotime($plus.' +15 day'));
		$this->dateFrom = $a;
		
		
		while($a <= $this->dateTo){
			$arrDate[] = $a;
			$a = date('Y/m/d',strtotime($a.' +1 day'));
		}
		$cntAllday = count($arrDate);	//その月の日数
		
		
		//社員ループ
		foreach ($this->arrHeader as $srow){
			$bcnt = 0;	//日付配列カウント
			$bkDay = "";	//日付バックアップ
			
			$cntHoliday = 0;	//休日カウント
			$cntNightWork = 0;	//夜間カウント
		
			// add a page
			$this->AddPage();
			//名前出力
			$this->SetXY(25, 38);
			$this->SetFont('msgothic', '', 16);
			$this->Cell(170, 12, $srow['NAME'], '', 1, 'C', 0);
			
			//ページの先頭位置
			$nextY = $this->tabYTop;
			$this->SetFont('msgothic', '', 10);
			
			//社員のスケジュール取得
			$this->arrPlace = $clsSch->getScheduleListforRep2($srow['NO'],$this->dateFrom,$this->dateTo,$srow['HONSHA']);

			//スケジュールループ
			foreach($this->arrPlace as $row) {
				//日付バックアップ
				if ($bkDay <> $row['SPD_DATE_SCHEDULE']){
				//日付が変わったら
					//現場（昼）印字
					if (count($arrDay)>0){
						$this->SetXY($this->genbaXleft, $nextY-5.5);
						$strDay = implode('　',$arrDay);
						$total_length = $this -> GetStringWidth($strDay);
						if ($total_length > 72){
							$this->Cell(72, 5.5, $strDay, '', 0, 'L', 0,'',2);
						}else{
							$this->Cell(72, 5.5, $strDay, '', 0, 'L', 0);
						}
					}
					
					//現場（夜）印字	
					if (count($arrNgt)>0){
						$this->SetTextColor(255, 0, 0);
						$this->SetXY($this->genbaXleft2, $nextY-5.5);
						$strNgt = implode('　',$arrNgt);
						$total_length = $this -> GetStringWidth($strNgt);
						if ($total_length > 72){
							$this->Cell(72, 5.5, $strNgt, '', 0, 'L', 0,'',2);
						}else{
							$this->Cell(72, 5.5, $strNgt, '', 0, 'L', 0);
						}
						//2017.02.13夜間現場で申請休ならカウントしない条件追加
						if ($strNgt <> '申請休'){
							$cntNightWork++;	//夜間カウント
						}
						$this->SetTextColor(0);
					}
				}

			
				//配列日付と、データの最初の日付が異なる場合は、足りない日付分を空行作成
				while ($arrDate[$bcnt] <> $row['SPD_DATE_SCHEDULE']){
					if (strlen($arrDate[$bcnt])==0){
						break;
					}
					if ($bkDay <> $arrDate[$bcnt]){
						//ページの先頭位置
						$this->SetXY($this->tabXleft, $nextY);
						//日付
						$this->Cell(17, 5.5, substr($arrDate[$bcnt],5,5), '', 0, 'C', 0);
						//曜日
						if (date("w", strtotime($arrDate[$bcnt])) == 0){
							$this->SetTextColor(255, 0, 0);
						}
						$this->Cell(15, 5.5, $arrYoubi[date("w", strtotime($arrDate[$bcnt]))], '', 0, 'C', 0);
						$this->SetTextColor(0);

						//休み
						$befY = $this->getY();
						$befX = $this->getX();
						$this->SetXY($befX+1, $nextY);
						$this->Cell(15, 5.5, '休', '', 0, 'L', 0);
						
						$nextY = $this->getY() + 5.5;
						
						$cntHoliday++;	//休日カウント

					}
					$bcnt++;
				}

				//日付バックアップ
				if ($bkDay <> $row['SPD_DATE_SCHEDULE']){
				//日付が変わったら

					//ページの先頭位置
					$this->SetXY($this->tabXleft, $nextY);
					//日付
					$this->Cell(17, 5.5, substr($row['SPD_DATE_SCHEDULE'],5,5), '', 0, 'C', 0);
					//曜日
					if (date("w", strtotime($row['SPD_DATE_SCHEDULE'])) == 0){
						$this->SetTextColor(255, 0, 0);
					}
					$this->Cell(15, 5.5, $arrYoubi[date("w", strtotime($row['SPD_DATE_SCHEDULE']))], '', 0, 'C', 0);
					$this->SetTextColor(0);
					
					$nextY = $this->getY() + 5.5;

					$bkDay = $row['SPD_DATE_SCHEDULE'];
					//カウンタリセット
					
					$arrDay = array();
					$arrNgt = array();
				
				}
				
				if ($row['SPD_DAYNIGHT'] == 0){
					$arrDay[] = (($row['SPD_HOLIDAY_FLG']==1)?"休":$row['SPD_PLACE_NAME']);
				}else if ($row['SPD_DAYNIGHT'] == 1){
					$arrNgt[] = $row['SPD_PLACE_NAME'];
				}
				
				if ($row['SPD_HOLIDAY_FLG']==1){
					$cntHoliday++;	//休日カウント
				}else if (($row['SPD_PLACE_NAME'] == "AM休")||($row['SPD_PLACE_NAME'] == "PM休")){
					$cntHoliday = $cntHoliday + 0.5;	//休日カウント
				//}else if ($row['SPD_PLACE_NAME'] == "申請休"){ //2017.06.19
				}else if (($row['SPD_DAYNIGHT'] <> 1)&&($row['SPD_PLACE_NAME'] == "申請休")){
					$cntHoliday++;	//休日カウント
				}

			}
			//現場（昼）印字
			if (count($arrDay)>0){
				$this->SetXY($this->genbaXleft, $nextY-5.5);
				$strDay = implode('　',$arrDay);
				$total_length = $this -> GetStringWidth($strDay);
				if ($total_length > 72){
					$this->Cell(72, 5.5, $strDay, '', 0, 'L', 0,'',2);
				}else{
					$this->Cell(72, 5.5, $strDay, '', 0, 'L', 0);
				}
			}
			
			//現場（夜）印字	
			if (count($arrNgt)>0){
				$this->SetTextColor(255, 0, 0);
				$this->SetXY($this->genbaXleft2, $nextY-5.5);
				$strNgt = implode('　',$arrNgt);
				$total_length = $this -> GetStringWidth($strNgt);
				if ($total_length > 72){
					$this->Cell(72, 5.5, $strNgt, '', 0, 'L', 0,'',2);
				}else{
					$this->Cell(72, 5.5, $strNgt, '', 0, 'L', 0);
				}
				$this->SetTextColor(0);
				//2017.02.13夜間現場で申請休ならカウントしない条件追加
				if ($strNgt <> '申請休'){
					$cntNightWork++;	//夜間カウント
				}
			}
			$arrDay = array();
			$arrNgt = array();

			while (($arrDate[$bcnt] <= ($this->dateTo))){
				if (strlen($arrDate[$bcnt])==0){
					break;
				}
				if ($bkDay <> $arrDate[$bcnt]){
					//ページの先頭位置
					$this->SetXY($this->tabXleft, $nextY);
					//日付
					$this->Cell(17, 5.5, substr($arrDate[$bcnt],5,5), '', 0, 'C', 0);
					//曜日
					if (date("w", strtotime($arrDate[$bcnt])) == 0){
						$this->SetTextColor(255, 0, 0);
					}
					$this->Cell(15, 5.5, $arrYoubi[date("w", strtotime($arrDate[$bcnt]))], '', 0, 'C', 0);
					$this->SetTextColor(0);
					//休み
					$befY = $this->getY();
					$befX = $this->getX();
					$this->SetXY($befX+1, $nextY);
					$this->Cell(15, 5.5, '休', '', 0, 'L', 0);
				
					$nextY = $this->getY() + 5.5;

					$cntHoliday++;	//休日カウント

				}
				$bcnt++;
			}
			//出勤合計
			$this->SetXY(105, 228);
			$this->Cell(17, 5.5, sprintf("%.1f", $cntAllday - $cntHoliday).'日', '', 0, 'R', 0);
			
			//休日合計
			$this->SetXY(105, 234);
			$this->Cell(17, 5.5, sprintf("%.1f", $cntHoliday).'日', '', 0, 'R', 0);
			
			//合計
			$this->SetXY(105, 246);
			$this->Cell(17, 5.5, sprintf("%.1f", $cntAllday).'日', '', 0, 'R', 0);
			
			//夜間合計
			$this->SetTextColor(255, 0, 0);
			$this->SetXY(178, 228);
			$this->Cell(17, 5.5, $cntNightWork.'日', '', 0, 'R', 0);
			$this->SetXY(178, 246);
			$this->Cell(17, 5.5, $cntNightWork.'日', '', 0, 'R', 0);
			$this->SetTextColor(0);
			


		}
		$clsSch->close();

	}	
	

	
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('Daiki Electric Co. Ltd.');
$pdf->SetAuthor('Daiki Electric Co. Ltd.');
//$pdf->SetMargins(10, 26.5, 5);
//$pdf->SetMargins(10, 27, 5);
$pdf->SetMargins(7, 46, 5);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);

//Footer消去
$pdf->setPrintFooter(false);
//set auto page breaks
//$pdf->SetAutoPageBreak(TRUE, 12);
$pdf->SetAutoPageBreak(false);

// ---------------------------------------------------------

// set font
$pdf->SetFont('msgothic', '', 10);

//期間を決定
$pageLimit = 0;

$sel_nen = substr($req['ym'],0,4);
$sel_getsu = substr($req['ym'],4,2);

$pdf->SetNendo($sel_nen,$sel_getsu);

//Data loading
$pdf->LoadDataHeader();
// add a page
//$pdf->AddPage();
//$pdf->LoadData($from, $to, null);
//$pdf->LoadDataPlace($from, $to, null);
$pdf->OutputTable($pageLimit);


// ---------------------------------------------------------

//Close and output PDF document
//$pdf->Output('reports/example_011.pdf', 'F');
$pdf->Output('kinmu'.$req['ym'].'.pdf', 'I');

//フォントサイズ計算
function getJustFontSizePDF($p_Cls ,$p_sValue, $p_nWidth, $p_nMaxSize) {
	$nWidth = $p_nWidth - 1;
	$nMax 	= $p_nMaxSize * 10;
	$nSize 	= $p_nMaxSize;
	for ($i = 0; $i <= $nMax; $i++) {
		$nSize = $p_nMaxSize - ($i * 0.1);
		$p_Cls->SetFont('msgothic', '', $nSize);
		$nLen = $p_Cls->GetStringWidth($p_sValue);
		if ($nLen <= $nWidth) {
			break;
		}
	}
	return $nSize;
}

//改行文字列処理
function getStringDivid($p_string, $p_length){
	//p_lengthの長さに分割した文字を返す
	//p_length：文字列長（1bite文字の文字数分の長さ）
	
	//文字列の文字数
	$mojiNum = mb_strlen($p_string);
	$a = 0;
	$mojiBite =0;
	$rtn = 0;
	$return = array();
	//文字数分ループ
	while ($a < $mojiNum){
		//$a文字目は全角か半角か？半角なら1全角なら2加算
		if (strlen(mb_substr($p_string,$a,1,'UTF-8')) > 1){
			$mojiBite = $mojiBite + 2;
		}else{
			$mojiBite = $mojiBite + 1;
		}
		//指定の長さを超えたらそのときの文字数を保存してループ終了
		if ($mojiBite > $p_length){
			$rtn = $a;
			break;
		}
		$a++;
	}
	$return[] = mb_substr($p_string,0,$rtn,'UTF-8');
	$return[] = mb_substr($p_string,$rtn,$p_length,'UTF-8');
	return $return;
}

//============================================================+
// END OF FILE                                                
//============================================================+
