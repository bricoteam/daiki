<?php
//============================================================+
// File name   : rep03.php
// Begin       : 2013-10-22
// Last Update : 2013-11-07
// Now 2015-08-28 ◇
//
// Description : 作業予定表出力
//
// Author: Reiko Suzuki @ Bricoleur inc.
//
// (c) Copyright:
//               Daiki Electric Co. Ltd.
//               Bricoleur inc.
//============================================================+
if ($_SERVER[SERVER_NAME] == 'daiki.bricoleur.in'){
	require_once('ini.inc');
}
include_once 'func.common.inc';
include_once 'func.field.inc';
include_once 'func.fieldcheck.inc';

include_once 'class.cls_schedule.inc';

session_start();
$req = $_REQUEST;

//ログインチェック
$blogin = isLogin();
if (!($blogin)){
	header("Location: ".URL_LOGIN);
	exit;
}
require_once('config/lang/jpn.php');
require_once('tcpdf.php');
require_once('fpdi.php');

$exit = false;
if (strlen($req['target'])==0){
	print 'パラメーターエラー：対象が選択されていません。<br>';
	$exit = true;
}
if (strlen($req['range'])==0){
	print 'パラメーターエラー：期間区分が選択されていません。<br>';
	$exit = true;
}else{
	if (($req['range']=='0')&&(strlen($req['span'])==0)){
		print 'パラメーターエラー：印刷範囲が選択されていません。<br>';
		$exit = true;
	}else if (($req['range']=='1')&&((strlen($req['from'])==0)||(strlen($req['to'])==0))){
		print 'パラメーターエラー：印刷範囲日付が入力されていません。<br>';
		$exit = true;
	}else if (($req['range']=='1')&&((!isValidDate($req['from'].' 00:00:00'))||(!isValidDate($req['to'].' 00:00:00')))){
		print 'パラメーターエラー：印刷範囲日付が不正です。<br>';
		$exit = true;
	}
}
if ($exit){
	exit;
}

// extend TCPF with custom functions
class MYPDF extends FPDI {
	//Column titles
	var $header = array('月 日', '曜日', '現　場　名', '人数','昼　　間','夜　　間');
	//カラムのセル幅
	var $arrW = array(60, 45, 12, 14, 20, 22, 20);
	
	var $cellH = 9;		//セルの高さ
	var $cellH1 = 12;		//見出し(1)セルの高さ
	var $cellH2 = 5;		//見出し(2)セルの高さ
	var $cellH3 = 7;		//見出し(3)セルの高さ
	var $cellL1 = 18;	//1ページに入る行数（最初のページ）
	var $cellL2 = 28;	//1ページに入る行数（2ページ目以降）
	var $choH = 4.5;	//二行出力のときの高さ
	var $cpY = 17.5;	//データセルの始まる位置（？）






//◆	var $tabXleft = 7;		//テーブルの左端
//◇	var $tabXleft = 3;		//テーブルの左端
	var $tabXleft = 1;		//テーブルの左端◇
//	var $tabYTop = 41;		//テーブルの上端
	var $tabYTop = 46;		//テーブルの上端
//◆	var $genbaXleft = 21;		//昼間現場の左端
//◇	var $genbaXleft = 17;		//昼間現場の左端
	var $genbaXleft = 9;		//昼間現場の左端◇
//◇	var $genbaXleft2 = 111.5;	//夜間現場の左端
	var $genbaXleft2 = 109;	//夜間現場の左端◇
//◆	var $genbaXleft2 = 107.5;	//夜間現場の左端
	
	//◆
//◇	var $wAllHeader = 203;		//月日・曜日・昼間・夜間の幅
	var $wAllHeader = 208;		//月日・曜日・昼間・夜間の幅◇
	
//◇	var $wDaNHeader = 94.5;		//昼間セルの幅、夜間セルの幅
	var $wDaNHeader = 100;		//昼間セルの幅、夜間セルの幅◇
	var $wDpNHeader = 200;		//昼間セルの幅＋夜間セルの幅
//◇	var $maxShainSu = 16;		//社員数最大
	var $maxShainSu = 18;		//社員数最大◇
	

	var $arrHeader;	//見出し
	var $arrPlace;	//現場見出し
	var $arrSchedule;	//予定データ
	
	var $dateFrom;
	var $dateTo;
	
	var $pageLimit;

	var $flgCoope;

	//Page header
	function Header() {
		// Header
		$this->setSourceFile('rep030.pdf');
		//◆日付出力
		$this->SetXY(-55, 12.3);
		$this->SetFont('msgothic', '', 10);
		$this->Cell(10, 8, date('Y/m/d'), '', 0, 'R', 0);
		//◆ページ出力
		$this->SetXY(-25, 12.3);
		$this->Cell(10, 8, $this->getPage(), '', 0, 'R', 0);
		$this->SetXY(10, $this->cpY);

		//$this->LoadTitle(0,$this->tabXleft,25);
		$this->LoadTitle(0,$this->tabXleft,30);
		$nowY = $this->getY();
		
		// import page 1
		$tplIdx = $this->importPage(1);
		// use the imported page 
		$this->useTemplate($tplIdx);
	}
	function AddPage($orientation = '', $format = '') {
		parent::AddPage($orientation, $format);
	}
	// Load table data from file
	public function LoadTitle($p_horf,$p_x,$p_y) {
		//社員名のテーブル見出し部作成。$p_horf：0=上部,1=下部
		$this->SetDrawColor(0);
		$this->SetFillColor(255, 255, 255);
		$this->SetTextColor(0);
		$this->SetLineWidth(0.2);
		$this->SetFont('msgothic', '', 8);
		
		$this->SetXY($p_x, $p_y);
//◆		$this->Cell(195, 16, '', 1, 0, 'C', 0);
		$this->Cell($this->wAllHeader, 16, '', 1, 0, 'C', 0);
		$this->SetXY($p_x, $p_y);
		//◇$this->SetLineWidth(0.1);
		$this->SetLineWidth(0.2);//◇
		$this->Cell(8, 16, '月 日', 'R', 0, 'C', 0);
		//◇$this->SetLineWidth(0.2);
		//◇$this->Cell(6, 16, '曜日', 'R', 0, 'C', 0);
		$befX = $this->getX();
		$befY = $this->getY();
		
		//◆ヘッダー上部だったら
		if ($p_horf==0){
			$this->Cell($this->wDaNHeader, 4, '昼　　間', 'RB', 0, 'C', 0);
			$this->Cell($this->wDaNHeader, 4, '夜　　間', 'B', 0, 'C', 0);
		}
		
		$arrShain = $this->arrHeader;
		$a = 0;
		//◆ヘッダー上部なら昼間夜間行を出した分下に印字
		if ($p_horf==0){
			$this->SetXY($befX, $befY+4);
		}else{
			$nowX = $this->getX();
		}
		//◆2回繰り返す（昼、夜）
		while($a < 2){
			$this->SetLineWidth(0.1);
			//◇$this->Cell(26.5, 12, '現　場　名', 'R', 0, 'C', 0);
			$this->Cell(24, 12, '現　場　名', 'R', 0, 'C', 0);//◇
			$this->SetLineWidth(0.2);
			$befX = $this->getX();
			$befY = $this->getY();
			$this->Cell(4, 6, "人", 'R', 2, 'C', 0);
			$this->Cell(4, 6, "数", 'R', 0, 'C', 0);
			$cnt = 0;
			$this->SetLineWidth(0.1);
			foreach ($arrShain as $row){
				//ストライプテーブル
				//◆if ($cnt%2==1){
				if ($cnt%2==0){
					$this->SetFillColor(230);
					$touka = 1;
				}else{
					$this->SetFillColor(255);
					$touka = 0;
				}
			
				if (count($arrShain)==$this->maxShainSu){
					$this->SetLineWidth(0.2);
					//break; //◇
				}
				$this->SetXY($befX+4+($cnt*4), $befY+0.0);
				$s = mb_strlen($row['NICK'],'UTF-8');
				$height = 12 / $s;
				for ($i = 0; $i < $s; ++$i){
					$this->Cell(4, $height, mb_substr($row['NICK'],$i,1,'UTF-8'), 'LR', 2, 'C', $touka);
				//	$this->Cell(4.5, $height, $row['NICK'], 'R', 2, 'C', 0);
//					if ($i==0){
//						$this->Cell(4, $height, mb_substr($row['NICK'],$i,1,'UTF-8'), 'LR', 2, 'C', $touka);
//					}
				}
				$this->SetXY($befX+4+($cnt*4), $befY+0.0);
				$this->SetLineWidth(0.2);
				$this->Cell(4, 12, '', 'TB', 2, 'C', 0);
				$this->SetLineWidth(0.1);
				$cnt++;
			}
			if ($cnt<$this->maxShainSu){
				$kara = $this->maxShainSu-$cnt;
				$kara2 = $cnt;
				for ($i = 0; $i < $kara; ++$i){
					//ストライプテーブル
					//◆if ($kara2%2==1){
					if ($kara2%2==0){
						$this->SetFillColor(230);
						$touka = 1;
					}else{
						$this->SetFillColor(255);
						$touka = 0;
					}
					$this->SetXY($befX+4+($cnt*4)+($i*4), $befY+0.0);
					$this->Cell(4, 12, '', 'LR', 2, 'C', $touka);
					//太い線上に
					$this->SetLineWidth(0.2);
					$this->SetXY($befX+4+($cnt*4)+($i*4), $befY+0.0);
					$this->Cell(4, 12, '', 'TB', 2, 'C', 0);
					$this->SetLineWidth(0.1);
					if ($i == ($kara-1)){
						//最後は右に太い線
						$this->SetXY($befX+4+($cnt*4)+($i*4), $befY);
						$this->SetLineWidth(0.2);
						$this->Cell(4, 12, '', 'R', 2, 'C', 0);
					}
					$kara2++;
				}
			}
			$this->SetFillColor(0);
			$befX = $this->getX();
			$befY = $this->getY();
			$this->SetXY($befX+4, $befY-12);
			$a++;
		}
		if ($p_horf==1){
			$nowY = $this->getY();
			$this->SetXY($nowX, $nowY+12);
			$this->Cell($this->wDaNHeader, 4, '昼　　間', 'TR', 0, 'C', 0);
			$this->Cell($this->wDaNHeader, 4, '夜　　間', 'T', 0, 'C', 0);
		}
		
		$befY = $this->getY();
		$this->SetXY($this->tabXleft, $this->tabYTop);


	}

	// Load table data from file
	public function LoadData($p_from,$p_to,$p_kbn=null) {
		//期間を保存
		$this->dateFrom = $p_from;
		$this->dateTo = $p_to;
		$clsSch 	= new cls_schedule();
		//期間を抽出
		$this->arrSchedule = $clsSch->getScheduleListforRep($p_from,$p_to,$p_kbn);
		$clsSch->close();
		return true;
	}
	// Load table data from file
	public function LoadDataPlace($p_from,$p_to,$p_kbn=null) {
		$clsSch 	= new cls_schedule();
		//期間を抽出
		$this->arrPlace = $clsSch->getPlaceListforRep($p_from,$p_to,$p_kbn);
		$clsSch->close();
		return true;
	}
	// Load table data from file
	public function LoadDataHeader1() {
		//社員読み込み
		$clsSch 	= new cls_schedule();
		//出力対象
		$this->arrHeader = $clsSch->getShain();
		$this->flgCoope = false;

		$clsSch->close();

		return true;
	}
	public function LoadDataHeader2($p_from,$p_to) {
		$clsSch 	= new cls_schedule();
		
		//社員読み込み
		$clsSch 	= new cls_schedule();
		//出力対象
		//$this->arrHeader = $clsSch->getShain(1);
		$this->arrHeader = $clsSch->getCoope($p_from,$p_to);
		$this->flgCoope = true;

		$clsSch->close();

		return true;
	}
	
	// Colored table
	public function OutputTable($p_limit) {
		
		$this->pageLimit = $p_limit;
		
		$cnt = 1;	//1ページの行カウント
		$bcnt = 0;	//日付配列カウント
		$pCntDay = 0;	//日付内行カウント
		$pCntNgt = 0;	//日付内行カウント
		$bkDay = "";	//日付バックアップ
		$bkDayNight = "";	//昼夜バックアップ
		
		$bBreak = false;	//ブレークしたよフラグ
		
		$arrYoubi = array(0=>"日",1=>"月",2=>"火",3=>"水",4=>"木",5=>"金",6=>"土");
		
		//日付ループ用配列の作成
		$a = $this->dateFrom;
		while($a <= $this->dateTo){

			$arrDate[] = $a;
			$a = date('Y/m/d',strtotime($a.' +1 day'));
		}

		
		//ページの先頭位置
		$this->SetXY($this->tabXleft, $this->tabYTop);

		$this->SetFont('msgothic', '', 8);
		

		//現場ループ
		foreach($this->arrPlace as $row) {
			//日付バックアップ
			if ($bkDay <> $row['SPD_DATE_SCHEDULE']){
			//日付が変わったら（または一件目）
				$this->SetLineWidth(0.2);
				//太い区切り罫線
				$befX = $this->getX();
				$befY = $this->getY();
				$this->SetXY($this->tabXleft, $befY);
				$this->Cell($this->wAllHeader, 4, '', 'T', 0, 'L', 0);
				$this->SetLineWidth(0.1);
				$this->SetXY($befX, $befY);
				
				$this->SetLineWidth(0.1);
				
				
				//日付リスト作成
				$iADaycnt = 0;
				$iANgtcnt = 0;
				foreach($this->arrPlace as $r) {
					if ($row['SPD_DATE_SCHEDULE'] == $r['SPD_DATE_SCHEDULE']){
						if ($r['SPD_DAYNIGHT']==0){
							$iADaycnt++;
						}else if ($r['SPD_DAYNIGHT']==1){
							$iANgtcnt++;
						}
					}
				}
				//改ページ判断(MAX桁数を超える場合、フッターを出力して改ページする)
				if ((($cnt + $iADaycnt) > 55)||(($cnt + $iANgtcnt) > 55)||(($cnt + 6) > 55)){
					$nowY = $this->getY();
//★					$this->LoadTitle(1,$this->tabXleft,$nowY);
					if( $this->getPage() == $this->pageLimit ) {
						$bBreak = true;
						break;
					}
					$this->LoadTitle(1,$this->tabXleft,$nowY);

					// add a page
					$this->AddPage();
					$befY = $this->getY();
					$iYDayTop = $befY;
					$cnt = 1;
				}

			}

			
			//配列日付と、データの最初の日付が異なる場合は、足りない日付分を空行作成
			while ($arrDate[$bcnt] <> $row['SPD_DATE_SCHEDULE']){
				if ($bkDay <> $arrDate[$bcnt]){
					for($i = 0; $i < 6; $i++) {
						if ($i == 2){
							//月日の左罫線
							$befY = $this->getY();
							$this->SetXY($this->tabXleft, $befY);
							$this->SetLineWidth(0.2);
							$this->Cell(8, 4, '', 'L', 0, 'C', 0);
							//月日
							//◇$this->SetLineWidth(0.1);
							$this->SetXY($this->tabXleft, $befY);
							$this->Cell(8, 4, substr($arrDate[$bcnt],5,5), 'R', 0, 'C', 0);
							//◇$time = strtotime($arrDate[$bcnt]);
							//◇$this->SetLineWidth(0.2);
							//曜日
							//◇$this->Cell(6, 4, $arrYoubi[date("w", $time)], 'R', 0, 'C', 0);
							$this->SetXY($this->tabXleft, $befY+4);//◇
							$this->Cell(8, 4, $arrYoubi[date("w", $time)], 'R', 0, 'C', 0);//◇
							$this->SetXY($this->tabXleft+8, $befY);//◇
							//総行カウントアップ
							$cnt++;
						}else {
						//昼間だったら空セルを
							$befY = $this->getY();
							$this->SetXY($this->tabXleft, $befY);
							$this->SetLineWidth(0.2);
							$this->Cell(8, 4, '', 'L', 0, 'C', 0);
							$this->SetXY($this->tabXleft, $befY);
							//◇$this->SetLineWidth(0.1);
							$this->Cell(8, 4, '', 'R', 0, 'C', 0);
							//◇$this->SetLineWidth(0.2);
							//◇$this->Cell(6, 4, '', 'R', 0, 'C', 0);
							//総行カウントアップ
							$cnt++;
						}
						$dn = 0;
						while ($dn < 2){
							$dn++;
							$this->SetLineWidth(0.1);
							//現場
							//◇$this->Cell(26.5, 4, '', 'R', 0, 'L', 0);
							$this->Cell(24, 4, '', 'R', 0, 'L', 0);//◇
							//現場の罫線左
							$this->SetLineWidth(0.2);
							//人数の罫線左
							$this->Cell(4, 4, '', 'R', 0, 'L', 0);
							$this->SetLineWidth(0.1);
							$l = 0;
							while ($l < ($this->maxShainSu - 1)){
								//ストライプテーブル
								//◆if ($l%2==1){
								if ($l%2==0){
									$this->SetFillColor(230);
									$touka = 1;
								}else{
									$this->SetFillColor(255);
									$touka = 0;
								}
								$l++;
								$this->Cell(4, 4, '', 'TRL', 0, 'L', $touka);
							}
							
							$this->SetLineWidth(0.2);
							if ($dn == 2){
								$this->Cell(4, 4, '', 'R', 2, 'L', 0);
								$befY = $this->getY();
								$this->SetXY($this->tabXleft, $befY);
						
							}else{
								$this->Cell(4, 4, '', 'R', 0, 'L', 0);
							}
						}
						if ($i == 0){
							$this->SetLineWidth(0.2);
							//太い区切り罫線
							$befX = $this->getX();
							$befY = $this->getY();
							$this->SetXY($befX, $befY-4);
							$this->Cell($this->wAllHeader, 4, '', 'T', 0, 'L', 0);
							$this->SetLineWidth(0.1);
//							$this->SetXY($befX, $befY);
//★fix15/11/20							$this->SetXY($befX+14, $befY);
							$this->SetXY($befX+8, $befY);
							$this->Cell($this->wDpNHeader, 4, '', 'T', 0, 'L', 0);
							$this->SetXY($befX, $befY);
						}else{
							$this->SetLineWidth(0.1);
							//細い区切り罫線
							$befX = $this->getX();
							$befY = $this->getY();
//★fix15/11/20							$this->SetXY($befX+14, $befY);
							$this->SetXY($befX+8, $befY);
							$this->Cell($this->wDpNHeader, 4, '', 'T', 0, 'L', 0);
							$this->SetXY($befX, $befY);
						}
					}			
					//改ページ判断(MAX桁数を超える場合、フッターを出力して改ページする)
					if (($cnt + 6) > 55){
						$nowY = $this->getY();
//★						$this->LoadTitle(1,$this->tabXleft,$nowY);
						if( $this->getPage() == $this->pageLimit ) {
							$bBreak = true;
							break 2;
						}
						$this->LoadTitle(1,$this->tabXleft,$nowY);
						// add a page
						$this->AddPage();
						$befY = $this->getY();
						$iYDayTop = $befY;
						$cnt = 1;
//////////////////////////////////////////////////
//					}else{
//						$this->SetLineWidth(0.2);
//						//太い区切り罫線
//						$befX = $this->getX();
//						$befY = $this->getY();
//						$this->SetXY($this->tabXleft, $befY);
//						$this->Cell(195, 4, '', 'T', 0, 'L', 0);
//						$this->SetLineWidth(0.1);
//						$this->SetXY($befX, $befY);
///////////////////////////////////////////////////
					}
				}
				$bcnt++;
			}

			//日付バックアップ
			if ($bkDay <> $row['SPD_DATE_SCHEDULE']){
			//日付が変わったら
				$bkDay = $row['SPD_DATE_SCHEDULE'];
				$bkDayNight = $row['SPD_DAYNIGHT'];
				//カウンタリセット
				$pCntDay = 0;
				$pCntNgt = 0;
				$iYDayTop = $befY;
				$iYNgtTop = $befY;
				
			}else{
				if ($bkDayNight <> $row['SPD_DAYNIGHT']){
					//昼夜フラグ
					$bkDayNight = $row['SPD_DAYNIGHT'];
				}	
			}
			
			
			//その日の現場が６行未満の場合は空行出力

			//さらに夜間の現場が無かった場合は夜間の部に６行空行出力
			
			//夜間の現場になったらバックアップしたX位置に移動
			
			//３行目に月日と曜日を出力
			if (($row['SPD_DAYNIGHT']==0)&&($pCntDay == 2)){
				//月日の左罫線
				$befY = $this->getY();
				$this->SetXY($this->tabXleft, $befY);
				$this->SetLineWidth(0.2);
				$this->Cell(8, 4, '', 'L', 0, 'C', 0);
				//月日
				//◇$this->SetLineWidth(0.1);
				$this->SetXY($this->tabXleft, $befY);
				$this->Cell(8, 4, substr($row['SPD_DATE_SCHEDULE'],5,5), 'R', 0, 'C', 0);
				$time = strtotime($row['SPD_DATE_SCHEDULE']);
				//◇$this->SetLineWidth(0.2);
				$this->SetXY($this->tabXleft, $befY+4);//◇
				//◇$this->Cell(6, 4, $arrYoubi[date("w", $time)], 'R', 0, 'C', 0);
				$this->Cell(8, 4, $arrYoubi[date("w", $time)], 'R', 0, 'C', 0);
				$this->SetXY($this->tabXleft+8, $befY);//◇
				$pCntDay++;
				//総行カウントアップ
				$cnt++;
			}else if ($row['SPD_DAYNIGHT']==0){
			//昼間だったら空セルを
				$befY = $this->getY();
				$this->SetXY($this->tabXleft, $befY);
				$this->SetLineWidth(0.2);
				$this->Cell(8, 4, '', 'L', 0, 'C', 0);
				$this->SetXY($this->tabXleft, $befY);
				//◇$this->SetLineWidth(0.1);
				$this->Cell(8, 4, '', 'R', 0, 'C', 0);
				//◇$this->SetLineWidth(0.2);
				//◇$this->Cell(6, 4, '', 'R', 0, 'C', 0);
				$pCntDay++;
				//総行カウントアップ
				$cnt++;
			}else if ($row['SPD_DAYNIGHT']==1){
			//夜間だったら出力しない
				$this->SetXY($this->genbaXleft2, $iYDayTop);
				$iYDayTop = $iYDayTop + 4;

				$pCntNgt++;
			}

			//現場名出力
			$this->SetLineWidth(0.1);
//			$nFontSize = getJustFontSizePDF($this,$row['SPD_PLACE_NAME'], 26, 8);
			$total_length = $this -> GetStringWidth($row['SPD_PLACE_NAME']);
			//◇if ($total_length > 26.5){
			if ($total_length > 23.5){
				//◇$this->Cell(26.5, 4, $row['SPD_PLACE_NAME'], 'BR', 0, 'L', 0,'',2);
				$this->Cell(24, 4, $row['SPD_PLACE_NAME'], 'BR', 0, 'L', 0,'',2);//◇
			}else{
				//◇$this->Cell(26.5, 4, $row['SPD_PLACE_NAME'], 'BR', 0, 'L', 0);
				$this->Cell(24, 4, $row['SPD_PLACE_NAME'], 'BR', 0, 'L', 0);//◇
			}

			$befX = $this->getX();
			$befY = $this->getY();
			$this->SetFont('msgothic', '', 8);
			//現場社員より人数が多かったら赤字に
			if ((!$this->flgCoope)&&(count($this->arrSchedule[$row['SPD_PLACE_NO']]) < nVal($row['SPD_REQUIRE_NUM'],0))){
				$this->SetTextColor(255,0,0);
				$this->Cell(4, 4, $row['SPD_REQUIRE_NUM'], 'B', 0, 'L', 0);
				$this->SetTextColor(0);
			}else{
				$this->Cell(4, 4, $row['SPD_REQUIRE_NUM'], 'B', 0, 'L', 0);
			}
			$this->SetXY($befX, $befY);
			$this->SetLineWidth(0.2);
			$this->Cell(4, 4, '', 'R', 0, 'L', 0);
			
			//予定出力
			$befX = $this->getX();
			$befY = $this->getY();

			$arrShain = $this->arrHeader;
			$ic = 0;
			if (count($arrShain)>0){
				foreach ($arrShain as $rShain){
					//ストライプテーブル
					//◆if ($ic%2==1){
					if ($ic%2==0){
						$this->SetFillColor(230);
						$touka = 1;
					}else{
						$this->SetFillColor(255);
						$touka = 0;
					}
					$ic++;
					$this->SetLineWidth(0.1);
					$kaigyou = 0;
					if ($ic == $this->maxShainSu){
	//					$this->SetLineWidth(0.2);
						$kaigyou = 2;
						$befXmax = $this->getX();
						$befYmax = $this->getY();
						//break;//◇
					}
					if ($this->arrSchedule[$row['SPD_PLACE_NO']][$rShain['NO']] == 1 ){
						if ($row['SPD_HOLIDAY_FLG']==1){
							$ssche = '休';
						}else{
							$ssche = '○';
						}
	//					$this->Cell(4, 4, $ssche, 'R', $kaigyou, 'C', 0);
						$this->Cell(4, 4, $ssche, 'TRL', $kaigyou, 'C', $touka);
					}else{
	//					$this->Cell(4, 4, '', 'R', $kaigyou, 'C', 0);
						$this->Cell(4, 4, '', 'TRL', $kaigyou, 'C', $touka);
					}
				}
			}
			
			//◆予定データ最後が社員数より少なかったら空セル出力
			$kara2 = 0;
			if ($ic < $this->maxShainSu){
				$kara = $this->maxShainSu - $ic; //足りないセル数算出
				$kara2 = $ic;			//現在列番
				for ($b = 0; $b < $kara; ++$b){
					//ストライプテーブル
					//◆if ($kara2%2==1){
					if ($kara2%2==0){
						$this->SetFillColor(230);
						$touka = 1;
					}else{
						$this->SetFillColor(255);
						$touka = 0;
					}
					$kaigyou = 0;
					if ($b == ($kara-1)){
//						$this->SetLineWidth(0.2);
						$kaigyou = 2;
					}			
					$befX = $this->getX();
					$befY = $this->getY();

					$this->Cell(4, 4, '', 'TLRB', $kaigyou, 'C', $touka);
					$kara2++;
				}
			}
			//◆仕切り線
			if ($kara2 == 0){
				$this->SetXY($befXmax, $befYmax);
				$this->Cell(4, 4, '', 'TLRB', 0, 'C', $touka);
				//昼夜の仕切り線太いの出力
				$this->SetXY($befXmax, $befYmax);
				$this->SetLineWidth(0.2);
				$this->Cell(4, 4, '', 'R', $kaigyou, 'C', 0);
				$this->SetLineWidth(0.1);
			}else{
			//昼夜の仕切り線太いの出力
			$this->SetXY($befX, $befY);
			$this->SetLineWidth(0.2);
			$this->Cell(4, 4, '', 'R', $kaigyou, 'C', 0);
			$this->SetLineWidth(0.1);
			}
			//細い区切り罫線
			$befX = $this->getX();
			$befY = $this->getY();
			if ((($pCntDay == 1)&&($row['SPD_DAYNIGHT']==0))||(($pCntNgt == 1)&&($row['SPD_DAYNIGHT']==1))){
				$this->SetLineWidth(0.2);
				$this->SetXY($this->tabXleft, $befY-4);
				$this->Cell(21, 4, '', 'T', 0, 'L', 0);
			}else{
				$this->SetLineWidth(0.1);
			}
			if ($row['SPD_DAYNIGHT']==0){
			//昼間
				$this->SetXY($this->genbaXleft, $befY-4);
				$this->Cell($this->wDaNHeader, 4, '', 'T', 0, 'L', 0);
			}else if ($row['SPD_DAYNIGHT']==1){
			//夜間
				$this->SetXY($this->genbaXleft2, $befY-4);
				$this->Cell($this->wDaNHeader, 4, '', 'T', 0, 'L', 0);
			}
			$this->SetXY($befX, $befY);


			//足りない行数を埋める
			if ((($iADaycnt == 0))||(($row['SPD_DAYNIGHT']==0)&&($iADaycnt == $pCntDay )&&(($pCntDay < 6)||(6 < $iANgtcnt)))){
				//昼間の現場がない場合もしくは、昼間行処理中で、6行以下の場合、または夜間が6行以上ある場合は夜間のMAX行数に合わせて空行出力
				if ($iADaycnt == 0){
					//夜間の開始位置
					$this->SetXY($this->tabXleft, $iYNgtTop);
//$this->SetDrawColor(255, 0, 0);
					$this->SetLineWidth(0.2);
					//太い区切り罫線
					$this->Cell($this->wAllHeader, 4, '', 'T', 0, 'L', 0);
					$this->SetLineWidth(0.1);
//$this->SetDrawColor(0, 0, 0);
					$this->SetXY($this->tabXleft, $iYNgtTop);
				}
				
				
				$maxl = 6;
				if ($iANgtcnt > 6) {
					$maxl = $iANgtcnt;
				}
				$i2 = 0;
				for($i = $pCntDay; $i < ($maxl); $i++) {
					$i2++;
//★★★//
//$befX = $this->getX();
//$befY = $this->getY();
//$this->SetXY(0, $befY);
//$this->Cell(20, 4, '  ('.$pCntNgt.')='.$cnt, 'T', 0, 'L', 0);
//$this->SetXY($befX, $befY);
					if ($pCntDay + $i2 == 3){
						//月日の左罫線
						$befY = $this->getY();
						$this->SetXY($this->tabXleft, $befY);
						$this->SetLineWidth(0.2);
						$this->Cell(8, 4, '', 'L', 0, 'C', 0);
						//月日
						//◇$this->SetLineWidth(0.1);
						$this->SetXY($this->tabXleft, $befY);
						$this->Cell(8, 4, substr($bkDay,5,5), 'R', 0, 'C', 0);
						$time = strtotime($bkDay);
						//◇$this->SetLineWidth(0.2);
						//曜日
						$this->SetXY($this->tabXleft, $befY+4);//◇
						$this->Cell(8, 4, $arrYoubi[date("w", $time)], 'R', 0, 'C', 0);
						$this->SetXY($this->tabXleft+8, $befY);//◇
					}else {
					//昼間だったら空セルを
						$befY = $this->getY();
						$this->SetXY($this->tabXleft, $befY);
						$this->SetLineWidth(0.2);
						$this->Cell(8, 4, '', 'L', 0, 'C', 0);
						$this->SetXY($this->tabXleft, $befY);
						//◇$this->SetLineWidth(0.1);
						$this->Cell(8, 4, '', 'R', 0, 'C', 0);
						//◇$this->SetLineWidth(0.2);
						//◇$this->Cell(6, 4, '', 'R', 0, 'C', 0);
					}
					//総行カウントアップ
					if (($pCntNgt == 0)||($pCntNgt == 1)){
						$cnt++;
					}

					$this->SetLineWidth(0.1);
					//現場
					//◇$this->Cell(26.5, 4, '', 'R', 0, 'L', 0);
					$this->Cell(24, 4, '', 'R', 0, 'L', 0);
					//現場の罫線左
					$this->SetLineWidth(0.2);
					//人数の罫線左
					$this->Cell(4, 4, '', 'R', 0, 'L', 0);
					$this->SetLineWidth(0.1);
					$l = 0;
					while ($l < ($this->maxShainSu - 1)){
						//ストライプテーブル
						//◆if ($l%2==1){
						if ($l%2==0){
							$this->SetFillColor(230);
							$touka = 1;
						}else{
							$this->SetFillColor(255);
							$touka = 0;
						}
						$l++;
						$this->Cell(4, 4, '', 'TRL', 0, 'L', $touka);
					}
					$this->SetLineWidth(0.2);
					$this->Cell(4, 4, '', 'R', 2, 'L', 0);

					if ($i == $maxl){
				//		$this->SetLineWidth(0.2);
				//		//太い区切り罫線
				//		$befX = $this->getX();
				//		$befY = $this->getY();
				//		$this->Cell(195, 4, '', 'T', 0, 'L', 0);
				//		$this->SetLineWidth(0.1);
				//		$this->SetXY($befX, $befY);
/*					if (($cnt<>0)&&($i == 0)){
*/
		//					$this->SetLineWidth(0.2);
		//					//太い区切り罫線
		//					$befX = $this->getX();
		//					$befY = $this->getY();
		//					$this->SetXY($befX, $befY-4);
		//					$this->Cell(195, 4, '', 'T', 0, 'L', 0);
		//					$this->SetLineWidth(0.1);
//		//					$this->SetXY($befX, $befY);
		//					$this->SetXY($befX+14, $befY);
		//					$this->Cell(181, 4, '', 'T', 0, 'L', 0);
		//					$this->SetXY($befX, $befY);

					}else{
						$this->SetLineWidth(0.1);
						//細い区切り罫線
						$befX = $this->getX();
						$befY = $this->getY();
						$this->SetXY($this->genbaXleft, $befY);
						$this->Cell($this->wDaNHeader, 4, '', 'T', 0, 'L', 0);
						if ($i == 0){
							$this->SetLineWidth(0.2);
							$this->SetXY($this->genbaXleft, $befY-4);
							$this->Cell($this->wDaNHeader, 4, '', 'T', 0, 'L', 0);
							$this->SetLineWidth(0.1);
						}
						$this->SetXY($befX, $befY);
					}
				}
			}

			//足りない行数を埋める
			if ((($iANgtcnt == 0)&&($iADaycnt == $pCntDay))||(($row['SPD_DAYNIGHT']==1)&&($iANgtcnt == $pCntNgt )&&(($pCntNgt < 6)||(6 < $iADaycnt)))){
				//夜間の現場が無い場合で、昼間の最終行　または　夜間ループ中で、夜間の最終行で、夜間が6行より少ない又は昼間が6行より多い
				if (($iANgtcnt == 0)&&($iADaycnt == $pCntDay)){
				//夜間の現場がない場合で、昼間の最後のとき
					//夜間の開始位置
					$this->SetXY($this->genbaXleft2, $iYDayTop);
				}

				$maxl = 6;
				if ($iADaycnt > 6) {
					$maxl = $iADaycnt;
				}
				if (($iADaycnt == 0)){
					//昼の出力がないパターン
					//夜間の開始位置
					$befY = $this->getY();
					$this->SetXY($this->genbaXleft2, $befY - (($maxl - $iANgtcnt)*4));
				}
				$i2 = 0;
				for($i = $pCntNgt; $i < ($maxl); $i++) {
					$i2++;
					//夜間の開始位置
					$befY = $this->getY();
					$this->SetXY($this->genbaXleft2, $befY);

					$this->SetLineWidth(0.1);
					//現場
					//◇$this->Cell(26.5, 4, '', 'R', 0, 'L', 0);
					$this->Cell(24, 4, '', 'R', 0, 'L', 0);
					//現場の罫線左
					$this->SetLineWidth(0.2);
					//人数の罫線左
					$this->Cell(4, 4, '', 'R', 0, 'L', 0);
					$this->SetLineWidth(0.1);
					$l = 0;
					while ($l < ($this->maxShainSu - 1)){
						//ストライプテーブル
						//◆if ($l%2==1){
						if ($l%2==0){
							$this->SetFillColor(230);
							$touka = 1;
						}else{
							$this->SetFillColor(255);
							$touka = 0;
						}
						$l++;
						$this->Cell(4, 4, '', 'TRL', 0, 'L', $touka);
					}
					$this->SetLineWidth(0.2);
					$this->Cell(4, 4, '', 'R', 2, 'L', 0);

//					if ($i == $maxl){
				//		$this->SetLineWidth(0.2);
				//		//太い区切り罫線
				//		$befX = $this->getX();
				//		$befY = $this->getY();
				//		$this->Cell(195, 4, '', 'T', 0, 'L', 0);
				//		$this->SetLineWidth(0.1);
				//		$this->SetXY($this->tabXleft, $befY);
					if ($i == 0){
						$this->SetLineWidth(0.2);
						//太い区切り罫線
						$befX = $this->getX();
						$befY = $this->getY();
//						$this->SetXY($befX, $befY-4);
						$this->SetXY($this->genbaXleft2, $befY-4);
						$this->Cell($this->wDaNHeader, 4, '', 'T', 0, 'L', 0);
						$this->SetLineWidth(0.1);
//							$this->SetXY($befX, $befY);
						$this->SetXY($this->genbaXleft2, $befY);
						$this->Cell($this->wDaNHeader, 4, '', 'T', 0, 'L', 0);
					//	$this->SetXY($befX, $befY);
					}else{
						$this->SetLineWidth(0.1);
						//細い区切り罫線
						$befX = $this->getX();
						$befY = $this->getY();
						$this->SetXY($this->genbaXleft2, $befY);
						$this->Cell($this->wDaNHeader, 4, '', 'T', 0, 'L', 0);
						$this->SetXY($befX, $befY);
					}
				}
			}

		}
		//配列日付と、データの最初の日付が異なる場合は、足りない日付分を空行作成
		//太い区切り罫線
		$befX = $this->getX();
		$befY = $this->getY();
		$this->SetLineWidth(0.2);
		$this->SetXY($this->tabXleft, $befY);
		$this->Cell($this->wAllHeader, 4, '', 'T', 0, 'L', 0);
		$this->SetXY($befX, $befY);
		$this->SetLineWidth(0.1);
		$maxbcnt = count($arrDate);
		
		if ($bBreak === false){
			while (($arrDate[$bcnt] <> strtotime($this->dateTo.' +1 day'))&&($bcnt < $maxbcnt)){
				if ($bkDay <> $arrDate[$bcnt]){
					//改ページ判断(MAX桁数を超える場合、フッターを出力して改ページする)
					if (($cnt + 6) > 55){
						if( $this->getPage() == $this->pageLimit ) {
							break;
						}
						$nowY = $this->getY();
						$this->LoadTitle(1,$this->tabXleft,$nowY);
						// add a page
						$this->AddPage();
						$befY = $this->getY();
						$iYDayTop = $befY;
						$cnt = 1;
					}

					for($i = 0; $i < 6; $i++) {
						if ($i == 2){
							//月日の左罫線
							$befY = $this->getY();
							$this->SetXY($this->tabXleft, $befY);
							$this->SetLineWidth(0.2);
							$this->Cell(8, 4, '', 'L', 0, 'C', 0);
							//月日
							//◇$this->SetLineWidth(0.1);
							$this->SetXY($this->tabXleft, $befY);
							$this->Cell(8, 4, substr($arrDate[$bcnt],5,5), 'R', 0, 'C', 0);
							$time = strtotime($arrDate[$bcnt]);
							//◇$this->SetLineWidth(0.2);
							//曜日
							//◇$this->Cell(6, 4, $arrYoubi[date("w", $time)], 'R', 0, 'C', 0);
							$this->SetXY($this->tabXleft, $befY+4);//◇
							$this->Cell(8, 4, $arrYoubi[date("w", $time)], 'R', 0, 'C', 0);
							$this->SetXY($this->tabXleft+8, $befY);//◇
							//総行カウントアップ
							$cnt++;
						}else {
						//昼間だったら空セルを
							$befY = $this->getY();
							$this->SetXY($this->tabXleft, $befY);
							$this->SetLineWidth(0.2);
							$this->Cell(8, 4, '', 'L', 0, 'C', 0);
							$this->SetXY($this->tabXleft, $befY);
							//◇$this->SetLineWidth(0.1);
							$this->Cell(8, 4, '', 'R', 0, 'C', 0);
							//◇$this->SetLineWidth(0.2);
							//◇$this->Cell(6, 4, '', 'R', 0, 'C', 0);
							//総行カウントアップ
							$cnt++;
						}
						$dn = 0;
						while ($dn < 2){
							$dn++;
							$this->SetLineWidth(0.1);
							//現場
							//◇$this->Cell(26.5, 4, '', 'R', 0, 'L', 0);
							$this->Cell(24, 4, '', 'R', 0, 'L', 0);
							//現場の罫線左
							$this->SetLineWidth(0.2);
							//人数の罫線左
							$this->Cell(4, 4, '', 'R', 0, 'L', 0);
							$this->SetLineWidth(0.1);
							$l = 0;
							while ($l < ($this->maxShainSu - 1)){
								//ストライプテーブル
								//◆if ($l%2==1){
								if ($l%2==0){
									$this->SetFillColor(230);
									$touka = 1;
								}else{
									$this->SetFillColor(255);
									$touka = 0;
								}
								$l++;
								$this->Cell(4, 4, '', 'TRL', 0, 'L', $touka);
							}
							$this->SetLineWidth(0.2);
							if ($dn == 2){
								$this->Cell(4, 4, '', 'R', 2, 'L', 0);
								$befY = $this->getY();
								$this->SetXY($this->tabXleft, $befY);
						
							}else{
								$this->Cell(4, 4, '', 'R', 0, 'L', 0);
							}
						}
						if ($i == 0){
							$this->SetLineWidth(0.2);
							//太い区切り罫線
							$befX = $this->getX();
							$befY = $this->getY();
							$this->SetXY($befX, $befY-4);
							$this->Cell($this->wAllHeader, 4, '', 'T', 0, 'L', 0);
							$this->SetLineWidth(0.1);
//							$this->SetXY($befX, $befY);
							//◇$this->SetXY($befX+14, $befY);
							$this->SetXY($befX+8, $befY);//◇
							$this->Cell($this->wDpNHeader, 4, '', 'T', 0, 'L', 0);
							$this->SetXY($befX, $befY);
						}else{
							$this->SetLineWidth(0.1);
							//細い区切り罫線
							$befX = $this->getX();
							$befY = $this->getY();
							//◇$this->SetXY($befX+14, $befY);
							$this->SetXY($befX+8, $befY);//◇
							$this->Cell($this->wDpNHeader, 4, '', 'T', 0, 'L', 0);
							$this->SetXY($befX, $befY);
						}
					}			
				}
				$bcnt++;
			}
		}
		$nowY = $this->getY();
		$this->LoadTitle(1,$this->tabXleft,$nowY);

	}
	

	
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('Daiki Electric Co. Ltd.');
$pdf->SetAuthor('Daiki Electric Co. Ltd.');
//$pdf->SetMargins(10, 26.5, 5);
//$pdf->SetMargins(10, 27, 5);
$pdf->SetMargins(7, 46, 5);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);

//Footer消去
$pdf->setPrintFooter(false);
//set auto page breaks
//$pdf->SetAutoPageBreak(TRUE, 12);
$pdf->SetAutoPageBreak(false);

// ---------------------------------------------------------

// set font
$pdf->SetFont('msgothic', '', 10);

//期間を決定
$pageLimit = 0;

if ($req['range']==0){
	$nowy = date("Y");
	$nowm = date("m");
	$nowd = date("d");
	if ($req['span']==0){
		//5日分
		$from = $nowy.'/'.$nowm.'/'.$nowd;
		$to = date("Y/m/d", strtotime($nowy."-".$nowm."-".$nowd." +4 day"));
	}else if ($req['span']==1){
		//7日分
		$from = $nowy.'/'.$nowm.'/'.$nowd;
		$to = date("Y/m/d", strtotime($nowy."-".$nowm."-".$nowd." +6 day"));
	}else if ($req['span']==2){
		//1page分
		$from = $nowy.'/'.$nowm.'/'.$nowd;
		$to = date("Y/m/d", strtotime($nowy."-".$nowm."-".$nowd." +11 day"));
		$pageLimit = 1;
	}else if ($req['span']==3){
		//2page分
		$from = $nowy.'/'.$nowm.'/'.$nowd;
		$to = date("Y/m/d", strtotime($nowy."-".$nowm."-".$nowd." +23 day"));
		$pageLimit = 2;
	}else if ($req['span']==4){
		//3page分
		$from = $nowy.'/'.$nowm.'/'.$nowd;
		$to = date("Y/m/d", strtotime($nowy."-".$nowm."-".$nowd." +35 day"));
		$pageLimit = 3;
	}else if ($req['span']==5){
		//6page分
		$from = $nowy.'/'.$nowm.'/'.$nowd;
		$to = date("Y/m/d", strtotime($nowy."-".$nowm."-".$nowd." +55 day"));
		$pageLimit = 6;
	}
}else if  ($req['range']==1){
		$from = $req['from'];
		$to = $req['to'];
}
//Data loading
if ($req['target']==0){
	$pdf->LoadDataHeader1();
	// add a page
	$pdf->AddPage();
	$pdf->LoadData($from, $to, null);
	$pdf->LoadDataPlace($from, $to, null);
	$pdf->OutputTable($pageLimit);
}else if ($req['target']==1){
	$pdf->LoadDataHeader2($from, $to);
	// add a page
	$pdf->AddPage();
	$pdf->LoadData($from, $to, 1 );
	$pdf->LoadDataPlace($from, $to, 1);
	$pdf->OutputTable($pageLimit);
}else if ($req['target']==2){
	$pdf->LoadDataHeader1();
	// add a page
	$pdf->AddPage();
	$pdf->LoadData($from, $to, null );
	$pdf->LoadDataPlace($from, $to, null);
	$pdf->OutputTable($pageLimit);
	$pdf->LoadDataHeader2($from, $to);
	// add a page
	$pdf->AddPage();
	$pdf->LoadData($from, $to, 1);
	$pdf->LoadDataPlace($from, $to, 1);
	$pdf->OutputTable($pageLimit+$pageLimit);
}

// print colored table
//$pdf->ColoredTable($data,$dataH);

// ---------------------------------------------------------

//Close and output PDF document
//$pdf->Output('reports/example_011.pdf', 'F');
$pdf->Output('yotei'.date('Ymd').'.pdf', 'I');

//フォントサイズ計算
function getJustFontSizePDF($p_Cls ,$p_sValue, $p_nWidth, $p_nMaxSize) {
	$nWidth = $p_nWidth - 1;
	$nMax 	= $p_nMaxSize * 10;
	$nSize 	= $p_nMaxSize;
	for ($i = 0; $i <= $nMax; $i++) {
		$nSize = $p_nMaxSize - ($i * 0.1);
		$p_Cls->SetFont('msgothic', '', $nSize);
		$nLen = $p_Cls->GetStringWidth($p_sValue);
		if ($nLen <= $nWidth) {
			break;
		}
	}
	return $nSize;
}

//改行文字列処理
function getStringDivid($p_string, $p_length){
	//p_lengthの長さに分割した文字を返す
	//p_length：文字列長（1bite文字の文字数分の長さ）
	
	//文字列の文字数
	$mojiNum = mb_strlen($p_string);
	$a = 0;
	$mojiBite =0;
	$rtn = 0;
	$return = array();
	//文字数分ループ
	while ($a < $mojiNum){
		//$a文字目は全角か半角か？半角なら1全角なら2加算
		if (strlen(mb_substr($p_string,$a,1,'UTF-8')) > 1){
			$mojiBite = $mojiBite + 2;
		}else{
			$mojiBite = $mojiBite + 1;
		}
		//指定の長さを超えたらそのときの文字数を保存してループ終了
		if ($mojiBite > $p_length){
			$rtn = $a;
			break;
		}
		$a++;
	}
	$return[] = mb_substr($p_string,0,$rtn,'UTF-8');
	$return[] = mb_substr($p_string,$rtn,$p_length,'UTF-8');
	return $return;
}

//============================================================+
// END OF FILE                                                
//============================================================+
