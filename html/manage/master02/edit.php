<?
	/***************************************************************************
	 * Name 		:edit.php
	 * Description 		:登録(新規コンテンツ)
	 * Include		:func.common.inc
	 * 			 	func.field.inc
	 * 				func.fieldcheck.inc
	 * 			 	class.cls_contents.inc
	 * Trigger		:
	 * Create		:2009/10/01 Brico Suzuki
	 * LastModify		:
	 *
	 *
	 *
	 **************************************************************************/
	if ($_SERVER[SERVER_NAME] == 'daiki.bricoleur.in'){
		require_once('ini.inc');
	}
	include_once 'func.common.inc';
	include_once 'func.field.inc';
	include_once 'func.fieldcheck.inc';

	include_once 'class.cls_kubun.inc';
	include_once 'class.cls_uchiwake.inc';

	session_start();
	$data = $_REQUEST;
	
	//ログインチェック
	$blogin = isLogin();
	if (!($blogin)){
		header("Location: ".URL_LOGIN);
		exit;
	}
	$incFile = "form.inc";

	$err = 0;
	$enc = 0;
	$bDouble = false;
	

	//区分リスト取得
	$clsKbn 	= new cls_kubun();
	$arrKbn	= $clsKbn->getAllList();
	$clsKbn->close();
	$clsUcw 	= new cls_uchiwake();

	$f_add = false;
	$f_del = false;
	$f_order = false;
	//「新規登録」ボタンが押された場合
	switch ($data['m']) {
	case 'commit':
		$sDBmode 	= $data['mode'];
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])===true){
			$clsUcw->setData($data,1);
			$clsUcw->setWhere();
			$arrErrors = $clsUcw->isValidData();
			if (count($arrErrors) == 0){
				if ($sDBmode == "upd"){;
					$ContId = $clsUcw->doUpdate();
					$f_upd = true;
				}else{
					$ContId = $clsUcw->doInsert();
					$f_add = true;
				}
				$sDBmode = "upd";
				$data['SEL_UCW_KBN_NO'] = "";
				$clsUcw->setData($data,1);
				$clsUcw->setWhere();
				$data 	= $clsUcw->getInfo();
				$data['SEL_UCW_KBN_NO'] = $data['UCW_KBN_NO'];
				$f_add = true;
			}
		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	//「参照」リンクが押された場合
	case 'new':
		//$sDBmode=updで登録画面を表示
		$sDBmode 	= "new";
		break;
	default:
		//$sDBmode=updで登録画面を表示
		$sDBmode 	= "upd";
		//★二重投稿チェック
		$clsUcw->setData($data,1);
		$clsUcw->setWhere();
		$data 	= $clsUcw->getInfo();
		$data['SEL_UCW_KBN_NO'] = $data['UCW_KBN_NO'];
		$b_show = true;
		break;
	}

	//★二重投稿エラー
	if ($bDouble===true){
		$sErrorMsg = "二重送信エラー";
		$sErrorMsg2 = "ブラウザの「戻る」や「進む」機能で二重に同じ処理をすることはできません。";
		$incFile	= 'error.inc';
	}

	include_once "header.inc";
	//★
	if (($incFile == 'nodata.inc')||($incFile == 'error.inc')){
		include_once $incFile;
		include_once "footer.inc";
	}else{
		$dstoken = doubleSubmit();	//新しいトークン発行
		include_once $incFile;
		include_once "footer.inc";
	}
	$clsUcw->close();


///テスト用
/*
$rows = array(0=>array('UCW_NAME'=>'諸　経　費'),1=>array('UCW_NAME'=>'人　件　費'),2=>array('UCW_NAME'=>'機　器　損　料'));
include_once "header.inc";
include_once $incFile;
include_once "footer.inc";
*/

?>
