<?
	/***************************************************************************
	 * Name 		:index.php
	 * Description 		:登録(新規コンテンツ)
	 * Include		:func.common.inc
	 * 			 	func.field.inc
	 * 				func.fieldcheck.inc
	 * 			 	class.cls_contents.inc
	 * Trigger		:
	 * Create		:2009/10/01 Brico Suzuki
	 * LastModify		:
	 *
	 *
	 *
	 **************************************************************************/
	if ($_SERVER[SERVER_NAME] == 'daiki.bricoleur.in'){
		require_once('ini.inc');
	}
	include_once 'func.common.inc';
	include_once 'func.field.inc';
	include_once 'func.fieldcheck.inc';

	include_once 'class.cls_kubun.inc';
	include_once 'class.cls_uchiwake.inc';

	session_start();
	$data = $_REQUEST;

	//ログインチェック
	$blogin = isLogin();
	if (!($blogin)){
		header("Location: ".URL_LOGIN);
		exit;
	}
	$incFile = "list.inc";

	$err = 0;
	$enc = 0;
	$bDouble = false;
	
	//区分リスト取得
	$clsKbn 	= new cls_kubun();
	$arrKbn	= $clsKbn->getAllList();
	$clsKbn->close();


	$clsUcw 	= new cls_uchiwake();

	$f_add = false;
	$f_del = false;
	$f_order = false;
	//「新規登録」ボタンが押された場合
	switch ($data['m']) {
	case 'search':
		$clsUcw->setData($data,1);
		//一覧を表示
		$arrErrors = $clsUcw->isValidSearchData();
		if (count($arrErrors) == 0){
			$clsUcw->setWhere();
			$rows 	= $clsUcw->getList();
		}
		$incFile	= 'list.inc';
		break;
	//一覧から「削除」リンクが押された場合
	case 'del':
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])){
			$clsUcw->setData($data,1);
			//削除処理
			$ContId = $clsUcw->doDelete($data['UCW_UID']);
		
			$f_del = true;
			$data = array();
			$data['SEL_UCW_KBN_NO'] = $_REQUEST['SEL_UCW_KBN_NO'];
			$clsUcw->setData($data,1);
			$clsUcw->setWhere();
			$rows 	= $clsUcw->getList();
			$data['UCW_UID'] = "";
		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	//一覧から「↑」「↓」リンクが押された場合
	case 'move':
		//★二重投稿チェック
		if (doubleSubmit($data['dstoken'])){
			$clsUcw->setData($data,1);
			//削除処理
			if (!($clsUcw->modPriority())){
				$err = 1;
			};
			//メッセージ設定
			if ($err==1){
				$arrErrors['ERROR'] = "system error: 順序の変更に失敗しました。";
			}else{
				$f_order = true;
				$data = array();
				$data['SEL_UCW_KBN_NO'] = $_REQUEST['SEL_UCW_KBN_NO'];
				$clsUcw->setData($data,1);
				$clsUcw->setWhere();
				$rows 	= $clsUcw->getList();
			}
		}else{
			//★二重投稿
			$bDouble = true;
		}
		break;
	default:
		break;
	}

	//★二重投稿エラー
	if ($bDouble===true){
		$sErrorMsg = "二重送信エラー";
		$sErrorMsg2 = "ブラウザの「戻る」や「進む」機能で二重に同じ処理をすることはできません。";
		$incFile	= 'error.inc';
	}

	include_once "header.inc";
	//★
	if (($incFile == 'nodata.inc')||($incFile == 'error.inc')){
		include_once $incFile;
		include_once "footer.inc";
	}else{
		$dstoken = doubleSubmit();	//新しいトークン発行
		include_once $incFile;
		include_once "footer.inc";
	}
	$clsUcw->close();



?>
