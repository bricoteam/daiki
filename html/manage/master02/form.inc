<script type="text/javascript">
<?php
	if (count($arrErrors)>0){
?>
    $(function() { 
        $.notifyBar({ html: "入力エラーがあります" });
    });
<?php
	}else if ($f_add === true){
?>
    $(function() { 
        $.notifyBar({ html: "登録しました" });
    });
<?php
	}
?>
</script>
<script type="text/javascript" src="<?=URL_COMMON?>js/manage.js"></script>
<script language="javascript">
<!--
function doCommit() {
	var f = document.fList;
	f.m.value = 'commit';
	f.submit();
	return false;
}
function doList() {
	var f = document.fList;
	f.action = 'index.php';
	if (f.SEL_UCW_KBN_NO.value == ""){
		f.m.value = '';
	}else{
		f.m.value = 'search';
	}
	f.UCW_UID.value = '';
	f.dstoken.value = '';
	f.mode.value = '';
	f.submit();
	return false;
}
function doCalc()
{
	var total;
	var total2;
	if (document.getElementById('input06').value !== 0){
		total = document.getElementById('input06').value * document.getElementById('input04').value;
		total2 = Math.round(total/10);
		total = total2 * 10;
		document.getElementById('input07').value = total;
	}
}
    //-->
    </script>

  </head>

  <body data-spy="scroll" data-target=".subnav" data-offset="50" >


  <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?=URL_MANAGE?>"><?=SITE_NAME?></a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="">
                <a href="<?=URL_MANAGE?>"><i class="icon-home icon-white"></i>&nbsp;HOME</a>
              </li>
              <li class="active">
                <a href="#">内訳項目マスタ登録</a>
              </li>
            </ul>
            <p class="navbar-text pull-right"><i class="icon-user icon-white"></i>&nbsp;<?= $_ADMIN[$_SESSION['USER']][1] ?>さんでログイン中&nbsp;<a href="<?=URL_MANAGE?>logout.php" class="btn btn-danger">ログアウト&nbsp;<i class="icon-share-alt icon-white"></i></a></p>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span2">
          <?php
	  include 'menu.inc';
	  ?>
        </div><!--/span-->
        <div class="span9">
          <h2>内訳項目マスタ登録(<?=($sDBmode=="new")?'新規':'変更'?>)</h2>
          <p class="lead"></p>
      <?php if (count($arrErrors) > 0){ ?>
          <div class="alert alert-error ">
            <a class="close" data-dismiss="alert">×</a>
	    <ul>
            <li>
            <?=implode('</li><li>',$arrErrors)?>
	    </li>
	    </ul>
          </div>
      <?php } ?>
          <form  class="form-horizontal" name="fList" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" >
          <fieldset>
            <input type="hidden" name="m" value="" />
            <input type="hidden" name="dstoken" value="<?=$dstoken?>" />
            <input type="hidden" name="SEL_UCW_KBN_NO" value="<?=$data['SEL_UCW_KBN_NO']?>" />
            <input type="hidden" name="UCW_UID" value="<?=$data['UCW_UID']?>" />
            <input type="hidden" name="mode" value="<?=$sDBmode?>" />
            <div class="control-group">
              <label class="control-label" for="input01">区分</label>
              <div class="controls">
                <?=getSelectBox ($arrKbn, 'UCW_KBN_NO', $data['UCW_KBN_NO'],null,' id="input01"')?>&nbsp;<span class="pink">※必須</span>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="input02">内訳項目名</label>
              <div class="controls">
                <input type="text" name="UCW_NAME" value="<?=mbConv(htmlspecialchars(br4nl(($data['UCW_NAME'])),ENT_QUOTES),$enc)?>" class="span4" placeholder="内訳項目名を入力" style="ime-mode: active;" id="input02" />&nbsp;<span class="pink">※必須</span>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="input03">形式・形状</label>
              <div class="controls">
                <input type="text" name="UCW_FORMAT" value="<?=mbConv(htmlspecialchars(br4nl(($data['UCW_FORMAT'])),ENT_QUOTES),$enc)?>" class="span3" placeholder="形式・形状を入力" style="ime-mode: active;" id="input03" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="input04">数量</label>
              <div class="controls">
                <input type="text" name="UCW_NUM" value="<?=$data['UCW_NUM']?>" class="span1" placeholder="数量を入力" style="ime-mode: disabled;" id="input04" onChange="doCalc();" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="input05">単位</label>
              <div class="controls">
                <input type="text" name="UCW_UNIT" value="<?=mbConv(htmlspecialchars(br4nl(($data['UCW_UNIT'])),ENT_QUOTES),$enc)?>" class="span2" placeholder="単位を入力" style="ime-mode: active;" id="input05" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="input06">単価</label>
              <div class="controls">
                <input type="text" name="UCW_PRICE" value="<?=$data['UCW_PRICE']?>" class="span2" placeholder="単価を入力" style="ime-mode: disabled;" id="input06" onChange="doCalc();" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="input07">金額</label>
              <div class="controls">
                <input type="text" name="UCW_TOTAL" value="<?=$data['UCW_TOTAL']?>" class="span2" placeholder="金額を入力" style="ime-mode: disabled;" id="input07" />
              </div>
            </div>
            <div class="well">
              <a href="javascript:void(0);" onclick="doCommit();return false;" class="btn btn-primary span2"><?=($sDBmode=="new")?'登録':'変更'?></a>&nbsp;&nbsp;&nbsp;
              <a href="javascript:void(0);" onclick="doList();return false;" class="btn">一覧へ戻る</a>
            </div>
          </fieldset>
          </form>	
        </div>
      </div>
