<script type="text/javascript">
<?php
	if (count($arrErrors)>0){
?>
    $(function() { 
        $.notifyBar({ html: "入力エラーがあります" });
    });
<?php
	}else if ($f_del){
?>
    $(function() { 
        $.notifyBar({ html: "区分を削除しました。" });
    });
<?php
	}else if ($f_add){
?>
    $(function() { 
        $.notifyBar({ html: "区分を追加しました。" });
    });
<?php
	}else if ($f_upd){
?>
    $(function() { 
        $.notifyBar({ html: "区分を変更しました。" });
    });
<?php
	}else if ($f_order){
?>
    $(function() { 
        $.notifyBar({ html: "順番を変更しました。" });
    });
<?php
	}
?>
</script>
<script type="text/javascript" src="<?=URL_COMMON?>js/manage.js"></script>
<script language="javascript">
<!--
function doSearch() {
	var f = document.fList;
	f.m.value = 'search';
	f.submit();
	return false;
}
function doDelete(uid) {
	if (confirm('削除してよろしいですか？')){
		var f = document.fList;
		f.m.value = 'del';
		f.UCW_UID.value = uid;
		f.submit();
	}
	return false;
}
function doMove(kno,uid,pri,move) {
	var f = document.fList;
	f.m.value = 'move';
	f.kno.value = kno;
	f.uid.value = uid;
	f.pri.value = pri;
	f.move.value = move;
	f.submit();
	return false;
}
    
    //-->
    </script>

  </head>

  <body data-spy="scroll" data-target=".subnav" data-offset="50" >


  <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?=URL_MANAGE?>"><?=SITE_NAME?></a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="">
                <a href="<?=URL_MANAGE?>"><i class="icon-home icon-white"></i>&nbsp;HOME</a>
              </li>
              <li class="active">
                <a href="#">内訳項目マスタ登録</a>
              </li>
            </ul>
            <p class="navbar-text pull-right"><i class="icon-user icon-white"></i>&nbsp;<?= $_ADMIN[$_SESSION['USER']][1] ?>さんでログイン中&nbsp;<a href="<?=URL_MANAGE?>logout.php" class="btn btn-danger">ログアウト&nbsp;<i class="icon-share-alt icon-white"></i></a></p>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span2">
          <?php
	  include 'menu.inc';
	  ?>
        </div><!--/span-->
        <div class="span9">
          <h2>内訳項目マスタ登録&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="edit.php?m=new" class="btn btn-primary"><i class="icon-pencil icon-white"></i>&nbsp;新規作成</a></h2>
          <p class="lead"></p>
      <?php if (count($arrErrors) > 0){ ?>
          <div class="alert alert-error ">
            <a class="close" data-dismiss="alert">×</a>
	    <ul>
            <li>
            <?=implode('</li><li>',$arrErrors)?>
	    </li>
	    </ul>
          </div>
      <?php } ?>
          <form class="well form-inline" name="fList" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" >
            <input type="hidden" name="m" value="" />
            <input type="hidden" name="dstoken" value="<?=$dstoken?>" />
            <input type="hidden" name="UCW_UID" value="<?=$data['UCW_UID']?>" />
            <input type="hidden" name="kno" value="" />
            <input type="hidden" name="uid" value="" />
            <input type="hidden" name="pri" value="" />
            <input type="hidden" name="move" value="" />
            <input type="hidden" name="mode" value="<?=$sDBmode?>" />
            <label>区分</label>
	    <?=getSelectBox ($arrKbn, 'SEL_UCW_KBN_NO', $data['SEL_UCW_KBN_NO'],'区分を選択')?>
            <a href="javascript:void(0);" onclick="doSearch();return false;" class="btn btn-primary">表示</a>
          </form>	
	
          <!--pageSet -->
          <table class="table table-bordered table-striped">
            <tr>
              <th class="span1h">表示順序</th>
              <th class="">内訳項目名</th>
              <th class="">形式・形状</th>
              <th class="">数量</th>
              <th class="">単位</th>
              <th class="">単価</th>
              <th class="">金額</th>
              <th class="span1h">編集</th>
              <th class="span1h">削除</th>
            </tr>
<?php
$cnt = 0;
if ((is_array($rows))&&(count($rows)>0)){
foreach($rows as $row){
	$cnt++;
?>
            <tr>
              <td>
                  <a href="javascript:void(0);" onclick="doMove('<?=$row['UCW_KBN_NO']?>','<?=$row['UCW_UID']?>','<?=$row['UCW_PRIORITY']?>','up');return false;"><i class="icon-arrow-up"></i>上</a>&nbsp;/
                  <a href="javascript:void(0);" onclick="doMove('<?=$row['UCW_KBN_NO']?>','<?=$row['UCW_UID']?>','<?=$row['UCW_PRIORITY']?>','down');return false;">下<i class="icon-arrow-down"></i></a>
              </td>
              <td><?=$row['UCW_NAME']?></td>
              <td><?=$row['UCW_FORMAT']?></td>
              <td><?=nVal($row['UCW_NUM'],'&nbsp;')?></td>
              <td><?=nVal($row['UCW_UNIT'],'&nbsp;')?></td>
              <td><?=((strlen($row['UCW_PRICE'])>0)?number_format($row['UCW_PRICE']):'&nbsp;')?></td>
              <td><?=((strlen($row['UCW_TOTAL'])>0)?number_format($row['UCW_TOTAL']):'&nbsp;')?></td>
              <td><a href="edit.php?uid=<?=$row['UCW_UID']?>" class="btn btn-success"><i class="icon-pencil icon-white"></i>編集</a></td>
              <td><a href="javascript:void(0);" onclick="doDelete('<?=$row['UCW_UID']?>');return false;" class="btn btn-danger"><i class="icon-trash icon-white"></i>削除</a></td>
            </tr>
<?php
}
}
?>
          </table>
        </div>
      </div>