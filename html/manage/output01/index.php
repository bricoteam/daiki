<?
	/***************************************************************************
	 * Name 		:index.php
	 * Description 		:注文一覧出力
	 * Include		:func.common.inc
	 * 			 	func.field.inc
	 * 				func.fieldcheck.inc
	 * 			 	class.cls_estimate.inc
	 * Trigger		:
	 * Create		:2012/10/01 Brico Suzuki
	 * LastModify		:
	 *
	 *
	 *
	 **************************************************************************/
	if ($_SERVER[SERVER_NAME] == 'daiki.bricoleur.in'){
		require_once('ini.inc');
	}
	include_once 'func.common.inc';
	include_once 'func.field.inc';
	include_once 'func.fieldcheck.inc';

	session_start();
	$data = $_REQUEST;

	//ログインチェック
	$blogin = isLogin();
	if (!($blogin)){
		header("Location: ".URL_LOGIN);
		exit;
	}
	
	$arrYear = array();
	$a = 2011;
	$nYend = date('Y')+1;
	while($a < $nYend) {
		$arrYear[($a - 2000)] = $a.'年';
		$a++;
	}
	
	$incFile = "list.inc";

	$err = 0;
	$enc = 0;
	$bDouble = false;
	
	$_CUSTOMER_2 = array();
	foreach ($_CUSTOMER as $key => $val){
		$_CUSTOMER_2[$key] = nVal($val,'その他の相手先');
	}
	
	include_once "header.inc";
	include_once $incFile;
	include_once "footer.inc";



?>
