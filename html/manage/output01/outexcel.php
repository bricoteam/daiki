<?
	/***************************************************************************
	 * Name 		:outexcel.php
	 * Description 		:
	 * Include		:func.common.inc
	 * 			 	func.field.inc
	 * 				func.fieldcheck.inc
	 * Trigger		:
	 * Create		:2012/05/01 Brico Suzuki
	 * LastModify		:
	 *
	 *
	 *
	 **************************************************************************/
	if ($_SERVER[SERVER_NAME] == 'daiki.bricoleur.in'){
		require_once('ini.inc');
	}
	include_once 'func.common.inc';
	include_once 'class.cls_estimate.inc';

	session_start();
	$data = $_REQUEST;

	$blogin = isLogin();
	if (!($blogin)){
		header("Location: ".URL_LOGIN);
		exit;
	}

	$clsEst 	= new cls_estimate();
	$clsEst->setData($data,1);
	$row 	= $clsEst->getOutputList();
	$clsEst->close();
	
	$arrYear = array();
	$a = 2011;
	$nYend = date('Y')+1;
	while($a < $nYend) {
		$arrYear[($a - 2000)] = $a.'年';
		$a++;
	}
	
	//必要なクラスをインクルード
	//set_include_path(get_include_path() . PATH_SEPARATOR . './Classes/');
	include 'Classes/PHPExcel.php';
	include 'Classes/PHPExcel/IOFactory.php';

	//PHPExcelオブジェクトの生成
	$xl = new PHPExcel();


	//シートの設定
	$xl->setActiveSheetIndex(0);
	$sheet = $xl->getActiveSheet();
	$sheet->setTitle('注文一覧');

	//スタイルの設定(標準フォント)
	$sheet->getDefaultStyle()->getFont()->setName('ＭＳ Ｐゴシック');
	$sheet->getDefaultStyle()->getFont()->setSize(11);

	//共通変数
	$colnum = 13; //列数

	//(4)セルの幅を設定
	$Col = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N');
	$width = array(12,12,12,40,20,35,35,35,12,12,12,12,14,14);
	for($a = 0; $a <= $colnum; $a++) {
		$sheet->getColumnDimension($Col[$a])->setWidth($width[$a]);
	}
	//$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
	//タイトルセルの高さを設定
	$sheet->getRowDimension('A1')->setRowHeight(23);
	
	//タイトルの値を設定
	$sheet->setCellValue('A1', '注文書'); //文字列
	$sheet->setCellValue('B1', ((strlen($data['c'])>0)?nVal($_CUSTOMER[$data['c']],'その他相手先'):'全ての相手先'));
	$sheet->setCellValue('C1', ((strlen($data['y'])>0)?$arrYear[$data['y']]:'全ての年'));
	//タイトルを太字、フォントを14にする
	$sheet->getStyleByColumnAndRow(0, 1)->getFont()->setBold(true);
	$sheet->getStyleByColumnAndRow(1, 1)->getFont()->setBold(true);
	$sheet->getStyleByColumnAndRow(2, 1)->getFont()->setBold(true);
	$sheet->getStyleByColumnAndRow(0, 1)->getFont()->setSize(14);
	$sheet->getStyleByColumnAndRow(1, 1)->getFont()->setSize(14);
	$sheet->getStyleByColumnAndRow(2, 1)->getFont()->setSize(14);

	
													
	//タイトル定義
	$title = array('受付日','見積書番号','取引先コード','相手先名','注文番号','向先件名','　','作業内容','作業期間','　','担当者','　','見積合計金額','当案件注文価格');
	//タイトル列の装飾
	for($a = 0; $a <= $colnum; $a++) {
		$sheet->getStyleByColumnAndRow($a, 2)->getFont()->setBold(true);
		$sheet->getStyleByColumnAndRow($a, 2)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
		$sheet->getStyleByColumnAndRow($a, 2)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$sheet->getStyleByColumnAndRow($a, 2)->getFill()->getStartColor()->setRGB('6699FF');//ブルー
		// 罫線
		$sheet->getStyleByColumnAndRow($a, 2)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$sheet->getStyleByColumnAndRow($a, 2)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$sheet->getStyleByColumnAndRow($a, 2)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		//タイトル文字の出力
		$sheet->setCellValueByColumnAndRow($a, 2, $title[$a]);
	}
	$sheet->getStyleByColumnAndRow(13, 2)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);		
	//データ出力
	$cnt = 3;
	if ((is_array($row))&&(count($row)>0)){
		foreach ($row as $rows){
			//受付日
			$sheet->setCellValueByColumnAndRow(0, $cnt, $rows['HDR_DATE_ACCEPT']);
			//見積書番号
			$sheet->setCellValueExplicitByColumnAndRow(1, $cnt, $rows['HDR_NO'], PHPExcel_Cell_DataType::TYPE_STRING);
			//取引先コード
			$sheet->setCellValueExplicitByColumnAndRow(2, $cnt, $rows['HDR_CUSTOMER_CODE'], PHPExcel_Cell_DataType::TYPE_STRING);
			//相手先名
			$sheet->setCellValueExplicitByColumnAndRow(3, $cnt, $rows['HDR_CUSTOMER_NAME'], PHPExcel_Cell_DataType::TYPE_STRING);
			//注文番号
			$sheet->setCellValueExplicitByColumnAndRow(4, $cnt, $rows['HDR_ORDER_NO'].$rows['HDR_ORDER_NO_B'], PHPExcel_Cell_DataType::TYPE_STRING);
			//向先件名
			$sheet->setCellValueExplicitByColumnAndRow(5, $cnt, $rows['HDR_TITLE_1'], PHPExcel_Cell_DataType::TYPE_STRING);
			//向先件名
			$sheet->setCellValueExplicitByColumnAndRow(6, $cnt, $rows['HDR_TITLE_2'], PHPExcel_Cell_DataType::TYPE_STRING);
			//作業内容
			$sheet->setCellValueExplicitByColumnAndRow(7, $cnt, $rows['HDR_WORK_CONTENTS'], PHPExcel_Cell_DataType::TYPE_STRING);
			$sheet->getStyleByColumnAndRow(7, $cnt)->getAlignment()->setWrapText(true);
			//作業期間
			$sheet->setCellValueByColumnAndRow(8, $cnt, $rows['HDR_DATE_TERM_F']);
			//作業期間
			$sheet->setCellValueByColumnAndRow(9, $cnt, $rows['HDR_DATE_TERM_T']);
			//担当者部署
			$sheet->setCellValueExplicitByColumnAndRow(10, $cnt, $rows['HDR_DEPT'], PHPExcel_Cell_DataType::TYPE_STRING);
			//担当者名
			$sheet->setCellValueExplicitByColumnAndRow(11, $cnt, $rows['HDR_CHARGER'], PHPExcel_Cell_DataType::TYPE_STRING);
			//見積合計金額
			$sheet->getCellByColumnAndRow(12, $cnt)->setValueExplicit(nVal($rows['HDR_TOTAL'],0), PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$sheet->getStyleByColumnAndRow(12, $cnt)->getNumberFormat()->setFormatCode('#,##0');
			//当案件注文価格
			$sheet->getCellByColumnAndRow(13, $cnt)->setValueExplicit(nVal($rows['HDR_ORDER_TOTAL'],0), PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$sheet->getStyleByColumnAndRow(13, $cnt)->getNumberFormat()->setFormatCode('#,##0');
			
			//列の装飾
			for($a = 0; $a <= $colnum; $a++) {
				// 罫線
				$sheet->getStyleByColumnAndRow($a, $cnt)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$sheet->getStyleByColumnAndRow($a, $cnt)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			}
			$sheet->getStyleByColumnAndRow(13, $cnt)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);		

			$cnt++;
		}
	}

	//(6)Excel2007形式で保存
	$file_name ='OrderList_'.$data['c'].$data['y'].date('Ymd').'.xlsx';
	$file_path ='excel/OrderList_'.$data['c'].$data['y'].date('Ymd').'.xlsx';
	
	$writer = PHPExcel_IOFactory::createWriter($xl, 'Excel2007');
	$writer->save($file_path);
	header('Content-Disposition: attachment; filename="'.$file_name.'"');
	header("Content-Type: application/octet-stream");
	readfile($file_path);

//	header('Content-Type: application/vnd.ms-excel');	
//	header('Content-Disposition: attachment; filename="OrderList_'.$data['c'].date('Ymd').'.xls"');	

?>
