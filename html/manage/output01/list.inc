<script type="text/javascript" src="<?=URL_COMMON?>js/manage.js"></script>
<script language="javascript">
<!--
function openXLS(){
	var f = document.fList;
	var index = f.sel_cust.selectedIndex;
	var cust = f.sel_cust.options[index].value;

	var index2 = f.sel_year.selectedIndex;
	var yyyy = f.sel_year.options[index2].value;

	var childWindow = window.open('<?=URL_MANAGE?>output01/outexcel.php?c='+cust+'&y='+yyyy, '_blank');
}
    //-->
    </script>

  </head>

  <body data-spy="scroll" data-target=".subnav" data-offset="50" >


  <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?=URL_MANAGE?>"><?=SITE_NAME?></a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="">
                <a href="<?=URL_MANAGE?>"><i class="icon-home icon-white"></i>&nbsp;HOME</a>
              </li>
              <li class="active">
                <a href="#">注文一覧出力</a>
              </li>
            </ul>
            <p class="navbar-text pull-right"><i class="icon-user icon-white"></i>&nbsp;<?= $_ADMIN[$_SESSION['USER']][1] ?>さんでログイン中&nbsp;<a href="<?=URL_MANAGE?>logout.php" class="btn btn-danger">ログアウト&nbsp;<i class="icon-share-alt icon-white"></i></a></p>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span2">
          <?php
	  include 'menu.inc';
	  ?>
        </div><!--/span-->
        <div class="span9">
          <h2>注文一覧出力</h2>
          <p class="lead"></p>
          <form class="well form-horizontal" name="fList" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>" >
            <fieldset>
            <div class="control-group">
              <label class="control-label" for="input01">相手先</label>
              <div class="controls">
                <?=getSelectBox ($_CUSTOMER_2, 'sel_cust', '','全て','id="input01"')?>
              </div>
              <label class="control-label" for="input02">年</label>
              <div class="controls">
                <?=getSelectBox ($arrYear, 'sel_year', '','全て','id="input02"')?>
              </div>
            </div>
            <a href="javascript:void(0);" onclick="openXLS();return false;" class="btn btn-primary span3 offset1"><i class="icon-download-alt icon-white"></i>注文一覧出力</a>
            </fieldset>
          </form>	
